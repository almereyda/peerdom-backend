#! /usr/bin/env bash

set -x

# We don't have pre command out of the dev env
export node_pre_command=""

# This check if we have a DATABASE URL in the environment.
# If so, that mean we are on dokku
if test ! -z ${DATABASE_URL+x}; then
  cp config/ormconfig.dokku.js ormconfig.js
elif test ! -z ${PLATFORM_APPLICATION_NAME}; then
  cp config/ormconfig.platformsh.js ormconfig.js
elif test ! -z ${GITLAB_CI}; then
  cp config/ormconfig.ci.json ormconfig.json
# Otherwise we copy the ormconfig sample
elif test ! -f ormconfig.json; then
  cp config/ormconfig-example.json ormconfig.json
fi
