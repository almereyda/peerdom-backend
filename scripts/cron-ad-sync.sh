#!/bin/bash -l

set -euo pipefail
IFS=$'\n\t'

HEALTHCHECK_URL="https://hc-ping.com/3ede5cc2-c79f-49f1-bd25-ed2082132bea"

cd "$APP_HOME"

node build/src/scripts/azure-sync-user.js &&
  curl -fsS --retry 3 "$HEALTHCHECK_URL" >/dev/null
