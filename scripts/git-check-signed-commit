#!/usr/bin/env bash

# We need to add the original upstream tree as origin in the MR so we can
# compare against the right master tree

git remote add peerdom-backend-upstream https://gitlab.com/peerdom/peerdom-backend.git
git fetch -q peerdom-backend-upstream master

if [ -z "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"]; then
  CI_MERGE_REQUEST_TARGET_BRANCH_NAME="peerdom-backend-upstream/master"
fi

echo "target=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
echo "source=$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"

# create list of commits of the current branch
commits=$(git rev-list --no-merges $CI_MERGE_REQUEST_TARGET_BRANCH_NAME..)
if [ -z "$commits" ]; then
  # OK if both branches are 'even'
  git diff --quiet $CI_MERGE_REQUEST_TARGET_BRANCH_NAME.. && exit 0

  echo "Couldn't find any commits to check"
  exit 1
fi

# check if author's email matches email in 'Signed-off-by'
for hash in $commits; do
  author=$(git log --format='%ae' ${hash}^\!)
  signed=$(git log --format='%b' ${hash}^\! | grep -i "Signed-off-by:")
  echo "Checking commit $hash from Author $author and Signed-off-by: $signed"
  if [ $? -ne 0 ]; then
    echo "Missing Signed-off-by"
    exit 1
  fi
  if ! echo $signed | grep -q "Signed-off-by:.*<${author}>"; then
    echo "Author '${author}' doesn't match"
    exit 1
  fi
done

# TODO: Auto comment in the MR about the Signed-off missing or not matching.
