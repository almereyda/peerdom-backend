#! /usr/bin/env bash

set -e

echo "PRD >> Running database migration"
node_modules/.bin/typeorm migration:run

echo "PRD >> Starting the server"
node_modules/.bin/pm2 --no-daemon \
  --interpreter-args '-r tsconfig-paths/register' \
  --time \
  --merge-logs \
  --log /app/run/peerdom.log \
  --force \
  start build/src/server.js --name peerdom-server
