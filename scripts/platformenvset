#!/usr/bin/env bash

set -e

declare -r PLATFORMSH_ID
declare -r PLATFORMSH_ENVIRONMENT

readonly COMMON_OPTIONS="--level environment --project ${PLATFORMSH_ID} --environment ${PLATFORMSH_ENVIRONMENT}"
readonly WRITE_OPTIONS="--json=false --enabled=true --inheritable=true --visible-build=true --visible-runtime=true --no-wait"

function create {
  local name="$1"
  local value="$2"
  local sensitive="${3:-false}"

  platform variable:create ${COMMON_OPTIONS} ${WRITE_OPTIONS} --name "env:$name" --value "$value" --sensitive "$sensitive"
}

function get {
  local name="$1"

  platform variable:get ${COMMON_OPTIONS} --property value "env:$name"
}

function update {
  local name="$1"
  local value="$2"
  local sensitive="${3:-false}"

  platform variable:update ${COMMON_OPTIONS} ${WRITE_OPTIONS} --value "$value" --sensitive "$sensitive" "env:$name"
}

function upsert {
  local name="$1"
  local value="$2"
  local sensitive="${3:-false}"

  set +e
  variable_current_value=$(get "$name" "$value" "$sensitive")
  retvalue=$?
  set -e

  # If getting the value fail, it means that we can create the variable
  if test $retvalue -eq 1; 
  then
    create "$name" "$value" "$sensitive"
    echo "success: the '$name' variable has been created."
  else
    # If the value passed to this script is the same as the one online, we don't do anything. 
    if test "$variable_current_value" = "$value"; then
      echo "success: the '$name' variable does not need to be created or updated."
    else
      update "$name" "$value" "$sensitive"
    echo "success: the '$name' variable has been updated."
    fi
  fi
}

upsert "$@"
