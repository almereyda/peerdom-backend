# Peerdom's Makefile
#
# Welcome to Peerdom's Makefile, this is your friendly tutorial about how a
# Makefile works!
#
# Makefile syntax can be weird, but with a bit of habbit it can become
# relativly readable.
#
# So let's get started.

# Here we ensure that every command this Makefile run will run in a bash shell,
# instead that the default 'sh'. This is actually just a variable assignement.
SHELL := /usr/bin/env bash

# Here we are defining some variable dedicated to our code organisation
#
# This one is the list of folder where we have source code file
SOURCE_DIR := src tests
# This one, contains a list of all the TypeScript file, note that it use
# `$(shell ...)` to call a standard shell command (in this case, the `find`
# command)
TS_FILES := $(shell find $(SOURCE_DIR) -name '*.ts')
DIR_TO_CREATE := public/uploads src/fixtures/avatar

# Environment Control
#
# Here we are defining small snippet of code and store them in variable. Those
# snippet are dedicated to testing our current environment, and will allow us
# to run or prevent running part of the Makefile depending of the current
# environment.
#
# You can think of those as function, but it is really just a little of code
# that will be replaced in place during the Makefile execution. This is why
# those are ended with `||` (the or symbole of bash), so that what we put after
# the test will be executed if the test fails.
#
# Run only if not in the CI
not_in_ci := test "${GITLAB_CI}" ||
# Don't run on platform.sh
not_on_platform := test ! -z ${PLATFORM_APPLICATION_NAME} ||
# Run only if not in production
not_in_prod := $(not_on_platform) test "${NODE_ENV}" == 'production' ||
# Run only in dev
only_in_dev := $(not_in_ci) $(not_in_prod)

# Pre command
#
# Finally, because each line in a Makefile is run in its own little shell, we
# have to define some pre-command that will do or load some stuff in the
# environment before we actually run the command we want.
#
# This ensure we load the correct node version via NVM, and because we use `=?`
# it is only set if a variable named `node_pre_command` don't yet exist. This
# allow you to override the pre command from outside the Makefile.
node_pre_command ?= . ~/.nvm/nvm.sh && nvm use --silent > /dev/null &&
# The .bin folder of the Node modules, useful to run tools installed via our
# `packages.json`
node_bin := $(node_pre_command) ./node_modules/.bin
# The npm command, with the precommand
npm := $(node_pre_command) npm
# The node command, with the precommand
node := $(node_pre_command) node

# This is a Makefile settings stating what target (ie: command) is ran by
# default. In this case we run the `help` target by default
.DEFAULT_GOAL := help

# We are finally at our first rule!
build: $(TS_FILES) node_modules    ## Build Peerdom
	@echo "🔨 Build Peerdom"
	@rm -rf build
	@$(node_bin)/tsc --build tsconfig.json
	@touch $@

# Let's break that down. A Makefile rule looks like this:
#
# target: dependencies
# 	command1
# 	command2
# 	...
#
# The rule build the target based on the dependencies as long as any dependency
# is newer than the target, a dependency can be either a file/folder, or
# another target in the Makefile.
#
# make will look at the target on the filesystem, here it will check if the
# `build` folder is older than any of its dependency and if so, it will execute
# all the commands in the rule.  In this case, the dependency are:
# - all the TypeScript files
# - the `node_modules` folder
#
# By default, make will print all the executed commands on stdout, if you want
# to silence that, you can start a command with `@` in a rule. For example here
# `echo "🔨 Build Peerdom"` would result in the following output:
# ```
# echo "🔨 Build Peerdom"
# 🔨 Build Peerdom
# ```
#
# While `@echo "🔨 Build Peerdom"` result in:
# ```
# 🔨 Build Peerdom
# ```
#
# In this makefile, we use `@` almost everywhere.
#
# When you need to call a variable, you must put `$()` around it. Here we use
# that to call the correct `tsc` binary via `$(node_bin)/tsc`. Make comes with
# a set of magic variable, here we can see `$@` that is replaced with the
# target of the rule. (so `build` in this example).
#
# Finally, just to be extra sure the changed time of build is updated after the
# rule being run, we `touch` the folder as the last command.



# Our second rule first define a `depinstall` target (which won't exist in the
# filesystem, so it is declared as .PHONY just after), and the `node_modules`
# as dependency.  Then, we define how to create the `node_modules` via the
# `node_modules` target, with our package.json and lock as dependencies. If one
# of those is updated, this will automatically run the `npm ci` command,
# otherwise it won't do anything.
#
# Here we use `depinstall` as an alias for `node_modules`. You can try it by
# running `make node_modules` in your shell. It will do the same things as
# running `make depinstall`
#
# This alias trick is use extensively throught the Makefile
depinstall: node_modules           ## Install all the code dependencies
node_modules: package.json package-lock.json
	@echo "📦 Install the dependencies"
	@$(npm) ci
.PHONY: depinstall
# We declare the .PHONY at the end only for organisational reason. It makes it
# look like we are "closing" our rule, and help the readability a bit.

test: build service-up node_modules .mk/migrate load-data env.sh ## Run the tests
	@echo "🧪  Run the tests"
	@source env.sh && export NODE_ENV=test && $(node_bin)/jest $(jest_additional_args)
.PHONY: test

test-watch: ## Run the tests, and watch for changes
	make test jest_additional_args="--watch"


# Here we use one of our "snippet" to ensure this only run when in dev mode.
# In the CI, staging, or production, we don't need to manually start the
# external service the backend depend on.
up: service-up  ## Alias for service-up
service-up: .mk docker-compose.yml ## Start the services we need
	@$(only_in_dev) echo "🐳 Start the needed containers"
	@$(only_in_dev) docker-compose up -d
.PHONY: service-up

.mk:
	@echo "📂 Create the needed folder for the Makefile"
	@mkdir -p $@

down: service-down  ## Alias for service-down
service-down:               ## Stop and destroy the services
	@$(only_in_dev) echo "🐳 Stop and delete our containers"
	@$(only_in_dev) docker-compose down
.PHONY: service-down

migrate: .mk/migrate        ## Migrate the database to the last state
.mk/migrate: .mk service-up build ormconfig.json
	@echo "🔄 Run the database migrations"
	@$(node_bin)/typeorm migration:run
	@touch $@
.PHONY: migrate

# In this rule, instead of trying to create a super complex tangling of
# dependency, we just call the Makefile target we wish to run directly in the
# rule.
#
# Because those command have dependency declared, we are sure everything will
# be run correctly.
migrate-from-scratch:        ## Migrate the database to the last but delete it first
	@make clean
	@make service-down
	@make migrate
.PHONY: migrate-from-scratch

s: serve             ## Alias for server
serve: server        ## Alias for server
server: build service-up .mk/migrate public/uploads ormconfig.json env.sh ## Start Peerdom
	@echo "🏁 Start the server"
	@source env.sh && $(node_bin)/nodemon
.PHONY: s serve server

dokku-build:
	@echo "🔨 Build Peerdom"
	@NODE_ENV=dev npm ci
	@make build
	@npm prune --production

prod-build: build
	@npm prune --production

prod-serve: ormconfig.json
	@echo "🏁 Start the server"
	@scripts/start.sh

create-dir: $(DIR_TO_CREATE)     ## Create the require directory for the server to work
$(DIR_TO_CREATE):
	@echo "📂 Create the $@ directory"
	@mkdir -p $@
.PHONY: create-dir

load-data: service-up build .mk/migrate public/uploads ormconfig.json src/fixtures/avatar ## Load our dummy data
	@echo "🚚 Load our fixtures"
	@$(node) build/src/scripts/loadData.js
	@cp -R src/fixtures/avatar public/uploads
.PHONY: load-data

dbconfig: ormconfig.json   ## Run our setup up DB script to copy the right configuration in your env
ormconfig.json:
	@echo "🔨 Copy our default dev DB configuration"
	@scripts/setup-db-config.sh
.PHONY: dbconfig

env.sh: ## Copy a default env.sh when running in development
	@$(only_in_dev) echo "🔨 Copy our default dev DB configuration"
	@$(only_in_dev) cp config/env.sh.example env.sh

clean:              ## Clean all the artifacts
	@echo "🗑  Delete artifacts"
	@rm -rf src/**/*.js
	@rm -rf src/**/*.js.map
	@rm -rf tests/**/*.js
	@rm -rf tests/**/*.js.map
	@rm -rf build
	@rm -rf .mk
.PHONY: clean

clean-all: clean     ## ⚠️  Clean everything, including your local configuration file
	@echo "🗑  Delete code dependencies and databases"
	@rm -rf node_modules
	@rm -rf ormconfig.json
	@rm -rf ormconfig.js
	@rm -rf env.sh
	@rm -rf $(DIR_TO_CREATE)
	@make service-down || true && echo ">>> No docker container to clean, that's fine!" # This has the right to fail
.PHONY: clean-all

# ###############
# Command Section
# ###############
ADD_TENANT_SUFFIX = .before-add-tenant.bck
add-tenant: build env.sh
	@echo ""
	@echo "ℹ️  Usage: make add-tenant name=\"Name of the tenant\" slug=\"slug\" emails=\"email1 email2 email3\""
	@echo "ℹ️"
	@echo "ℹ️  This is going to source your env.sh file, and run a script that modify the PRODUCTION database."
	@echo "ℹ️  Please make sure that:"
	@echo "ℹ️    - you have the last version of the code"
	@echo "ℹ️    - you have exported the DATABASE_URL variable in your environment."
	@echo ""
	@echo "❓ Press [Ctrl-C] to cancel the run"
	@read -p "❓ Press [Enter] key to run the script"
	@echo "ℹ️  We save any ormconfig.js{,on} if one exist"
	@(test -f ormconfig.js && mv ormconfig.js ormconfig.js$(ADD_TENANT_SUFFIX)) || true
	@(test -f ormconfig.json && mv ormconfig.json ormconfig.json$(ADD_TENANT_SUFFIX)) || true
	@cp config/ormconfig.dokku.js ormconfig.js
	@echo "🛂  Creating a new tenant"
	@source env.sh && $(node) build/src/scripts/create-tenant.js "$(name)" "$(slug)" $(emails)
	@rm -rf ormconfig.js || true
	@echo "ℹ️  We restore any ormconfig.js{,on} if one exist"
	@(test -f ormconfig.js$(ADD_TENANT_SUFFIX) && mv ormconfig.js$(ADD_TENANT_SUFFIX) ormconfig.js) || true
	@(test -f ormconfig.json$(ADD_TENANT_SUFFIX) && mv ormconfig.json$(ADD_TENANT_SUFFIX) ormconfig.json) || true

ADD_ACCOUNTS_SUFFIX = .before-add-accounts.bck
add-accounts: build env.sh
	@echo ""
	@echo "ℹ️  Usage: make add-accounts slug=\"tenantSlug\" csv=\"path/to/the/csv\""
	@echo "ℹ️"
	@echo "ℹ️  This is going to source your env.sh file, and run a script that modify the PRODUCTION database."
	@echo "ℹ️  Please make sure that:"
	@echo "ℹ️    - you have the last version of the code"
	@echo "ℹ️    - you have exported the DATABASE_URL variable in your environment."
	@echo ""
	@echo "❓ Press [Ctrl-C] to cancel the run"
	@read -p "❓ Press [Enter] key to run the script"
	@echo "ℹ️  We save any ormconfig.js{,on} if one exist"
	@(test -f ormconfig.js && mv ormconfig.js ormconfig.js$(ADD_ACCOUNTS_SUFFIX)) || true
	@(test -f ormconfig.json && mv ormconfig.json ormconfig.json$(ADD_ACCOUNTS_SUFFIX)) || true
	@cp config/ormconfig.dokku.js ormconfig.js
	@echo "🛂  Adding the new accounts"
	@source env.sh && $(node) build/src/scripts/add-accounts.js "$(slug)" "$(csv)"
	@rm -rf ormconfig.js || true
	@echo "ℹ️  We restore any ormconfig.js{,on} if one exist"
	@(test -f ormconfig.js$(ADD_ACCOUNTS_SUFFIX) && mv ormconfig.js$(ADD_ACCOUNTS_SUFFIX) ormconfig.js) || true
	@(test -f ormconfig.json$(ADD_ACCOUNTS_SUFFIX) && mv ormconfig.json$(ADD_ACCOUNTS_SUFFIX) ormconfig.json) || true

LOAD_AVATAR_URLS_SUFFIX = .before-load-avatar-urls.bck
load-avatar-urls: build env.sh
	@echo ""
	@echo "ℹ️  Usage: make load-avatar-urls endpoint=\"s3Endpoint\" accessKey=\"s3AccessKey\" secret=\"s3Secret\" bucket=\"s3Bucket\""
	@echo "ℹ️"
	@echo "ℹ️  This is going to source your env.sh file, and run a script that modify the PRODUCTION database."
	@echo "ℹ️  Please make sure that:"
	@echo "ℹ️    - you have the last version of the code"
	@echo "ℹ️    - you have exported the DATABASE_URL variable in your environment."
	@echo ""
	@echo "❓ Press [Ctrl-C] to cancel the run"
	@read -p "❓ Press [Enter] key to run the script"
	@echo "ℹ️  We save any ormconfig.js{,on} if one exist"
	@(test -f ormconfig.js && mv ormconfig.js ormconfig.js$(LOAD_AVATAR_URLS_SUFFIX)) || true
	@(test -f ormconfig.json && mv ormconfig.json ormconfig.json$(LOAD_AVATAR_URLS_SUFFIX)) || true
	@cp config/ormconfig.dokku.js ormconfig.js
	@echo "🛂  Loading avatar URLs from S3 storage"
	@source env.sh && $(node) build/src/scripts/load-avatar-urls.js "$(endpoint)" "$(accessKey)" "$(secret)" "$(bucket)"
	@rm -rf ormconfig.js || true
	@echo "ℹ️  We restore any ormconfig.js{,on} if one exist"
	@(test -f ormconfig.js$(LOAD_AVATAR_URLS_SUFFIX) && mv ormconfig.js$(LOAD_AVATAR_URLS_SUFFIX) ormconfig.js) || true
	@(test -f ormconfig.json$(LOAD_AVATAR_URLS_SUFFIX) && mv ormconfig.json$(LOAD_AVATAR_URLS_SUFFIX) ormconfig.json) || true

# The last command of this help does most of the magic. It check for any target
# that end in comment with `##`, extract the target and command, and print it
# as help.  Any target without a `##` comment won't show up in the help.
help: ## This help
	@echo Basic commands
	@echo
	@printf "\033[36m%-30s\033[0m %s\n" "make build" "Build the project"
	@printf "\033[36m%-30s\033[0m %s\n" "make server" "Start the server"
	@printf "\033[36m%-30s\033[0m %s\n" "make test" "Run the test"
	@printf "\033[36m%-30s\033[0m %s\n" "make clean-all" "Clean everything"
	@echo
	@echo Most of the remaining command are used has dependency to the basic ones.
	@echo But can also be used directly.
	@echo
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help
