import {createConnection} from 'typeorm';

import {app, graphqlPath} from './app';
import {checkEmailProviderConnection} from './lib/email';

const port = process.env.PORT || 3000;

createConnection()
  .then(async () => {
    // Check if the email configuration is correct
    await checkEmailProviderConnection();
    await app.listen({port}, () => {
      console.log(`🚀 Server ready at http://127.0.0.1:${port}${graphqlPath}`);
    });
  })
  .catch(error => console.log(error));
