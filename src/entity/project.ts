import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  JoinColumn,
  Index,
  ManyToMany,
  JoinTable,
  OneToMany
} from 'typeorm';

import {Tenant} from './tenant';
import {Holder} from './holder';

@Entity('project')
@Index(['slug', 'tenant'], {unique: true})
export class Project {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  slug: string;

  @Column({nullable: true})
  description: string;

  @Column({nullable: true})
  notes: string;

  @Column({nullable: true})
  externalUrl: string;

  @Column({default: false})
  archived: boolean;

  @ManyToOne(type => Project, parent => parent.children, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  parent: Project;

  @OneToMany(type => Project, node => node.parent)
  children: Project[];

  @ManyToMany(type => Holder)
  @JoinTable()
  holders: Holder[];

  @ManyToOne(type => Tenant, tenant => tenant.projects, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  tenant: Tenant;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
