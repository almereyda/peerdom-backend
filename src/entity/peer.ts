import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany,
  JoinColumn,
  OneToOne,
  Index
} from 'typeorm';

import {Tenant} from './tenant';
import {Holder} from './holder';
import {Mission} from './mission';
import {Account} from './account';

@Entity('peer')
@Index(['slug', 'tenant'], {unique: true})
export class Peer {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Tenant, tenant => tenant.peers, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  tenant: Tenant;

  @ManyToOne(type => Account, account => account.peers)
  @JoinColumn()
  account: Account;

  @OneToMany(type => Holder, holder => holder.peer)
  holders: Holder[];

  @Column()
  displayName: string;

  @Column()
  slug: string;

  @Column({nullable: true})
  avatarUrl: string;

  @Column({nullable: true})
  email: string;

  @OneToOne(type => Mission, mission => mission.peer)
  mission: Mission;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
