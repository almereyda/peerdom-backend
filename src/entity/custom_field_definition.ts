import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany,
  JoinColumn
} from 'typeorm';

import {Tenant} from './tenant';
import {CustomField} from './custom_field';

interface Translation {
  en?: string;
  de?: string;
  fr?: string;
  nl?: string;
  sv?: string;
}

export enum CustomFieldType {
  Text = 'text',
  Textarea = 'textarea',
  Email = 'email',
  Link = 'link',
  Date = 'date'
}

@Entity('custom_field_definition')
export class CustomFieldDefinition {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Tenant, tenant => tenant.customFieldDefinitions, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  tenant: Tenant;

  @OneToMany(type => CustomField, customField => customField.definition)
  fields: CustomField[];

  @Column()
  name: string;

  @Column({type: 'jsonb', nullable: true})
  translation: Translation;

  @Column({
    type: 'enum',
    enum: Object.keys(CustomFieldType).map(k => CustomFieldType[k]),
    default: 'text'
  })
  type: CustomFieldType;

  @Column({default: false})
  multiEntry: boolean;

  @Column({type: 'integer', nullable: true})
  order: number;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
