import {Entity, PrimaryColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, Column} from 'typeorm';

import {Account} from './account';

@Entity('token')
export class Token {
  @PrimaryColumn()
  id: string;

  @ManyToOne(type => Account, account => account.tokens, {cascade: true})
  account: Account;

  @Column({nullable: true})
  xsrfSalt: string;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
