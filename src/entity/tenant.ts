import {Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, OneToMany} from 'typeorm';

import {Peer} from './peer';
import {Node} from './node';
import {Project} from './project';
import {ShareToken} from './share_token';
import {NodeTemplate} from './node_template';
import {AccountPermission} from './account_permission';
import {CustomFieldDefinition} from './custom_field_definition';
import {EventsLog} from './events_log';
import {UserSyncSetting} from '../lib/tenant';
import {ExternalAuthSetting} from '../lib/tenant';

@Entity('tenant')
export class Tenant {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(type => Peer, peer => peer.tenant)
  peers: Peer[];

  @OneToMany(type => Node, node => node.tenant)
  nodes: Node[];

  @OneToMany(type => NodeTemplate, nodeTemplate => nodeTemplate.tenant)
  nodeTemplates: NodeTemplate[];

  @OneToMany(type => CustomFieldDefinition, customFieldDefinition => customFieldDefinition.tenant)
  customFieldDefinitions: CustomFieldDefinition[];

  @OneToMany(type => Project, project => project.tenant)
  projects: Project[];

  @OneToMany(type => EventsLog, eventLog => eventLog.tenant)
  eventsLog: EventsLog[];

  @Column({unique: true})
  slug: string;

  @Column()
  name: string;

  @OneToMany(type => AccountPermission, accountPermission => accountPermission.tenant)
  accountPermissions: AccountPermission[];

  @OneToMany(type => ShareToken, shareToken => shareToken.tenant)
  shareTokens: ShareToken[];

  @Column({type: 'jsonb', default: {}})
  mapSettings: any;

  @Column({type: 'jsonb', default: {}})
  missionSettings: any;

  @Column({type: 'jsonb', default: {}})
  projectsSettings: any;

  @Column({type: 'jsonb', default: {}})
  userSyncSettings: UserSyncSetting;

  @Column({type: 'jsonb', default: {}})
  externalAuthSettings: ExternalAuthSetting;

  @Column({default: false})
  public: boolean;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
