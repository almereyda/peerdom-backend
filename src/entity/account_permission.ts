import {Entity, ManyToOne, Column, Index, PrimaryColumn} from 'typeorm';

import {Tenant} from './tenant';
import {Account} from './account';

@Entity('account_permission')
export class AccountPermission {
  @PrimaryColumn('uuid', {name: 'tenantId'})
  @ManyToOne(type => Tenant, tenant => tenant.accountPermissions)
  tenant: Tenant;

  @PrimaryColumn('uuid', {name: 'accountId'})
  @ManyToOne(type => Account, account => account.permissions)
  account: Account;

  @Column()
  writeAccess: boolean;
}
