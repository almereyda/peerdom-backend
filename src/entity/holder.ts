import {
  Entity,
  Index,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany,
  JoinColumn
} from 'typeorm';

import {Peer} from './peer';
import {Node} from './node';

@Entity('holder')
@Index(['peer', 'role'], {unique: true})
export class Holder {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Peer, peer => peer.holders, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  peer: Peer;

  @ManyToOne(type => Node, node => node.holders, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  role: Node;

  @Column({nullable: true})
  focus: string;

  @Column({type: 'timestamptz', nullable: true})
  electedUntil: Date;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
