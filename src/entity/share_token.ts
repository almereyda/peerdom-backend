import {Entity, PrimaryColumn, ManyToOne, Column, CreateDateColumn, UpdateDateColumn} from 'typeorm';

import {Tenant} from './tenant';

@Entity('share_token')
export class ShareToken {
  @PrimaryColumn()
  id: string;

  @ManyToOne(type => Tenant, tenant => tenant.shareTokens, {cascade: true})
  tenant: Tenant;

  @Column({default: 'map'})
  app: string;

  @Column()
  valid: boolean;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
