import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
  Index
} from 'typeorm';

import {Tenant} from './tenant';
import {Holder} from './holder';
import {NodeTemplate} from './node_template';
import {CustomField} from './custom_field';

enum NodeType {
  Circle = 'circle',
  Role = 'role'
}

@Entity('node')
@Index(['slug', 'parent'], {unique: true})
export class Node {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Node, parent => parent.children, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  parent: Node;

  @OneToMany(type => Node, node => node.parent)
  children: Node[];

  @ManyToOne(type => Tenant, tenant => tenant.nodes)
  @JoinColumn()
  tenant: Tenant;

  @OneToMany(type => Holder, holder => holder.role)
  holders: Holder[];

  @ManyToOne(type => Node, node => node.representatives)
  @JoinColumn()
  representing: Node;

  @OneToMany(type => Node, node => node.representing)
  representatives: Node[];

  @Column()
  name: string;

  @Column()
  slug: string;

  @ManyToMany(type => CustomField, customFields => customFields.nodes)
  @JoinTable()
  customFields: CustomField[];

  @Column({nullable: true})
  purpose: string;

  @Column({type: 'jsonb', nullable: true})
  policies: any;

  @Column({type: 'jsonb', nullable: true})
  notes: any;

  @Column({nullable: true})
  color: string;

  @Column({
    type: 'enum',
    enum: Object.keys(NodeType).map(k => NodeType[k])
  })
  type: NodeType;

  @Column({type: 'jsonb', nullable: true})
  accountabilities: any;

  @Column({type: 'jsonb', nullable: true})
  domains: any;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;

  @ManyToOne(type => NodeTemplate, nodeTemplate => nodeTemplate.nodes, {nullable: true})
  @JoinColumn()
  template: NodeTemplate;

  @Column({default: false})
  external: boolean;

  @Column({default: false})
  electable: boolean;
}
