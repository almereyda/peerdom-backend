import {Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column} from 'typeorm';
import {Tenant} from './tenant';
import {Account} from './account';

@Entity()
export class EventsLog {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({nullable: false})
  entity: string;

  @ManyToOne(type => Tenant, tenant => tenant.eventsLog)
  @JoinColumn()
  tenant: Tenant;

  // Warning: this need to be set programmatically based on the item createdAt
  @Column({nullable: false})
  createdAt: Date;

  @ManyToOne(type => Account, author => author.eventsLog, {nullable: true})
  @JoinColumn()
  author: Account;

  /* item */
  @Column({type: 'uuid', nullable: false})
  item: any;

  @Column({
    type: 'enum',
    enum: ['insert', 'delete', 'update']
  })
  action: 'insert' | 'delete' | 'update';

  @Column({type: 'jsonb', nullable: false})
  change: any;

  @Column({default: false})
  cascadeTriggered: boolean;

  // tslint:disable-next-line:max-line-length
  /* Value must respect semver. See https://github.com/semver/semver/blob/master/semver.md#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string */
  @Column()
  /* TODO: use validation see https://github.com/typeorm/typeorm/blob/master/docs/validation.md */
  // @Matches(
  // tslint:disable-next-line:max-line-length
  //   /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/i
  // )
  version: string;
}
