import {Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, OneToMany} from 'typeorm';

import {Token} from './token';
import {AccountPermission} from './account_permission';
import {EventsLog} from './events_log';
import {Peer} from './peer';

@Entity('account')
export class Account {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(type => AccountPermission, accountPermission => accountPermission.account)
  permissions: AccountPermission[];

  @OneToMany(type => Token, token => token.account)
  tokens: Token[];

  @OneToMany(type => EventsLog, eventLog => eventLog.author, {nullable: true})
  eventsLog: EventsLog[];

  @Column({unique: true})
  email: string;

  @Column({nullable: true})
  password: string;

  @Column({nullable: true})
  resetPasswordToken: string;

  @Column({type: 'timestamptz', nullable: true})
  resetPasswordExpires: Date;

  @OneToMany(type => Peer, peer => peer.account)
  peers: Peer[];

  @Column({type: 'boolean', default: false})
  activated: boolean;

  @Column({type: 'boolean', default: false})
  useExternalAuth: boolean;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
