import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  OneToOne,
  OneToMany,
  JoinColumn
} from 'typeorm';

import {Peer} from './peer';

@Entity('mission')
export class Mission {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(type => Peer, peer => peer.id, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  peer: Peer;

  @Column()
  text: string;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
