import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn
} from 'typeorm';

import {Tenant} from './tenant';
import {Node} from './node';

@Entity('node_template')
export class NodeTemplate {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Tenant, tenant => tenant.nodeTemplates)
  @JoinColumn()
  tenant: Tenant;

  @OneToMany(type => Node, node => node.template)
  nodes: Node[];

  @Column()
  name: string;

  @Column()
  slug: string;

  @Column({nullable: true})
  purpose: string;

  @Column({type: 'jsonb', nullable: true})
  policies: any;

  @Column({type: 'jsonb', nullable: true})
  notes: any;

  @Column({nullable: true})
  color: string;

  @Column({
    type: 'enum',
    enum: ['circle', 'role']
  })
  type: 'circle' | 'role';

  @Column({type: 'jsonb', nullable: true})
  accountabilities: any;

  @Column({type: 'jsonb', nullable: true})
  domains: any;

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
