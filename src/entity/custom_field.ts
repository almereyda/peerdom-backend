import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  JoinColumn,
  ManyToMany
} from 'typeorm';

import {CustomFieldDefinition} from './custom_field_definition';
import {Node} from './node';

@Entity('custom_field')
export class CustomField {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => CustomFieldDefinition, customFieldDefinition => customFieldDefinition.fields, {
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  definition: CustomFieldDefinition;

  @ManyToMany(type => Node, node => node.customFields, {
    onDelete: 'CASCADE'
  })
  nodes: Node[];

  @Column({type: 'jsonb', nullable: true})
  value: string | string[];

  @CreateDateColumn({type: 'timestamptz'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamptz'})
  updatedAt: Date;
}
