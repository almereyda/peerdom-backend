import {EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent, RemoveEvent} from 'typeorm';
import {
  saveEventLogFromItemInsertEvent,
  saveEventLogFromItemUpdateEvent,
  saveEventLogFromItemRemoveEvent,
  saveEventLogFromCascadeManualRemove
} from '../controllers/itemsLogger';
import {Node} from '../entity/node';

@EventSubscriber()
export class NodeEventsLogSubscriber implements EntitySubscriberInterface<Node> {
  listenTo() {
    return Node;
  }

  async afterInsert(event: InsertEvent<Node>) {
    await saveEventLogFromItemInsertEvent<Node>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterUpdate(event: UpdateEvent<Node>) {
    await saveEventLogFromItemUpdateEvent<Node>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterRemove(event: RemoveEvent<Node>) {
    await saveEventLogFromCascadeManualRemove<Node>(
      event.queryRunner.data.cascadeItems,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
    await saveEventLogFromItemRemoveEvent(event, event.queryRunner.data.tenantId, event.queryRunner.data.accountId);
  }
}
