import {EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent, RemoveEvent} from 'typeorm';
import {
  saveEventLogFromItemInsertEvent,
  saveEventLogFromItemUpdateEvent,
  saveEventLogFromItemRemoveEvent,
  saveEventLogFromCascadeManualRemove
} from '../controllers/itemsLogger';
import {Peer} from '../entity/peer';

@EventSubscriber()
export class PeerEventsLogSubscriber implements EntitySubscriberInterface<Peer> {
  listenTo() {
    return Peer;
  }

  async afterInsert(event: InsertEvent<Peer>) {
    await saveEventLogFromItemInsertEvent<Peer>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterUpdate(event: UpdateEvent<Peer>) {
    await saveEventLogFromItemUpdateEvent<Peer>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterRemove(event: RemoveEvent<Peer>) {
    await saveEventLogFromCascadeManualRemove<Peer>(
      event.queryRunner.data.cascadeItems,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
    await saveEventLogFromItemRemoveEvent(event, event.queryRunner.data.tenantId, event.queryRunner.data.accountId);
  }
}
