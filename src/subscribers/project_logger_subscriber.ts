import {EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent, RemoveEvent} from 'typeorm';
import {
  saveEventLogFromItemInsertEvent,
  saveEventLogFromItemUpdateEvent,
  saveEventLogFromItemRemoveEvent
} from '../controllers/itemsLogger';
import {Project} from '../entity/project';

@EventSubscriber()
export class ProjectEventsLogSubscriber implements EntitySubscriberInterface<Project> {
  listenTo() {
    return Project;
  }

  async afterInsert(event: InsertEvent<Project>) {
    await saveEventLogFromItemInsertEvent<Project>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterUpdate(event: UpdateEvent<Project>) {
    await saveEventLogFromItemUpdateEvent<Project>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterRemove(event: RemoveEvent<Project>) {
    await saveEventLogFromItemRemoveEvent<Project>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }
}
