import {EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent, RemoveEvent} from 'typeorm';
import {
  saveEventLogFromItemInsertEvent,
  saveEventLogFromItemUpdateEvent,
  saveEventLogFromItemRemoveEvent
} from '../controllers/itemsLogger';
import {Holder} from '../entity/holder';

@EventSubscriber()
export class HolderEventsLogSubscriber implements EntitySubscriberInterface<Holder> {
  listenTo() {
    return Holder;
  }

  async afterInsert(event: InsertEvent<Holder>) {
    await saveEventLogFromItemInsertEvent<Holder>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterUpdate(event: UpdateEvent<Holder>) {
    await saveEventLogFromItemUpdateEvent<Holder>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterRemove(event: RemoveEvent<Holder>) {
    // @ts-ignore
    event.entity.peer = event.entity.peer.id;
    // @ts-ignore
    event.entity.role = event.entity.role.id;
    await saveEventLogFromItemRemoveEvent<Holder>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }
}
