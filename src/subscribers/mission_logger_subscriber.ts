import {EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent, RemoveEvent} from 'typeorm';
import {
  saveEventLogFromItemInsertEvent,
  saveEventLogFromItemUpdateEvent,
  saveEventLogFromItemRemoveEvent
} from '../controllers/itemsLogger';
import {Mission} from '../entity/mission';

@EventSubscriber()
export class MissionEventsLogSubscriber implements EntitySubscriberInterface<Mission> {
  listenTo() {
    return Mission;
  }

  async afterInsert(event: InsertEvent<Mission>) {
    await saveEventLogFromItemInsertEvent<Mission>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterUpdate(event: UpdateEvent<Mission>) {
    await saveEventLogFromItemUpdateEvent<Mission>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterRemove(event: RemoveEvent<Mission>) {
    await saveEventLogFromItemRemoveEvent<Mission>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }
}
