import {EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent, RemoveEvent} from 'typeorm';
import {
  saveEventLogFromItemInsertEvent,
  saveEventLogFromItemUpdateEvent,
  saveEventLogFromItemRemoveEvent
} from '../controllers/itemsLogger';
import {NodeTemplate} from '../entity/node_template';

@EventSubscriber()
export class NodeTemplateEventsLogSubscriber implements EntitySubscriberInterface<NodeTemplate> {
  listenTo() {
    return NodeTemplate;
  }

  async afterInsert(event: InsertEvent<NodeTemplate>) {
    await saveEventLogFromItemInsertEvent<NodeTemplate>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterUpdate(event: UpdateEvent<NodeTemplate>) {
    await saveEventLogFromItemUpdateEvent<NodeTemplate>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }

  async afterRemove(event: RemoveEvent<NodeTemplate>) {
    await saveEventLogFromItemRemoveEvent<NodeTemplate>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }
}
