import {EntitySubscriberInterface, EventSubscriber, UpdateEvent} from 'typeorm';
import {saveEventLogFromItemUpdateEvent} from '../controllers/itemsLogger';
import {CustomField} from '../entity/custom_field';

@EventSubscriber()
export class CustomFieldEventsLogSubscriber implements EntitySubscriberInterface<CustomField> {
  listenTo() {
    return CustomField;
  }

  async afterUpdate(event: UpdateEvent<CustomField>) {
    await saveEventLogFromItemUpdateEvent<CustomField>(
      event,
      event.queryRunner.data.tenantId,
      event.queryRunner.data.accountId
    );
  }
}
