import {MigrationInterface, QueryRunner} from "typeorm";

export class CustomFieldOrder1585312511893 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "custom_field_definition" ADD "order" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "custom_field_definition" DROP COLUMN "order"`);
    }

}
