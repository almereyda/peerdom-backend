import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveCoreRoleConcept1561462445604 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "holder" DROP CONSTRAINT "FK_871b47959475f59da2307abd082"`);
        await queryRunner.query(`ALTER TABLE "holder" DROP COLUMN "circleId"`);
        await queryRunner.query(`ALTER TYPE "public"."node_template_type_enum" RENAME TO "node_template_type_enum_old"`);
        await queryRunner.query(`CREATE TYPE "node_template_type_enum" AS ENUM('circle', 'role')`);
        await queryRunner.query(`ALTER TABLE "node_template" ALTER COLUMN "type" TYPE "node_template_type_enum" USING "type"::"text"::"node_template_type_enum"`);
        await queryRunner.query(`DROP TYPE "node_template_type_enum_old"`);
        await queryRunner.query(`ALTER TYPE "public"."node_type_enum" RENAME TO "node_type_enum_old"`);
        await queryRunner.query(`CREATE TYPE "node_type_enum" AS ENUM('circle', 'role')`);
        await queryRunner.query(`ALTER TABLE "node" ALTER COLUMN "type" TYPE "node_type_enum" USING "type"::"text"::"node_type_enum"`);
        await queryRunner.query(`DROP TYPE "node_type_enum_old"`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_eb3ebc92c324e3184b77d53112" ON "holder" ("peerId", "roleId") `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_eb3ebc92c324e3184b77d53112"`);
        await queryRunner.query(`CREATE TYPE "node_type_enum_old" AS ENUM('circle', 'role', 'coreRole')`);
        await queryRunner.query(`ALTER TABLE "node" ALTER COLUMN "type" TYPE "node_type_enum_old" USING "type"::"text"::"node_type_enum_old"`);
        await queryRunner.query(`DROP TYPE "node_type_enum"`);
        await queryRunner.query(`ALTER TYPE "node_type_enum_old" RENAME TO  "node_type_enum"`);
        await queryRunner.query(`CREATE TYPE "node_template_type_enum_old" AS ENUM('circle', 'role', 'coreRole')`);
        await queryRunner.query(`ALTER TABLE "node_template" ALTER COLUMN "type" TYPE "node_template_type_enum_old" USING "type"::"text"::"node_template_type_enum_old"`);
        await queryRunner.query(`DROP TYPE "node_template_type_enum"`);
        await queryRunner.query(`ALTER TYPE "node_template_type_enum_old" RENAME TO  "node_template_type_enum"`);
        await queryRunner.query(`ALTER TABLE "holder" ADD "circleId" uuid`);
        await queryRunner.query(`ALTER TABLE "holder" ADD CONSTRAINT "FK_871b47959475f59da2307abd082" FOREIGN KEY ("circleId") REFERENCES "node"("id") ON DELETE CASCADE`);
    }

}
