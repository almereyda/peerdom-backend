import {MigrationInterface, QueryRunner} from 'typeorm';

export class ShareToken1575548649235 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "share_token" ("id" character varying NOT NULL, "app" character varying NOT NULL DEFAULT 'map', "valid" boolean NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "tenantId" uuid, CONSTRAINT "PK_c4c49ef646f2ba0ae81c1d167e5" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "share_token" ADD CONSTRAINT "FK_a0d88df2a05298340a35a2a6aa5" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "share_token" DROP CONSTRAINT "FK_a0d88df2a05298340a35a2a6aa5"`);
    await queryRunner.query(`DROP TABLE "share_token"`);
  }
}
