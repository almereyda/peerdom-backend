import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveChangeTablesEntities1568383327700 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "holder_change" DROP CONSTRAINT "FK_1ea9df9b29d9f7b07e30520e10e"`);
        await queryRunner.query(`ALTER TABLE "mission_change" DROP CONSTRAINT "FK_80097fc6a1a01bb5148e9b7926f"`);
        await queryRunner.query(`ALTER TABLE "node_change" DROP CONSTRAINT "FK_b0cf0635caf4aabff57691f9335"`);
        await queryRunner.query(`ALTER TABLE "peer_change" DROP CONSTRAINT "FK_aa9ca86de5a4c6971fc2d415fae"`);
        await queryRunner.query(`DROP TABLE "peer_change"`);
        await queryRunner.query(`DROP TABLE "mission_change"`);
        await queryRunner.query(`DROP TABLE "holder_change"`);
        await queryRunner.query(`DROP TABLE "node_change"`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "holder_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "holderId" uuid, CONSTRAINT "PK_556487a330ba6e25ea801cd9cb6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "mission_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "missionId" uuid, CONSTRAINT "PK_236acb0ba8f75cd76ddce70aa12" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "node_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "nodeId" uuid, CONSTRAINT "PK_9cad3eb222962e0813ac68e1bb1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "peer_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "peerId" uuid, CONSTRAINT "PK_98fec452815dec709947304b556" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "peer_change" ADD CONSTRAINT "FK_aa9ca86de5a4c6971fc2d415fae" FOREIGN KEY ("peerId") REFERENCES "peer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "node_change" ADD CONSTRAINT "FK_b0cf0635caf4aabff57691f9335" FOREIGN KEY ("nodeId") REFERENCES "node"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "mission_change" ADD CONSTRAINT "FK_80097fc6a1a01bb5148e9b7926f" FOREIGN KEY ("missionId") REFERENCES "mission"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "holder_change" ADD CONSTRAINT "FK_1ea9df9b29d9f7b07e30520e10e" FOREIGN KEY ("holderId") REFERENCES "holder"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
