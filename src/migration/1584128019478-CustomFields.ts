import {MigrationInterface, QueryRunner, EntityManager, Repository, IsNull, Not, In} from 'typeorm';
import {Tenant} from '../entity/tenant';
import {Node} from '../entity/node';
import {NodeTemplate} from '../entity/node_template';
import {CustomField} from '../entity/custom_field';
import {CustomFieldDefinition} from '../entity/custom_field_definition';

// defines the maximum number of entities to be updated at once
const TYPEORM_CHUNK = 250;

const definitionData = {
  purpose: {
    name: 'Purpose',
    translation: {
      en: 'Purpose',
      de: 'Ziel',
      fr: "Raison d'être",
      nl: 'Doel',
      sv: 'Avsikt'
    }
  },
  accountabilities: {
    name: 'Accountabilities',
    translation: {
      en: 'Accountabilities',
      de: 'Verantwortlichkeiten',
      fr: 'Responsabilités',
      nl: 'Verantwoordelijkheden',
      sv: 'Ansvarigheter'
    },
    multiEntry: true
  },
  domains: {
    name: 'Domains',
    translation: {
      en: 'Domains',
      de: 'Bereiche',
      fr: 'Domaines',
      nl: 'Domein',
      sv: 'Domäner'
    },
    multiEntry: true
  },
  policies: {
    name: 'Policies',
    translation: {
      en: 'Policies',
      de: 'Grundsätze',
      fr: 'Politiques',
      nl: 'Beleid',
      sv: 'Politik'
    },
    multiEntry: true
  },
  notes: {
    name: 'Notes',
    translation: {
      en: 'Notes',
      de: 'Notizen',
      fr: 'Notes',
      nl: 'Aantekeningen',
      sv: 'Anteckningar'
    },
    type: 'textarea'
  }
};

export class CustomFields1584128019478 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "custom_field_definition_type_enum" AS ENUM('text', 'textarea', 'email', 'link', 'date')`
    );
    await queryRunner.query(
      `CREATE TABLE "custom_field_definition" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "translation" jsonb, "type" "custom_field_definition_type_enum" NOT NULL DEFAULT 'text', "multiEntry" boolean NOT NULL DEFAULT false, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "tenantId" uuid, CONSTRAINT "PK_f75626eda05d6b2c0671422f0c8" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "custom_field" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "value" jsonb, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "definitionId" uuid, CONSTRAINT "PK_70c7eb2dfb5b81c051a6ba3ace8" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "node_custom_fields_custom_field" ("nodeId" uuid NOT NULL, "customFieldId" uuid NOT NULL, CONSTRAINT "PK_62dac14284477d97593a86829cf" PRIMARY KEY ("nodeId", "customFieldId"))`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_ca818c4ec3e1133577b0dfd96c" ON "node_custom_fields_custom_field" ("nodeId") `
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_b73c69ab641c5232334fc2f7b5" ON "node_custom_fields_custom_field" ("customFieldId") `
    );
    await queryRunner.query(
      `ALTER TABLE "custom_field_definition" ADD CONSTRAINT "FK_d5eb99c6bdefa0c793b2341b474" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "custom_field" ADD CONSTRAINT "FK_ba131e4452bf73963136c8c8347" FOREIGN KEY ("definitionId") REFERENCES "custom_field_definition"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "node_custom_fields_custom_field" ADD CONSTRAINT "FK_ca818c4ec3e1133577b0dfd96c9" FOREIGN KEY ("nodeId") REFERENCES "node"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "node_custom_fields_custom_field" ADD CONSTRAINT "FK_b73c69ab641c5232334fc2f7b54" FOREIGN KEY ("customFieldId") REFERENCES "custom_field"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );

    await migrateFields(queryRunner.manager);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "node_custom_fields_custom_field" DROP CONSTRAINT "FK_b73c69ab641c5232334fc2f7b54"`
    );
    await queryRunner.query(
      `ALTER TABLE "node_custom_fields_custom_field" DROP CONSTRAINT "FK_ca818c4ec3e1133577b0dfd96c9"`
    );
    await queryRunner.query(`ALTER TABLE "custom_field" DROP CONSTRAINT "FK_ba131e4452bf73963136c8c8347"`);
    await queryRunner.query(`ALTER TABLE "custom_field_definition" DROP CONSTRAINT "FK_d5eb99c6bdefa0c793b2341b474"`);
    await queryRunner.query(`DROP INDEX "IDX_b73c69ab641c5232334fc2f7b5"`);
    await queryRunner.query(`DROP INDEX "IDX_ca818c4ec3e1133577b0dfd96c"`);
    await queryRunner.query(`DROP TABLE "node_custom_fields_custom_field"`);
    await queryRunner.query(`DROP TABLE "custom_field"`);
    await queryRunner.query(`DROP TABLE "custom_field_definition"`);
    await queryRunner.query(`DROP TYPE "custom_field_definition_type_enum"`);
  }
}

const migrateFields = async (manager: EntityManager) => {
  try {
    console.log('Connecting to DB...');
    const tenantRepository = manager.getRepository(Tenant);
    const nodeRepository = manager.getRepository(Node);
    const nodeTemplateRepository = manager.getRepository(NodeTemplate);
    const cfRepository = manager.getRepository(CustomField);
    const cfdRepository = manager.getRepository(CustomFieldDefinition);

    console.log('Creating definitions...');
    const definitions = await createDefinitions(tenantRepository, cfdRepository);
    console.log('Creating custom fields...');
    await createCustomFields(definitions, nodeRepository, cfRepository);
    console.log('Creating custom fields for templates...');
    await createTemplateCustomFields(definitions, nodeRepository, nodeTemplateRepository, cfRepository);
    console.log('Cleaning up empty fields...');
    await cleanUpEmptyFields(cfRepository);
  } catch (error) {
    console.log(error);
  }
};

const createDefinitions = async (
  tenantRepository: Repository<Tenant>,
  cfdRepository: Repository<CustomFieldDefinition>
): Promise<any> => {
  const tenants: any[] = await tenantRepository.find({
    select: ['id']
  });

  const definitions = {};
  for (const tenant of tenants) {
    definitions[tenant.id] = {};
    definitions[tenant.id].purpose = await cfdRepository.save({
      ...(definitionData.purpose as CustomFieldDefinition),
      tenant
    });
    definitions[tenant.id].accountabilities = await cfdRepository.save({
      ...(definitionData.accountabilities as CustomFieldDefinition),
      tenant
    });
    definitions[tenant.id].domains = await cfdRepository.save({
      ...(definitionData.domains as CustomFieldDefinition),
      tenant
    });
    definitions[tenant.id].policies = await cfdRepository.save({
      ...(definitionData.policies as CustomFieldDefinition),
      tenant
    });
    definitions[tenant.id].notes = await cfdRepository.save({
      ...(definitionData.notes as CustomFieldDefinition),
      tenant
    });
  }

  return definitions;
};

const createCustomFields = async (
  definitions: any,
  nodeRepository: Repository<Node>,
  cfRepository: Repository<CustomField>
) => {
  const nodes: any[] = await nodeRepository.find({
    select: ['id', 'purpose', 'accountabilities', 'domains', 'policies', 'notes'],
    where: {
      template: IsNull()
    },
    loadRelationIds: {
      relations: ['tenant']
    }
  });

  let purposeFields = nodes.map(n => {
    return {
      value: n.purpose,
      definition: definitions[n.tenant.id].purpose
    };
  });
  purposeFields = await cfRepository.save(purposeFields, {
    chunk: TYPEORM_CHUNK
  });

  let accountabilityFields = nodes.map(n => {
    return {
      value: n.accountabilities,
      definition: definitions[n.tenant.id].accountabilities
    };
  });
  accountabilityFields = await cfRepository.save(accountabilityFields, {
    chunk: TYPEORM_CHUNK
  });

  let domainFields = nodes.map(n => {
    return {
      value: n.domains,
      definition: definitions[n.tenant.id].domains
    };
  });
  domainFields = await cfRepository.save(domainFields, {
    chunk: TYPEORM_CHUNK
  });

  let policyFields = nodes.map(n => {
    return {
      value: n.policies,
      definition: definitions[n.tenant.id].policies
    };
  });
  policyFields = await cfRepository.save(policyFields, {
    chunk: TYPEORM_CHUNK
  });

  let noteFields = nodes.map(n => {
    return {
      value: n.notes,
      definition: definitions[n.tenant.id].notes
    };
  });
  noteFields = await cfRepository.save(noteFields, {
    chunk: TYPEORM_CHUNK
  });

  const nodePatches: any[] = [];
  for (let i = 0; i < nodes.length; i++) {
    const nodePatch = {
      id: nodes[i].id,
      customFields: [purposeFields[i], accountabilityFields[i], domainFields[i], policyFields[i], noteFields[i]]
    };
    nodePatches.push(nodePatch);
  }
  await nodeRepository.save(nodePatches, {
    chunk: TYPEORM_CHUNK
  });
};

const createTemplateCustomFields = async (
  definitions: any,
  nodeRepository: Repository<Node>,
  nodeTemplateRepository: Repository<NodeTemplate>,
  cfRepository: Repository<CustomField>
) => {
  const templateNodes: any[] = await nodeRepository.find({
    select: ['id', 'template'],
    where: {
      template: Not(IsNull())
    },
    relations: ['template']
  });
  const templates: any[] = await nodeTemplateRepository.find({
    select: ['id', 'tenant', 'purpose', 'accountabilities', 'domains', 'policies', 'notes'],
    loadRelationIds: {
      relations: ['tenant']
    }
  });

  let nodePatches = [];
  await Promise.all(
    templates.map(async template => {
      let customFields = [
        {
          value: template.purpose,
          definition: definitions[template.tenant.id].purpose
        },
        {
          value: template.accountabilities,
          definition: definitions[template.tenant.id].accountabilities
        },
        {
          value: template.domains,
          definition: definitions[template.tenant.id].domains
        },
        {
          value: template.policies,
          definition: definitions[template.tenant.id].policies
        },
        {
          value: template.notes,
          definition: definitions[template.tenant.id].notes
        }
      ];
      customFields = await cfRepository.save(customFields);
      const nodesFromTemplate = templateNodes.filter(n => n.template && n.template.id === template.id);
      if (nodesFromTemplate && nodesFromTemplate.length) {
        const templateNodePatches = nodesFromTemplate.map(n => ({
          id: n.id,
          customFields
        }));
        nodePatches = [...nodePatches, ...templateNodePatches];
      }
    })
  );
  await nodeRepository.save(nodePatches, {
    chunk: TYPEORM_CHUNK
  });
};

const cleanUpEmptyFields = async (cfRepository: Repository<CustomField>) => {
  let fieldsToRemove = await cfRepository.find({
    where: {
      value: IsNull()
    }
  });
  if (fieldsToRemove && fieldsToRemove.length) {
    await cfRepository.remove(fieldsToRemove, {
      chunk: TYPEORM_CHUNK
    });
  }

  fieldsToRemove = await cfRepository.find({
    where: {
      value: In([[], '[]'])
    }
  });
  if (fieldsToRemove && fieldsToRemove.length) {
    await cfRepository.remove(fieldsToRemove, {
      chunk: TYPEORM_CHUNK
    });
  }
};
