import {MigrationInterface, QueryRunner, Repository, EntityManager} from 'typeorm';
import {Peer} from '../entity/peer';
import * as uuid from 'uuid/v4';
import * as packageJson from '../../package.json';
import {EventsLog} from '../entity/events_log';
import {Mission} from '../entity/mission';
import {Tenant} from '../entity/tenant';
import {NodeTemplate} from '../entity/node_template';
import {Node} from '../entity/node';
import {Holder} from '../entity/holder';

export class AddEventsLog1568383346177 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "events_log_action_enum" AS ENUM('insert', 'delete', 'update')`);
    await queryRunner.query(
      `CREATE TABLE "events_log" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "entity" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL, "item" uuid NOT NULL, "action" "events_log_action_enum" NOT NULL, "change" jsonb NOT NULL, "cascadeTriggered" boolean NOT NULL DEFAULT false, "version" character varying NOT NULL, "tenantId" uuid, "authorId" uuid, CONSTRAINT "PK_468512023f59a5c2f92da645ab8" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "events_log" ADD CONSTRAINT "FK_d93a9cd26fd21362d9f100b6020" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "events_log" ADD CONSTRAINT "FK_671b9cf23589abee86543d61283" FOREIGN KEY ("authorId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await manuallyInsertEventsLogs(queryRunner.manager);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "events_log" DROP CONSTRAINT "FK_671b9cf23589abee86543d61283"`);
    await queryRunner.query(`ALTER TABLE "events_log" DROP CONSTRAINT "FK_d93a9cd26fd21362d9f100b6020"`);
    await queryRunner.query(`DROP TABLE "events_log"`);
    await queryRunner.query(`DROP TYPE "events_log_action_enum"`);
  }
}

interface Item {
  tenant: Tenant;
  createdAt: Date;
  id: string;
  /* rest of prop will be put into the `change` EventsLog prop */
}

const logPeersEvents = async (logEventsRepository: Repository<EventsLog>, peerRepository: Repository<Peer>) => {
  const peers = await peerRepository.find({
    // A subset selection is required
    // Otherwise, the TS entities would not be in sync with the DB tables
    // and the migration would fail
    select: ['id', 'displayName', 'slug', 'createdAt', 'updatedAt'],
    loadRelationIds: {
      relations: ['tenant']
    }
  });

  await Promise.all(
    peers.map(async peer => {
      console.log('Peer ' + peer.id);
      await logItemEvents(peer, logEventsRepository, 'peer');
    })
  );
};

const logMissionsEvents = async (
  logEventsRepository: Repository<EventsLog>,
  missionRepository: Repository<Mission>,
  peerRepository: Repository<Peer>
) => {
  const missions = await missionRepository.find({
    select: ['id', 'text', 'updatedAt', 'createdAt'],
    loadRelationIds: {
      relations: ['peer']
    }
  });

  await Promise.all(
    missions.map(async mission => {
      console.log('Mission ' + mission.id);
      const peer = await peerRepository.findOne({
        select: ['id', 'displayName', 'slug', 'createdAt', 'updatedAt'],
        loadRelationIds: {
          relations: ['tenant']
        },
        where: {id: mission.peer.id}
      });
      const missionItem: Mission & Item = {
        ...mission,
        tenant: peer.tenant
      };
      delete missionItem.peer;
      await logItemEvents(missionItem, logEventsRepository, 'mission');
    })
  );
};

const logNodeTemplateEvents = async (
  logEventsRepository: Repository<EventsLog>,
  nodeTemplateRepository: Repository<NodeTemplate>
) => {
  const nodeTemplates = await nodeTemplateRepository.find({
    select: [
      'id',
      'name',
      'slug',
      'purpose',
      'policies',
      'notes',
      'color',
      'type',
      'accountabilities',
      'domains',
      'createdAt',
      'updatedAt'
    ],
    loadRelationIds: {
      relations: ['tenant']
    }
  });
  // The order in which we load the nodes doesn't depend on the tree structure
  // as we take back the createdAt of the node itself
  await Promise.all(
    nodeTemplates.map(async nodeTemplate => {
      console.log('Node Template ' + nodeTemplate.id);
      await logItemEvents(nodeTemplate, logEventsRepository, 'node_template');
    })
  );
};

const logNodeEvents = async (logEventsRepository: Repository<EventsLog>, nodeRepository: Repository<Node>) => {
  const nodes = await nodeRepository.find({
    // A subset selection is required
    // Otherwise, the TS entities would not be in sync with the DB tables
    // and the migration would fail
    select: [
      'id',
      'name',
      'slug',
      'purpose',
      'policies',
      'notes',
      'color',
      'type',
      'accountabilities',
      'domains',
      'createdAt',
      'updatedAt'
    ],
    loadRelationIds: {
      relations: ['tenant']
    }
  });

  await Promise.all(
    nodes.map(async node => {
      console.log('Node ' + node.id);
      await logItemEvents(node, logEventsRepository, 'node');
    })
  );
};

const logHolderEvents = async (
  logEventsRepository: Repository<EventsLog>,
  holderRepository: Repository<Holder>,
  peerRepository: Repository<Peer>
) => {
  const holders = await holderRepository.find({
    select: ['id', 'focus', 'electedUntil', 'createdAt', 'updatedAt'],
    loadRelationIds: {
      relations: ['peer', 'role']
    }
  });

  await Promise.all(
    holders.map(async holder => {
      console.log('Holder ' + holder.id);
      const peer = await peerRepository.findOne({
        select: ['id', 'displayName', 'slug', 'createdAt', 'updatedAt'],
        loadRelationIds: {
          relations: ['tenant']
        },
        where: {id: holder.peer.id}
      });
      const holderItem: Holder & Item = {
        ...holder,
        tenant: peer.tenant
      };
      delete holderItem.peer;
      await logItemEvents(holderItem, logEventsRepository, 'holder');
    })
  );
};

const logItemEvents = async (item: Item, logEventsRepository: Repository<EventsLog>, entity: string) => {
  /* TODO: determine if really need to do deep copy */
  const itemDuplicated = JSON.parse(JSON.stringify(item));
  const itemLog: EventsLog = {
    id: uuid(),
    entity: entity,
    action: 'insert',
    author: null,
    change: null,
    tenant: itemDuplicated.tenant,
    version: packageJson.version,
    createdAt: itemDuplicated.createdAt,
    item: itemDuplicated.id,
    cascadeTriggered: false
  };

  delete itemDuplicated.id;
  delete itemDuplicated.createdAt;
  delete itemDuplicated.tenant;

  // We on purpose let the null as the change prop of an insert should contain everything
  itemLog.change = itemDuplicated;

  await logEventsRepository.save(itemLog);
};

const manuallyInsertEventsLogs = async (manager: EntityManager) => {
  try {
    console.log('Connecting to DB...');
    const eventsLogRepository = await manager.getRepository(EventsLog);
    const peerRepository = await manager.getRepository(Peer);
    const missionRepository = await manager.getRepository(Mission);
    const nodeTemplateRepository = await manager.getRepository(NodeTemplate);
    const nodeRepository = manager.getRepository(Node);
    const holderRepository = manager.getRepository(Holder);

    try {
      console.log('Reading the database...');
      console.log('========== LOG PEERS EVENTS==========');
      await logPeersEvents(eventsLogRepository, peerRepository);
      console.log('========== LOG MISSION EVENTS==========');
      await logMissionsEvents(eventsLogRepository, missionRepository, peerRepository);
      console.log('========== LOG NODE TEMPLATE EVENTS==========');
      await logNodeTemplateEvents(eventsLogRepository, nodeTemplateRepository);
      console.log('========== LOG NODE EVENTS==========');
      await logNodeEvents(eventsLogRepository, nodeRepository);
      console.log('========== LOG HOLDER EVENTS==========');
      await logHolderEvents(eventsLogRepository, holderRepository, peerRepository);
    } catch (error) {
      console.log(error);
    }
  } catch (error) {
    console.log(error);
  }
};
