import {MigrationInterface, QueryRunner} from "typeorm";

export class AvatarUrl1585642256352 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "peer" ADD "avatarUrl" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "peer" DROP COLUMN "avatarUrl"`);
    }

}
