import {MigrationInterface, QueryRunner} from "typeorm";

export class AddSlugNodePeer1561542027705 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "node_template" ADD "slug" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "node" ADD "slug" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "peer" ADD "slug" character varying NOT NULL`);
        await queryRunner.query(`DROP INDEX "IDX_46bc5eb9ec91a2b4d7d5a8cd8b"`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_49004deb9df44d27861f99f20c" ON "node" ("slug", "parentId") `);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_822eacd9effd186e71be810c91" ON "peer" ("slug", "tenantId") `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_822eacd9effd186e71be810c91"`);
        await queryRunner.query(`DROP INDEX "IDX_49004deb9df44d27861f99f20c"`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_46bc5eb9ec91a2b4d7d5a8cd8b" ON "peer" ("displayName", "tenantId") `);
        await queryRunner.query(`ALTER TABLE "peer" DROP COLUMN "slug"`);
        await queryRunner.query(`ALTER TABLE "node" DROP COLUMN "slug"`);
        await queryRunner.query(`ALTER TABLE "node_template" DROP COLUMN "slug"`);
    }

}
