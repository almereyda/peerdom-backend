import {MigrationInterface, QueryRunner} from "typeorm";

export class Projects1586976771539 implements MigrationInterface {
    name = 'Projects1586976771539'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "project" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "slug" character varying NOT NULL, "description" character varying, "notes" character varying, "externalUrl" character varying, "archived" boolean NOT NULL DEFAULT false, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "parentId" uuid, "tenantId" uuid, CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_5ca538993f2d7ea3580e75c38f" ON "project" ("slug", "tenantId") `, undefined);
        await queryRunner.query(`CREATE TABLE "project_holders_holder" ("projectId" uuid NOT NULL, "holderId" uuid NOT NULL, CONSTRAINT "PK_c8f4a61a98ea33d12b69b27dc96" PRIMARY KEY ("projectId", "holderId"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_3fa1a92a43b88ba666f80e7869" ON "project_holders_holder" ("projectId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_e842652208ca4363392f685aeb" ON "project_holders_holder" ("holderId") `, undefined);
        await queryRunner.query(`ALTER TABLE "tenant" ADD "projectsSettings" jsonb NOT NULL DEFAULT '{}'`, undefined);
        await queryRunner.query(`ALTER TABLE "project" ADD CONSTRAINT "FK_972cc84102e4234fb489536fccf" FOREIGN KEY ("parentId") REFERENCES "project"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "project" ADD CONSTRAINT "FK_710135bfa3254e493e6ebe683d0" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "project_holders_holder" ADD CONSTRAINT "FK_3fa1a92a43b88ba666f80e78697" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "project_holders_holder" ADD CONSTRAINT "FK_e842652208ca4363392f685aebe" FOREIGN KEY ("holderId") REFERENCES "holder"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_holders_holder" DROP CONSTRAINT "FK_e842652208ca4363392f685aebe"`, undefined);
        await queryRunner.query(`ALTER TABLE "project_holders_holder" DROP CONSTRAINT "FK_3fa1a92a43b88ba666f80e78697"`, undefined);
        await queryRunner.query(`ALTER TABLE "project" DROP CONSTRAINT "FK_710135bfa3254e493e6ebe683d0"`, undefined);
        await queryRunner.query(`ALTER TABLE "project" DROP CONSTRAINT "FK_972cc84102e4234fb489536fccf"`, undefined);
        await queryRunner.query(`ALTER TABLE "tenant" DROP COLUMN "projectsSettings"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_e842652208ca4363392f685aeb"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_3fa1a92a43b88ba666f80e7869"`, undefined);
        await queryRunner.query(`DROP TABLE "project_holders_holder"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_5ca538993f2d7ea3580e75c38f"`, undefined);
        await queryRunner.query(`DROP TABLE "project"`, undefined);
    }

}
