import {MigrationInterface, QueryRunner} from "typeorm";

export class All1553138212528 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "node_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "nodeId" uuid, CONSTRAINT "PK_9cad3eb222962e0813ac68e1bb1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "node_type_enum" AS ENUM('circle', 'role', 'coreRole')`);
        await queryRunner.query(`CREATE TABLE "node" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "purpose" character varying, "policies" jsonb, "notes" jsonb, "color" character varying, "type" "node_type_enum" NOT NULL, "accountabilities" jsonb, "domains" jsonb, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "parentId" uuid, "tenantId" uuid, CONSTRAINT "PK_8c8caf5f29d25264abe9eaf94dd" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "holder_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "holderId" uuid, CONSTRAINT "PK_556487a330ba6e25ea801cd9cb6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "holder" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "focus" character varying, "electedUntil" TIMESTAMP, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "peerId" uuid, "roleId" uuid, "circleId" uuid, CONSTRAINT "PK_8266ed18d931b168de2723ad322" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_be7026ea7ef37369eb594fedd7" ON "holder"("peerId", "roleId", "circleId") `);
        await queryRunner.query(`CREATE TABLE "mission_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "missionId" uuid, CONSTRAINT "PK_236acb0ba8f75cd76ddce70aa12" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "mission" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "text" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "peerId" uuid, CONSTRAINT "REL_dffed21f8b1720f37ff7d1f12b" UNIQUE ("peerId"), CONSTRAINT "PK_54f1391034bc7dd30666dee0d4c" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "peer_change" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "field" character varying NOT NULL, "before" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "peerId" uuid, CONSTRAINT "PK_98fec452815dec709947304b556" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "peer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "displayName" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "tenantId" uuid, CONSTRAINT "PK_3a3bede69c11e056079aaece6db" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_46bc5eb9ec91a2b4d7d5a8cd8b" ON "peer"("displayName", "tenantId") `);
        await queryRunner.query(`CREATE TABLE "tenant" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "mapSettings" jsonb, "missionSettings" jsonb, "matomoSettings" jsonb, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_56211336b5ff35fd944f2259173" UNIQUE ("name"), CONSTRAINT "PK_da8c6efd67bb301e810e56ac139" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "token" ("id" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "accountId" uuid, CONSTRAINT "PK_82fae97f905930df5d62a702fc9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "account" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "email" character varying NOT NULL, "password" character varying, "resetPasswordToken" character varying, "resetPasswordExpires" TIMESTAMP, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_4c8f96ccf523e9a3faefd5bdd4c" UNIQUE ("email"), CONSTRAINT "PK_54115ee388cdb6d86bb4bf5b2ea" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "account_permission" ("tenantId" uuid NOT NULL, "accountId" uuid NOT NULL, "writeAccess" boolean NOT NULL, CONSTRAINT "PK_a2ebfc791aa67eed470ff1444ce" PRIMARY KEY ("tenantId", "accountId"))`);
        await queryRunner.query(`ALTER TABLE "node_change" ADD CONSTRAINT "FK_b0cf0635caf4aabff57691f9335" FOREIGN KEY ("nodeId") REFERENCES "node"("id")`);
        await queryRunner.query(`ALTER TABLE "node" ADD CONSTRAINT "FK_ba001b660671bf4233abd7e7955" FOREIGN KEY ("parentId") REFERENCES "node"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "node" ADD CONSTRAINT "FK_a92995d1d191fcd6b94e3bbb07e" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id")`);
        await queryRunner.query(`ALTER TABLE "holder_change" ADD CONSTRAINT "FK_1ea9df9b29d9f7b07e30520e10e" FOREIGN KEY ("holderId") REFERENCES "holder"("id")`);
        await queryRunner.query(`ALTER TABLE "holder" ADD CONSTRAINT "FK_7612c497489bd184520d19f52b1" FOREIGN KEY ("peerId") REFERENCES "peer"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "holder" ADD CONSTRAINT "FK_e47cfb235597b313ee03cf7df87" FOREIGN KEY ("roleId") REFERENCES "node"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "holder" ADD CONSTRAINT "FK_871b47959475f59da2307abd082" FOREIGN KEY ("circleId") REFERENCES "node"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "mission_change" ADD CONSTRAINT "FK_80097fc6a1a01bb5148e9b7926f" FOREIGN KEY ("missionId") REFERENCES "mission"("id")`);
        await queryRunner.query(`ALTER TABLE "mission" ADD CONSTRAINT "FK_dffed21f8b1720f37ff7d1f12b0" FOREIGN KEY ("peerId") REFERENCES "peer"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "peer_change" ADD CONSTRAINT "FK_aa9ca86de5a4c6971fc2d415fae" FOREIGN KEY ("peerId") REFERENCES "peer"("id")`);
        await queryRunner.query(`ALTER TABLE "peer" ADD CONSTRAINT "FK_63d4b11d8bbefeab2a784d6839f" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "token" ADD CONSTRAINT "FK_7c9a1a6d90661d190ed278592c0" FOREIGN KEY ("accountId") REFERENCES "account"("id")`);
        await queryRunner.query(`ALTER TABLE "account_permission" ADD CONSTRAINT "FK_5e6c7ed823ca0ab9fb6c11b8919" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id")`);
        await queryRunner.query(`ALTER TABLE "account_permission" ADD CONSTRAINT "FK_67c76018a72dcddb3320388a442" FOREIGN KEY ("accountId") REFERENCES "account"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "account_permission" DROP CONSTRAINT "FK_67c76018a72dcddb3320388a442"`);
        await queryRunner.query(`ALTER TABLE "account_permission" DROP CONSTRAINT "FK_5e6c7ed823ca0ab9fb6c11b8919"`);
        await queryRunner.query(`ALTER TABLE "token" DROP CONSTRAINT "FK_7c9a1a6d90661d190ed278592c0"`);
        await queryRunner.query(`ALTER TABLE "peer" DROP CONSTRAINT "FK_63d4b11d8bbefeab2a784d6839f"`);
        await queryRunner.query(`ALTER TABLE "peer_change" DROP CONSTRAINT "FK_aa9ca86de5a4c6971fc2d415fae"`);
        await queryRunner.query(`ALTER TABLE "mission" DROP CONSTRAINT "FK_dffed21f8b1720f37ff7d1f12b0"`);
        await queryRunner.query(`ALTER TABLE "mission_change" DROP CONSTRAINT "FK_80097fc6a1a01bb5148e9b7926f"`);
        await queryRunner.query(`ALTER TABLE "holder" DROP CONSTRAINT "FK_871b47959475f59da2307abd082"`);
        await queryRunner.query(`ALTER TABLE "holder" DROP CONSTRAINT "FK_e47cfb235597b313ee03cf7df87"`);
        await queryRunner.query(`ALTER TABLE "holder" DROP CONSTRAINT "FK_7612c497489bd184520d19f52b1"`);
        await queryRunner.query(`ALTER TABLE "holder_change" DROP CONSTRAINT "FK_1ea9df9b29d9f7b07e30520e10e"`);
        await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "FK_a92995d1d191fcd6b94e3bbb07e"`);
        await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "FK_ba001b660671bf4233abd7e7955"`);
        await queryRunner.query(`ALTER TABLE "node_change" DROP CONSTRAINT "FK_b0cf0635caf4aabff57691f9335"`);
        await queryRunner.query(`DROP TABLE "account_permission"`);
        await queryRunner.query(`DROP TABLE "account"`);
        await queryRunner.query(`DROP TABLE "token"`);
        await queryRunner.query(`DROP TABLE "tenant"`);
        await queryRunner.query(`DROP INDEX "IDX_46bc5eb9ec91a2b4d7d5a8cd8b"`);
        await queryRunner.query(`DROP TABLE "peer"`);
        await queryRunner.query(`DROP TABLE "peer_change"`);
        await queryRunner.query(`DROP TABLE "mission"`);
        await queryRunner.query(`DROP TABLE "mission_change"`);
        await queryRunner.query(`DROP INDEX "IDX_be7026ea7ef37369eb594fedd7"`);
        await queryRunner.query(`DROP TABLE "holder"`);
        await queryRunner.query(`DROP TABLE "holder_change"`);
        await queryRunner.query(`DROP TABLE "node"`);
        await queryRunner.query(`DROP TYPE "node_type_enum"`);
        await queryRunner.query(`DROP TABLE "node_change"`);
    }

}
