import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserSyncSettings1567103680873 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "tenant" ADD "userSyncSettings" jsonb NOT NULL DEFAULT '{}'`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "tenant" DROP COLUMN "userSyncSettings"`);
    }

}
