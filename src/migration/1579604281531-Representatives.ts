import {MigrationInterface, QueryRunner} from 'typeorm';

export class Representatives1579604281531 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "node" ADD "representingId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "node" ADD CONSTRAINT "FK_f2d35dac4e41a3f6ec715968626" FOREIGN KEY ("representingId") REFERENCES "node"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "FK_f2d35dac4e41a3f6ec715968626"`);
    await queryRunner.query(`ALTER TABLE "node" DROP COLUMN "representingId"`);
  }
}
