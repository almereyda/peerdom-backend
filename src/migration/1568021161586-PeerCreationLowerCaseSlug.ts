import {MigrationInterface, QueryRunner} from "typeorm";

export class PeerCreationLowerCaseSlug1568021161586 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`UPDATE "peer" SET "slug"=lower("slug")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
