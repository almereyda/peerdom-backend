import {MigrationInterface, QueryRunner} from "typeorm";

export class ExternalNodes1574769012758 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "node" ADD "external" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "node" DROP COLUMN "external"`);
    }

}
