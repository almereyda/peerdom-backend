import {MigrationInterface, QueryRunner} from "typeorm";

export class Miscellaneous1558521795392 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "tenant" ADD "slug" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tenant" ADD CONSTRAINT "UQ_abfd243f7bd832e806d19c5a919" UNIQUE ("slug")`);
        await queryRunner.query(`ALTER TABLE "tenant" ADD "public" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "tenant" DROP CONSTRAINT "UQ_56211336b5ff35fd944f2259173"`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "mapSettings" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "mapSettings" SET DEFAULT '{}'`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "missionSettings" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "missionSettings" SET DEFAULT '{}'`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "matomoSettings" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "matomoSettings" SET DEFAULT '{}'`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "matomoSettings" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "matomoSettings" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "missionSettings" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "missionSettings" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "mapSettings" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "mapSettings" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tenant" ADD CONSTRAINT "UQ_56211336b5ff35fd944f2259173" UNIQUE ("name")`);
        await queryRunner.query(`ALTER TABLE "tenant" DROP COLUMN "public"`);
        await queryRunner.query(`ALTER TABLE "tenant" DROP CONSTRAINT "UQ_abfd243f7bd832e806d19c5a919"`);
        await queryRunner.query(`ALTER TABLE "tenant" DROP COLUMN "slug"`);
    }

}
