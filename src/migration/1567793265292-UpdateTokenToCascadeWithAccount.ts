import {MigrationInterface, QueryRunner} from 'typeorm';

export class UpdateTokenToCascadeWithAccount1567793265292 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "token" DROP CONSTRAINT "FK_7c9a1a6d90661d190ed278592c0"`);
    await queryRunner.query(
      `ALTER TABLE "token" ADD CONSTRAINT "FK_7c9a1a6d90661d190ed278592c0" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE CASCADE`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "token" DROP CONSTRAINT "FK_7c9a1a6d90661d190ed278592c0"`);
    await queryRunner.query(
      `ALTER TABLE "token" ADD CONSTRAINT "FK_7c9a1a6d90661d190ed278592c0" FOREIGN KEY ("accountId") REFERENCES "account"("id")`
    );
  }
}
