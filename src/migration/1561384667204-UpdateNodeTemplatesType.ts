import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateNodeTemplatesType1561384667204 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TYPE "public"."node_template_type_enum" RENAME TO "node_template_type_enum_old"`);
        await queryRunner.query(`CREATE TYPE "node_template_type_enum" AS ENUM('circle', 'role', 'coreRole')`);
        await queryRunner.query(`ALTER TABLE "node_template" ALTER COLUMN "type" TYPE "node_template_type_enum" USING "type"::"text"::"node_template_type_enum"`);
        await queryRunner.query(`DROP TYPE "node_template_type_enum_old"`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "node_template_type_enum_old" AS ENUM('coreRole')`);
        await queryRunner.query(`ALTER TABLE "node_template" ALTER COLUMN "type" TYPE "node_template_type_enum_old" USING "type"::"text"::"node_template_type_enum_old"`);
        await queryRunner.query(`DROP TYPE "node_template_type_enum"`);
        await queryRunner.query(`ALTER TYPE "node_template_type_enum_old" RENAME TO  "node_template_type_enum"`);
    }

}