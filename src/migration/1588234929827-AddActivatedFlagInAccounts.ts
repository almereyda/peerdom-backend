import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddActivatedFlagInAccounts1588234929827 implements MigrationInterface {
  name = 'AddActivatedFlagInAccounts1588234929827';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "account" ADD "activated" boolean NOT NULL DEFAULT false`, undefined);

    await queryRunner.query(`UPDATE account SET activated = true WHERE password IS NOT NULL`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "activated"`, undefined);
  }
}
