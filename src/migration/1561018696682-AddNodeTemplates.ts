import {MigrationInterface, QueryRunner} from "typeorm";

export class AddNodeTemplates1561018696682 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "node_template_type_enum" AS ENUM('coreRole')`);
        await queryRunner.query(`CREATE TABLE "node_template" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "purpose" character varying, "policies" jsonb, "notes" jsonb, "color" character varying, "type" "node_template_type_enum" NOT NULL, "accountabilities" jsonb, "domains" jsonb, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "tenantId" uuid, "nodesId" uuid, CONSTRAINT "PK_c56756492adb6effc7448ad8360" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "node" ADD "templateId" uuid`);
        await queryRunner.query(`ALTER TABLE "node_template" ADD CONSTRAINT "FK_b2f9b8e8f2ca051a6156afb456d" FOREIGN KEY ("tenantId") REFERENCES "tenant"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "node_template" ADD CONSTRAINT "FK_1676fcb361f24417993ebaa5133" FOREIGN KEY ("nodesId") REFERENCES "node"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "node" ADD CONSTRAINT "FK_67a49747cbc7dc6108b12c581e2" FOREIGN KEY ("templateId") REFERENCES "node_template"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "FK_67a49747cbc7dc6108b12c581e2"`);
        await queryRunner.query(`ALTER TABLE "node_template" DROP CONSTRAINT "FK_1676fcb361f24417993ebaa5133"`);
        await queryRunner.query(`ALTER TABLE "node_template" DROP CONSTRAINT "FK_b2f9b8e8f2ca051a6156afb456d"`);
        await queryRunner.query(`ALTER TABLE "node" DROP COLUMN "templateId"`);
        await queryRunner.query(`DROP TABLE "node_template"`);
        await queryRunner.query(`DROP TYPE "node_template_type_enum"`);
    }

}
