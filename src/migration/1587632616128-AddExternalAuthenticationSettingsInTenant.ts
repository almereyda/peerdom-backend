import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddExternalAuthenticationSettingsInTenant1587632616128 implements MigrationInterface {
  name = 'AddExternalAuthenticationSettingsInTenant1587632616128';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "tenant" ADD "externalAuthSettings" jsonb NOT NULL DEFAULT '{}'`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "tenant" DROP COLUMN "externalAuthSettings"`, undefined);
  }
}
