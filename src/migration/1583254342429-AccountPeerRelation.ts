import {MigrationInterface, QueryRunner} from "typeorm";

export class AccountPeerRelation1583254342429 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "peer" ADD "accountId" uuid`);
        await queryRunner.query(`ALTER TABLE "peer" ADD CONSTRAINT "FK_ec11a188272edff2649764639d2" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "peer" DROP CONSTRAINT "FK_ec11a188272edff2649764639d2"`);
        await queryRunner.query(`ALTER TABLE "peer" DROP COLUMN "accountId"`);
    }

}
