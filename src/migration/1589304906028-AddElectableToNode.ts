import {MigrationInterface, QueryRunner} from "typeorm";

export class AddElectableToNode1589304906028 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "node" ADD "electable" boolean NOT NULL DEFAULT false`, undefined);
        await queryRunner.query(`UPDATE "node" SET "electable"=true WHERE "templateId" IS NOT NULL`, undefined);
      }

      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "node" DROP COLUMN "electable"`, undefined);
      }
}
