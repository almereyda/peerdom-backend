import {MigrationInterface, QueryRunner} from "typeorm";

export class FixNodeTemplateRelation1561374737453 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "node_template" DROP CONSTRAINT "FK_1676fcb361f24417993ebaa5133"`);
        await queryRunner.query(`ALTER TABLE "node_template" DROP COLUMN "nodesId"`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "node_template" ADD "nodesId" uuid`);
        await queryRunner.query(`ALTER TABLE "node_template" ADD CONSTRAINT "FK_1676fcb361f24417993ebaa5133" FOREIGN KEY ("nodesId") REFERENCES "node"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
