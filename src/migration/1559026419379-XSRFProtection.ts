import {MigrationInterface, QueryRunner} from "typeorm";

export class XSRFProtection1559026419379 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "token" ADD "xsrfSalt" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "token" DROP COLUMN "xsrfSalt"`);
    }

}
