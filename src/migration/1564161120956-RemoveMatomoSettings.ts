import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveMatomoSettings1564161120956 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "tenant" DROP COLUMN "matomoSettings"`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "tenant" ADD "matomoSettings" jsonb NOT NULL DEFAULT '{}'`);
    }

}
