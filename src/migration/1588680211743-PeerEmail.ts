import {MigrationInterface, QueryRunner} from "typeorm";

export class PeerEmail1588680211743 implements MigrationInterface {
    name = 'PeerEmail1588680211743'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "peer" ADD "email" character varying`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "peer" DROP COLUMN "email"`, undefined);
    }

}
