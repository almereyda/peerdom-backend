import {MigrationInterface, QueryRunner} from "typeorm";

export class TimestampWithTimezone1558546162654 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "account" ALTER COLUMN "resetPasswordExpires" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "account" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "account" ALTER COLUMN "updatedAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "holder" ALTER COLUMN "electedUntil" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "holder" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "holder" ALTER COLUMN "updatedAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "mission" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "mission" ALTER COLUMN "updatedAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "node" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "node" ALTER COLUMN "updatedAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "peer" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "peer" ALTER COLUMN "updatedAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "updatedAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "token" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "token" ALTER COLUMN "updatedAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "holder_change" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "mission_change" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "node_change" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
      await queryRunner.query(`ALTER TABLE "peer_change" ALTER COLUMN "createdAt" TYPE TIMESTAMPTZ`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "peer_change" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "node_change" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "mission_change" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "holder_change" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "token" ALTER COLUMN "updatedAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "token" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "updatedAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "tenant" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "peer" ALTER COLUMN "updatedAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "peer" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "node" ALTER COLUMN "updatedAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "node" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "mission" ALTER COLUMN "updatedAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "mission" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "holder" ALTER COLUMN "updatedAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "holder" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "holder" ALTER COLUMN "electedUntil" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "account" ALTER COLUMN "updatedAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "account" ALTER COLUMN "createdAt" TYPE TIMESTAMP`);
      await queryRunner.query(`ALTER TABLE "account" ALTER COLUMN "resetPasswordExpires" TYPE TIMESTAMP`);
    }

}
