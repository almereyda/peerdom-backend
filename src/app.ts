import * as express from 'express';
import * as passport from 'passport';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import bodyParser = require('body-parser');
import {ApolloServer, AuthenticationError} from 'apollo-server-express';

import {
  localStrategy,
  loginHandler,
  logoutHandler,
  addAccountToRequest,
  ownAccountHandler,
  addXsrfCookieToResponse
} from './controllers/auth';
import * as passwordReset from './controllers/passwordReset';
import {
  checkShareTokenHandler,
  getValidShareTokenHandler,
  generateShareTokenHandler,
  invalidateShareTokenHandler
} from './controllers/shareToken';

import * as Task from './controllers/tasks';

import {config} from './peerdom-config';
import {typeDefs, resolvers} from './graphql/schema';

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({req}) => {
    // Check that we have an account and a tenant
    const account = req.account;
    if (!account || !account.tenant) {
      throw new AuthenticationError('Access denied');
    }

    // Add the account to the context
    return {account};
  },
  playground: {
    settings: {
      'request.credentials': 'same-origin'
    }
  }
});

export const app = express();

const asyncHandler = (fn: express.RequestHandler) => (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => Promise.resolve(fn(req, res, next)).catch(next);

const corsOptions: cors.CorsOptions = {
  credentials: true
};

if (config.domain) {
  corsOptions.origin = ['https://' + config.domain];
} else {
  corsOptions.origin = ['http://localhost:4200', 'http://127.0.0.1:4200'];
}

// Add CORS support
app.use(cors(corsOptions));

// Define root of static files
app.use(express.static('public'));

// Set up passport, login and logout route
// User can have a string as name in first argument. Which can then be reused in
// the passport.authenticate method
// See: https://github.com/jaredhanson/passport/issues/287#issuecomment-58188179
// for example
passport.use(localStrategy);
app.use(passport.initialize());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(addXsrfCookieToResponse);
app.post(
  '/login',
  passport.authenticate('local', {
    session: false
  }),
  loginHandler
);
app.post('/logout', logoutHandler);

// Add routes for password resetting
app.post('/requestPasswordReset', passwordReset.requestPasswordReset);
app.post('/resetPassword', asyncHandler(passwordReset.resetPassword));

app.get('/me', addAccountToRequest, ownAccountHandler);

app.get('/ping', (_req, res) => res.send('pong'));

app.get('/shareToken/:token', checkShareTokenHandler);
app.get('/:tenant/shareToken', addAccountToRequest, getValidShareTokenHandler);
app.post('/:tenant/shareToken', addAccountToRequest, generateShareTokenHandler);
app.put('/:tenant/shareToken/:token', addAccountToRequest, invalidateShareTokenHandler);

app.get('/tasks/azure-sync-user', asyncHandler(Task.azureAdSync));

// Base path of graph ql (":tenant" refers to the id of the tenant)
const GRAPHQL_PATH = '/:tenant/graphql';

// Simple generic error handler
app.use(function(err, req, res, next) {
  if (err) {
    if (res.statusCode < 400) {
      // If no error status code has been set, use 500 by default
      res.status(500);
    }

    // Send the error details
    res.json({error: err.message});
  }
  next();
});

// Try to add the account to the request (checking the auth header)
app.use(GRAPHQL_PATH, addAccountToRequest);

// Initialise Apollo
server.applyMiddleware({
  app,
  cors: corsOptions,
  path: GRAPHQL_PATH
});

export const graphqlPath = server.graphqlPath;
