export const config = {
  domain: process.env.PRD_DOMAIN,
  email: {
    from: process.env.PRD_EMAIL_FROM,
    api_key: process.env.PRD_MAILGUN_API_KEY,
    domain: process.env.PRD_MAILGUN_DOMAIN,
    host: process.env.PRD_MAILGUN_HOST,
    protocol: process.env.PRD_MAILGUN_PROTOCOL || 'https:',
    port: process.env.PRD_MAILGUN_PORT || 443
  },
  s3: {
    endpoint: process.env.PRD_S3_ENDPOINT,
    accessKey: process.env.PRD_S3_ACCESS_KEY,
    secret: process.env.PRD_S3_SECRET,
    bucket: process.env.PRD_S3_BUCKET
  },
  cronjob: {
    requiredHeader: process.env.PRD_CRONJOB_HEADER,
    headerValue: process.env.PRD_CRONJOB_HEADER_VALUE
  }
};
