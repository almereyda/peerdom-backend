import {getConnection, getRepository} from 'typeorm';
import debugFactory from 'debug';
import * as request from 'request-promise-native';

import {config} from '../peerdom-config';
import {dumbLog} from '../lib/helpers';
import * as Tenant from '../lib/tenant';
import * as Account from '../lib/account';
import * as Peer from '../lib/peer';
import * as graph from './azure-ad/graphHelper';

const debug = debugFactory('cron:ad-sync');
const debugSteps = debugFactory('cron:ad-sync:all-steps');

const listDifference = (l1: any[], l2: any[]) => {
  return l1.filter(item => l2.indexOf(item) < 0);
};

const getNewToken = async (clientId: string, clientSecret: string, refreshToken: string) => {
  const formData = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
    client_id: clientId,
    client_secret: clientSecret
  };

  const res = JSON.parse(
    await request.post({
      url: 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
      formData: formData
    })
  );

  return [res.refresh_token, res.access_token];
};

const extractUsername = person => {
  if (person.mail) {
    return person.mail.split('@')[0];
  } else {
    return (person.givenName[0] + person.surname).toLowerCase();
  }
};

export const sync = async () => {
  try {
    const connection = await getConnection();
    // Get all tenant with azureAd setup
    const tenants = await Tenant.getAllActiveAzureAd();

    if (tenants.length === 0) {
      debug('No tenant as AD sync setup');
    }

    for (const tenant of tenants) {
      debug('Starting to sync users for ' + tenant.name);
      const settings = tenant.userSyncSettings.azureAd;

      debug('Getting a new access token');
      const [refreshToken, accessToken] = await getNewToken(
        settings.clientId,
        settings.clientSecret,
        settings.refreshToken
      );

      debug('Updating the refresh token in the db');
      await Tenant.updateAzureADRefreshToken(refreshToken, tenant.id);

      debug('Listing all users');
      const list = await graph.listUsers(accessToken);
      const people = list.map(person => ({
        username: extractUsername(person),
        email: person.mail,
        displayName: person.displayName
      }));

      people.sort((a, b) => {
        return a.username.localeCompare(b.username);
      });

      // console.log(people);

      // What need to happen:
      // 1. Compare the list of accounts with the list of people
      const accounts = await Tenant.listAllAccounts(tenant.id);
      // console.log(accounts);

      // 2. Compare the list of peer with the list of people
      const peers = await Tenant.listAllPeers(tenant.id);
      // console.log(peers);

      const [peopleEmail, peopleName] = (() => {
        const emails = new Array();
        const names = new Array();
        for (const person of people) {
          emails.push(person.email);
          names.push(person.displayName);
        }
        return [emails, names];
      })();
      // console.log(peopleEmail);
      // console.log(peopleName);

      const accountsEmail = (() => {
        const emails = new Array();
        for (const account of accounts) {
          emails.push(account.email);
        }
        return emails;
      })();
      // console.log(accountsEmail);

      const peerNames = (() => {
        const names = new Array();
        for (const peer of peers) {
          names.push(peer.displayName);
        }
        return names;
      })();
      // console.log(peerNames);

      // List comparison
      // ---------------
      // 1. Getting the list of new accounts: listDifference(people.email, accounts.email)
      const newAccounts = listDifference(peopleEmail, accountsEmail);
      // console.log(newAccounts);

      // 2. Getting the list of accounts to delete: listDifference(accounts.email, people.email)
      // We don't remove the access right if a user have an email ending in peerdom.org
      const accountsToRemove = listDifference(accountsEmail, peopleEmail).filter(
        email => !email.endsWith('peerdom.org')
      );
      // console.log(accountsToRemove);

      // 3. Getting the list of new peers: listDifference(people.displayName, peers.displayName)
      const newPeers = listDifference(peopleName, peerNames);
      // console.log(newPeers);

      // 3. Getting the list of peers to delete: listDifference(peers.displayName, people.displayName)
      const peersToDelete = listDifference(peerNames, peopleName);
      // console.log(peersToDelete);

      // Deleting the accounts and peers
      // -------------------------------

      // 1. Account deletion
      // -------------------
      if (accountsToRemove.length > 0) {
        debug('Deleting permission and accounts');
        for (const email of accountsToRemove) {
          //  - Delete the permission of this account, for this tenant
          const account = await Account.findByEmail(email);
          const deleted = await Tenant.removeAccess(tenant.id, account.id);
          debugSteps(`Removed access to ${tenant.name} to ${account.email}`);
        }
      } else {
        debug('No accounts to remove');
      }

      // 2. Peer deletion
      // ----------------
      //    - Delete the peer, holders will be deleted in cascade
      if (peersToDelete.length > 0) {
        debug('Deleting peers');
        for (const name of peersToDelete) {
          const peer = peers.find(function (element) {
            return element.displayName === name;
          });
          await Peer.remove(peer.id, [], undefined, tenant.id);
          debugSteps(`Deleted peer ${peer.displayName} from the database`);
        }
      } else {
        debug('No peers to remove');
      }

      // Adding new accounts and peers
      // -----------------------------

      // 1. Creating new accounts
      if (newAccounts.length > 0) {
        debug('Creating new accounts');
        for (const email of newAccounts) {
          const createdAccount = await Account.upsert(email);
          await Tenant.giveAccess(tenant.id, createdAccount.id, false);
          debugSteps(`Created an account for ${email} and gave read access to ${tenant.name}`);
        }
      } else {
        debug('No new accounts to create');
      }

      // 2. Creating new peers
      if (newPeers.length > 0) {
        debug('Creating new peers');
        for (const name of newPeers) {
          await Peer.create({name}, undefined, tenant.id);
          debugSteps(`Created a peer for ${name}`);
        }
      } else {
        debug('No new peers to create');
      }
    }
  } catch (error) {
    throw error;
  }
};
