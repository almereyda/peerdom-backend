/*
 * Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

import * as request from 'request-promise-native';

const hostUrl = 'https://graph.microsoft.com';

/**
 * Generates a GET request the user endpoint.
 * @param {string} accessToken The access token to send with the request.
 * @param {Function} callback
 */
export const getUserData = async accessToken => {
  return await request({url: hostUrl + '/beta/me', headers: bearer(accessToken)});
};

/**
 * Generate a GET request to list all users of the directory
 * @param {string} accessToken The access token to send with the request.
 * @param {Function} callback
 */
export const listUsers = async accessToken => {
  const header = bearer(accessToken);
  const response = await request({
    url:
      hostUrl +
      '/v1.0/groups/1b9a027b-1b34-4e79-802b-02e3d713edcd/members?$select=id,displayName,mail,givenName,surname&$top=999',
    headers: bearer(accessToken)
  });

  return JSON.parse(response).value;
};

// function getUserPhoto(accessToken, userId, callback) {
//   request
//     .get(hostUrl + '/beta/users/' + userId + '/photo/$value')
//     .set('Authorization', 'Bearer ' + accessToken)
//     .end((err, res) => {
//       callback(err, res);
//     });
// }

/* Return a bearer header */
const bearer = accessToken => {
  return {Authorization: `Bearer ${accessToken}`};
};
