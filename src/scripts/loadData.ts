import {createConnection} from 'typeorm';
import * as fs from 'fs';
/*
Hardcoded ids scheme:
Peer => 10000000-0000-0000-0000-XXXXXXXXXXXX
Tenant => 20000000-0000-0000-0000-XXXXXXXXXXXX
User => 30000000-0000-0000-0000-XXXXXXXXXXXX
Node (circle) => 40000000-1000-0000-0000-XXXXXXXXXXXX
Node (role following template) => 40000000-2000-0000-0000-XXXXXXXXXXXX
Node (role) => 40000000-3000-0000-0000-XXXXXXXXXXXX
NodeTemplate => 50000000-3000-0000-0000-XXXXXXXXXXXX
CustomFieldDefinition => 60000000-0000-0000-0000-XXXXXXXXXXXX
CustomField (circle) => 70000000-1000-0000-0000-XXXXXXXXXXXX
CustomField (role following template) => 70000000-2000-0000-0000-XXXXXXXXXXXX
CustomField (role) => 70000000-3000-0000-0000-XXXXXXXXXXXX
Holders => 80000000-X000-0000-0000-XXXXXXXXXXXX
Project => 90000000-0000-0000-0000-XXXXXXXXXXXX
*/

// WARNING: order of jsonFileNames is important due to foreign keys
const entitiesToLoad = [
  {name: 'Tenant', jsonFileNames: ['tenants']},
  {name: 'Account', jsonFileNames: ['account']},
  {name: 'AccountPermission', jsonFileNames: ['accountPermission']},
  {name: 'Peer', jsonFileNames: ['peers']},
  {name: 'Mission', jsonFileNames: ['missions']},
  {
    name: 'CustomFieldDefinition',
    jsonFileNames: ['customFieldDefinitions', 'customFieldDefinitions2', 'customFieldDefinitions3']
  },
  {
    name: 'CustomField',
    jsonFileNames: [
      'customFieldsCircle',
      'customFieldsCircle2',
      'customFieldsCircle3',
      'customFieldsRole',
      'customFieldsTemplate'
    ]
  },
  {
    name: 'NodeTemplate',
    jsonFileNames: ['roleTemplates']
  },
  {
    name: 'Node',
    jsonFileNames: [
      'circles/GC',
      'circles/GC2',
      'circles/GC3',
      'circles/finances_admin',
      'circles/research',
      'circles/design',
      'circles/development',
      'circles/communication',
      'circles/marketing'
    ]
  },
  {name: 'Token', jsonFileNames: ['token']},
  {
    name: 'Holder',
    jsonFileNames: [
      'circles/GC_holders',
      'circles/finances_admin_holders',
      'circles/research_holders',
      'circles/design_holders',
      'circles/development_holders',
      'circles/communication_holders',
      'circles/marketing_holders'
    ]
  },
  {name: 'Project', jsonFileNames: ['projects']}
];

/*
 * This script is inspired by https://github.com/jgordor
 * https://github.com/nestjs/nest/issues/409#issuecomment-364639051
 */

async function loadAll(connection) {
  console.log('===== LOAD DATA =====');
  try {
    for (const entityToLoad of entitiesToLoad) {
      const repository = await connection.getRepository(entityToLoad.name);
      for (const jsonFileName of entityToLoad.jsonFileNames) {
        const fixtureFile = `src/fixtures/${jsonFileName}.json`;
        if (fs.existsSync(fixtureFile)) {
          console.log(`Uploading content of ${jsonFileName}.json`);
          const fixture = JSON.parse(fs.readFileSync(fixtureFile, 'utf8'));
          let items = fixture.items;
          if (entityToLoad.name === 'Node') {
            // Retrieve the information from the related template if the node has a template id
            items = await Promise.all(
              items.map(async item => {
                if (item.template) {
                  const template = await findRelatedTemplate(connection, item.template.id);
                  delete template.id;
                  template.tenant = {id: template.tenant.id};
                  item = {...item, ...template};
                }
                return item;
              })
            );
          }

          // Retrieving context from fixture
          let contextAuthorId: string;
          let contextTenantId: string;
          if (fixture.context) {
            if (fixture.context.author && fixture.context.author.id) {
              contextAuthorId = fixture.context.author.id;
            }
            if (fixture.context.tenant) {
              contextTenantId = fixture.context.tenant.id;
            }
          }

          // Saving data in DB
          await repository.save(items, {
            data: {
              accountId: contextAuthorId,
              tenantId: contextTenantId
            }
          });
        }
      }
      console.log(`Entity ${entityToLoad.name} loaded`);
    }
  } catch (error) {
    throw new Error(`ERROR Loading fixtures on test db: ${error}`);
  }
}

async function cleanAll(connection) {
  console.log('  Dropping the database...');
  await connection.dropDatabase();
  console.log('  Running the migration...');
  return connection.runMigrations();
}

async function findRelatedTemplate(connection, templateId) {
  const repository = await connection.getRepository('NodeTemplate');
  return repository.findOne({
    relations: ['tenant'],
    where: {id: templateId}
  });
}

(async () => {
  try {
    console.log('Connecting to DB...');
    const connection = await createConnection();
    try {
      console.log('Cleaning the database ...');
      await cleanAll(connection);
      console.log('Loading fixtures...');
      await loadAll(connection);
      await connection.close();
    } catch (error) {
      await connection.close();
      console.log(error);
    }
  } catch (error) {
    console.log(error);
  }
})();
