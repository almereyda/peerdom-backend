/**
 * Add accounts to a specific tenant.
 *
 * Accounts must be provided in an accounts.csv file in same directory as this script, with data formatted as such:
 * ```
 * displayName,email,writeAccess
 * Test One,test1@peerdom.org,false
 * Test two,test2@peerdom.org,true
 * ```
 *
 * @example ts-node add-accounts.ts tenantSlug <path to account csv>
 */
import * as fs from 'fs';
import * as csv from 'csv-parser';
import axios from 'axios';
import {createConnection, getRepository} from 'typeorm';
import {AccountPermission} from '../entity/account_permission';
import * as Account from '../lib/account';
import {Tenant} from '../entity/tenant';
import {Peer} from '../entity/peer';
import {slugify} from '../lib/helpers';

const logAccount = (userAccount, newAccount) => {
  let message = '';
  if (!userAccount) {
    message = 'Creating ';
  } else {
    message = 'Updating ';
  }

  if (newAccount.writeAccess) {
    message += 'account with write access ';
  } else {
    message += 'read-only account ';
  }
  message += 'for ' + newAccount.email + '...';
  console.log(message);
};

const upsertAccount = async (userAccount, newAccount) => {
  // We create the account if it doesn't already exist
  if (!userAccount) {
    userAccount = await Account.create(newAccount.email);
  }

  return userAccount;
};

const upsertPermission = async (tenant, userAccount, newAccount, repository) => {
  // This is the key that will help us find an already existing permission
  const permissionKey = {
    account: {id: userAccount.id},
    tenant: {id: tenant.id}
  };

  // This is the new permission to apply
  const newPermission = {
    account: {id: userAccount.id},
    tenant: {id: tenant.id},
    writeAccess: newAccount.writeAccess
  };

  // If the permission set already exist, we update it
  // Otherwise, we create new
  if (await repository.findOne(permissionKey)) {
    await repository.update(permissionKey, newPermission);
  } else {
    await repository.save(newPermission);
  }
};

const addPeer = async (name, tenant, newAccount, repository) => {
  const nameSlug = slugify(name);

  const peer = {
    displayName: name,
    slug: nameSlug,
    email: newAccount.email,
    account: {id: newAccount.id},
    tenant: {id: tenant.id}
  };

  // If the peer don't exist, we create one.
  // Otherwise, we just ignore
  if (!(await repository.findOne(peer))) {
    console.log('Adding a new peer ' + peer.displayName + ' for ' + newAccount.email);
    await repository.save(peer, {data: {tenantId: tenant.id}});
  }
};

const sendEmailReset = async account => {
  if (Account.isActivated(account)) {
    console.log('No email sent for this account, the password is already set.');
  } else {
    await axios
      .post(
        'https://backend.peerdom.org/requestPasswordReset',
        {
          email: account.email
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
      .then(res => {
        console.log(`Password initialisation email sent for ${account.email} (${res.status}).`);
      })
      .catch(error => {
        console.error('Error when sending password initialisation email.', error);
        console.log(error.response.data);
      });
  }
};

(async () => {
  const tenantSlug = process.argv[2];
  const accountFile = process.argv[3];

  if (!tenantSlug) {
    console.log('You must provide the slug of the tenant as first argument');
  }

  try {
    const connection = await createConnection();

    const tenantRepository = await getRepository(Tenant);
    const permissionRepository = await connection.getRepository(AccountPermission);
    const peerRepository = await connection.getRepository(Peer);

    const tenant = await tenantRepository.findOne({select: ['name', 'id'], where: {slug: tenantSlug}});

    const accounts = await new Promise<{displayName: string; email: string; writeAccess: boolean}[]>(
      (resolve, reject) => {
        const rows = [];
        fs.createReadStream(accountFile)
          .pipe(
            csv({
              mapValues: ({header, index, value}) => {
                if (header === 'writeAccess') {
                  // Convert string to boolean
                  return !!(value === 'true');
                }
                return value;
              }
            })
          )
          .on('data', async row => {
            rows.push(row);
          })
          .on('end', async () => {
            resolve(rows);
          });
      }
    );

    console.log('Adding ' + accounts.length + ' accounts for ' + tenant.name + '...');
    for (const account of accounts) {
      // We get the account if any
      let userAccount = await Account.findByEmail(account.email);

      if (account.email) {
        logAccount(userAccount, account);

        userAccount = await upsertAccount(userAccount, account);
        await upsertPermission(tenant, userAccount, account, permissionRepository);

        // Only send the email if a password is not already set
        await sendEmailReset(userAccount);
      }

      // Add the peer equivalent of the account
      await addPeer(account.displayName, tenant, userAccount, peerRepository);
    }
    console.log('Done.');

    await connection.close();
  } catch (error) {
    console.error(error);
  }
})();
