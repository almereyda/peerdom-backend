/* Make SuperUser
 *
 * This script add a specific account to all Tenants with write access.
 *
 * Usage:
 *   make-superuser.js email@ofthesuper.user
 *
 */

import {createConnection} from 'typeorm';
import {Account} from '../entity/account';
import {AccountPermission} from '../entity/account_permission';
import {Tenant} from '../entity/tenant';

(async () => {
  const SUPERUSER = process.argv[2];

  if (!SUPERUSER) {
    console.log('You must provide the email of the user as first argument');
  }

  try {
    const connection = await createConnection();

    const accountPermission = await connection.getRepository(AccountPermission);

    const superuser = await connection.getRepository(Account).findOne({email: SUPERUSER});

    console.log(superuser);

    const tenantRepo = await connection.getRepository(Tenant);
    const tenants = await tenantRepo.find();

    for (const tenant of tenants) {
      console.log('Giving access to ' + tenant.name);
      try {
        const permission = {
          account: {id: superuser.id},
          tenant: {id: tenant.id},
          writeAccess: true
        };
        await accountPermission.save(permission);
      } catch (error) {
        console.log(error);
      }
    }

    await connection.close();
  } catch (error) {
    console.log(error);
  }
})();
