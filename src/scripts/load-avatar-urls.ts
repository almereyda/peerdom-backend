import * as AWS from 'aws-sdk';
import {createConnection} from 'typeorm';
import {Peer} from '../entity/peer';

const s3Endpoint = process.argv[2];
const s3AccessKey = process.argv[3];
const s3Secret = process.argv[4];
const s3Bucket = process.argv[5];

const s3 = new AWS.S3({
  endpoint: s3Endpoint,
  accessKeyId: s3AccessKey,
  secretAccessKey: s3Secret,
  s3ForcePathStyle: true,
  signatureVersion: 'v4'
});
const TYPEORM_CHUNK = 250;

(async () => {
  try {
    console.log('Fetching objects from bucket...');
    let s3Response = await s3
      .listObjects({
        Bucket: s3Bucket
      })
      .promise();
    if (!s3Response.Contents || !s3Response.Contents.length) {
      console.log('No objects in bucket...');
      return;
    }
    let uploads = s3Response.Contents;

    while (s3Response.Contents && s3Response.Contents.length === 1000) {
      const marker = s3Response.Contents[999].Key;
      console.log('> There are more objects in the bucket. Marker:', marker);
      s3Response = await s3
        .listObjects({
          Bucket: s3Bucket,
          Marker: marker
        })
        .promise();

      if (s3Response.Contents && s3Response.Contents.length > 1) {
        console.log('>> Number of additional objects:', s3Response.Contents.length);
        console.log('>> Sample additional object:', s3Response.Contents[0]);
        uploads = [...uploads, ...s3Response.Contents.slice(1)];
      }
    }
    console.log('> Total number of objects:', uploads.length);
    console.log('> Sample object:', uploads[0]);

    console.log('Building avatar URL patches...');
    const patchIds: string[] = [];
    const peerPatches: any[] = [];
    for (const upload of uploads) {
      const key = upload.Key;
      if (key.endsWith('.jpg')) {
        const fileName = key.substring(key.lastIndexOf('/') + 1);
        const peerId = fileName.replace('.jpg', '');
        // check for a valid peer ID
        if (/^.{8}-.{4}-.{4}-.{4}-.{12}$/.test(peerId)) {
          const url = `/${key}`;
          patchIds.push(peerId);
          peerPatches.push({
            id: peerId,
            avatarUrl: url
          });
        }
      }
    }
    console.log('> Number of patch IDs:', patchIds.length);
    console.log('> Sample ID:', patchIds[0]);
    console.log('> Number of patches:', peerPatches.length);
    console.log('> Sample patch:', peerPatches[0]);

    console.log('Filtering obsolete avatar patches...');
    const connection = await createConnection();
    const peerRepository = connection.getRepository(Peer);
    const peers = await peerRepository.findByIds(patchIds, {
      select: ['id']
    });
    const peerIds = peers.map(p => p.id);
    const filteredPatches = peerPatches.filter(p => peerIds.indexOf(p.id) !== -1);
    console.log('> Number of peers:', peers.length);
    console.log('> Sample peer:', peers[0]);
    console.log('> Sample peer ID:', peerIds[0]);
    console.log('> Number of filtered patches:', filteredPatches.length);
    console.log('> Sample filtered patch:', filteredPatches[0]);

    if (filteredPatches.length) {
      console.log('Saving avatar URLs in DB...');
      const savedData = await peerRepository.save(filteredPatches, {
        chunk: TYPEORM_CHUNK
      });
      console.log('> Saved data length:', savedData.length);
      console.log('> Sample saved data:', savedData[0]);
    } else {
      console.log('No avatar URLs to save in DB...');
    }
  } catch (error) {
    console.error(error);
  }
})();
