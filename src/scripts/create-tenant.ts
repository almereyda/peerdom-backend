/* Create a new Tenant
 *
 * This will create a new tenant with the name passed as first argument and the slug as second argument.
 * If a list of emails (separated by space) is then passed, it will also add those to the tenant.
 *
 * Usage: create-tenant.js "Tenant name" tenantSlug email1 email2 ...
 */

import {createConnection, getRepository} from 'typeorm';
import {AccountPermission} from '../entity/account_permission';
import * as Account from '../lib/account';
import {Tenant} from '../entity/tenant';
import {CustomFieldDefinition, CustomFieldType} from '../entity/custom_field_definition';
import * as node from '../lib/node';

// We always make business creators and admins super users
const superUsers = [
  {id: '8b540390-db5e-47dd-b9ce-7316879a35fc', email: 'alex@peerdom.org'},
  {id: 'cb8c806b-a1d9-4896-bc35-58c2a0ad10dc', email: 'bastiaan@peerdom.org'},
  {id: 'fd193bdb-b8f6-4209-b001-762ea6072fa8', email: 'david@peerdom.org'},
  {id: '8c21c7c2-d6f5-4a52-bd77-f2149be5cf01', email: 'mayeu@peerdom.org'},
  {id: '29790906-c7b6-4dd6-82fd-e12c9114b6d9', email: 'nathan@peerdom.org'}
];

const defaultCustomFieldDefinitions = [
  {
    name: 'Purpose',
    translation: {
      en: 'Purpose',
      de: 'Ziel',
      fr: "Raison d'être",
      nl: 'Doel',
      sv: 'Avsikt'
    }
  },
  {
    name: 'Responsibilities',
    translation: {
      en: 'Responsibilities',
      de: 'Verantwortlichkeiten',
      fr: 'Responsabilités',
      nl: 'Verantwoordelijkheden',
      sv: 'Ansvarsområden'
    },
    multiEntry: true
  },
  {
    name: 'Notes',
    translation: {
      en: 'Notes',
      de: 'Notizen',
      fr: 'Notes',
      nl: 'Aantekeningen',
      sv: 'Anteckningar'
    },
    type: CustomFieldType.Textarea
  }
];

const upsertAccount = async email => {
  // We create the account if it doesn't already exist
  let userAccount = await Account.findByEmail(email);

  if (!userAccount) {
    console.log(`Creating account for ${email}.`);
    userAccount = await Account.create(email);
  } else {
    console.log(`An account for ${email} already exist. Using it.`);
  }

  return userAccount;
};

const upsertPermission = async (tenant, userAccount, repository) => {
  // This is the key that will help us find an already existing permission
  const permissionKey = {
    account: {id: userAccount.id},
    tenant: {id: tenant.id}
  };

  // This is the new permission to apply
  const newPermission = {
    account: {id: userAccount.id},
    tenant: {id: tenant.id},
    writeAccess: true
  };

  // If the permission set already exist, we update it
  // Otherwise, we create new
  if (await repository.findOne(permissionKey)) {
    await repository.update(permissionKey, newPermission);
  } else {
    await repository.save(newPermission);
  }
};

(async () => {
  const tenantName = process.argv[2];
  const tenantSlug = process.argv[3];
  const emails = process.argv.slice(4);

  console.log(process.argv);
  console.log(emails);

  if (!tenantName) {
    console.error('You must provide the name of the tenant as first argument');
    process.exit(1);
  }

  if (!tenantSlug) {
    console.error('You must provide the slug of the tenant as second argument');
    process.exit(1);
  }

  try {
    const connection = await createConnection();

    const tenant = await getRepository(Tenant);
    const accountPermission = await connection.getRepository(AccountPermission);

    console.log(`Creating tenant ${tenantName}...`);
    const newTenant = await tenant.save({
      name: tenantName,
      slug: tenantSlug
    });

    for (const email of emails) {
      const userAccount = await upsertAccount(email);
      await upsertPermission(newTenant, userAccount, accountPermission);
    }

    for (const superUser of superUsers) {
      const permission = {
        account: {id: superUser.id},
        tenant: {id: newTenant.id},
        writeAccess: true
      };

      console.log(`Giving write access to ${superUser.email}...`);
      await accountPermission.save(permission);
    }

    console.log(`Creating root circle ${tenantName}...`);
    await node.create(
      {
        name: tenantName,
        slug: tenantSlug,
        type: 'circle',
        tenant: {
          id: newTenant.id
        }
      },
      undefined,
      newTenant.id
    );

    console.log('Creating default custom field definitions...');
    const cfd = await getRepository(CustomFieldDefinition);
    for (const definition of defaultCustomFieldDefinitions) {
      console.log(`Creating the "${definition.name}" definition...`);
      await cfd.save({
        ...definition,
        tenant: {id: newTenant.id}
      });
    }

    await connection.close();
  } catch (error) {
    console.log(error);
  }
})();
