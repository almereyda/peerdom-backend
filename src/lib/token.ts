import * as crypto from 'crypto';

import * as account from '../lib/account';
import {cryptoHashString} from '../lib/helpers';

/**
 * Period for which a reset token is valid
 */
export const TOKEN_VALIDITY_PERIOD = 14 * 24 * 60 * 60 * 1000; // 14 days

/* Token
 *
 * We create a token type
 */
export type Token = string;
export type Hash = string;

/* Return a new token and its hash
 *
 * @return [Token, Hash]
 */
export const generate = (): [Token, Hash] => {
  const token = crypto.randomBytes(15).toString('hex');
  const hashToken = cryptoHashString(token);
  return [token, hashToken];
};
