import {createTransport} from 'nodemailer';
import * as mailgun from 'nodemailer-mailgun-transport';
import {config} from '../peerdom-config';

const auth = {
  auth: {
    api_key: config.email.api_key,
    domain: config.email.domain
  },
  host: config.email.host,
  protocol: config.email.protocol,
  port: config.email.port
};

const transporter = createTransport(mailgun(auth));

/**
 * Subject and body for the reset password e-mail
 */
const EMAIL_TEXT = {
  subject: 'Please change your Peerdom password',
  body:
    'Hello there, dear peer!\n\n' +
    'You have either requested to change your password, or someone has invited you to join Peerdom. Please set your password by clicking on the following link or copy/pasting it into your browser:\n\n' +
    '{{URL}}\n\n' +
    'Happy mapping,\n' +
    'The Peerdom team',
  html:
    'Hello there, dear peer!<br><br>' +
    'You have either requested to change your password, or someone has invited you to join Peerdom. Please set your password by clicking on the following link or copy/pasting it into your browser:<br><br>' +
    '<a href="{{URL}}">{{LINK}}</a><br><br>' +
    'Happy mapping,<br>' +
    'The Peerdom team<br>' +
    '<p>' +
    '<img src="https://static.peerdom.org/img/Peerdom_EmailSig.jpg" alt="Peerdom logo" style="width: 125px;" /><br>' +
    '<strong>Redesigning organisations</strong><br>' +
    '<a href="https://about.peerdom.org">about.peerdom.org</a>' +
    '</p>'
};

export const sendResetPasswordEmail = (email: string, resetToken: string) => {
  console.log('💌 email: Sending reset password email to: ' + email);

  // Send the e-mail to the owner of the account
  let text = EMAIL_TEXT.body;
  text = text.replace(/\{\{URL\}\}/g, 'https://' + config.domain + '/login/reset/' + resetToken);

  let htmlversion = EMAIL_TEXT.html;
  htmlversion = htmlversion.replace(/\{\{URL\}\}/g, 'https://' + config.domain + '/login/reset/' + resetToken);
  htmlversion = htmlversion.replace(/\{\{LINK\}\}/g, config.domain + '/login/reset/' + resetToken);

  if (process.env.NODE_ENV !== 'production') {
    console.log(text);
  }

  return transporter.sendMail({
    to: email,
    from: config.email.from,
    subject: EMAIL_TEXT.subject,
    text: text,
    html: htmlversion
  });
};

export const checkEmailProviderConnection = async () => {
  await transporter.verify((error, success) => {
    if (error) {
      console.log('💌 error:email: the email server is NOT configured correctly. Please check your configuration');
      console.log(error);
    } else {
      console.log('💌 email: the email server is ready to take our messages');
    }
  });
};
