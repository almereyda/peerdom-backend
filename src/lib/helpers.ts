import * as crypto from 'crypto';
import * as fs from 'fs';
import * as sharp from 'sharp';
import * as AWS from 'aws-sdk';
import {ForbiddenError} from 'apollo-server-express';
const getSlug = require('speakingurl');

import {config} from '../peerdom-config';
const s3 = new AWS.S3({
  endpoint: config.s3.endpoint,
  accessKeyId: config.s3.accessKey,
  secretAccessKey: config.s3.secret,
  s3ForcePathStyle: true,
  signatureVersion: 'v4'
});

export type WithMandatory<T, K extends keyof T> = Pick<T, K> & Partial<Exclude<T, K>>;
export type Identity<T> = {[P in keyof T]: T[P]};
export type Replace<T, K extends keyof T, TReplace> = Identity<
  Pick<T, Exclude<keyof T, K>> &
    {
      [P in K]: TReplace;
    }
>;

/**
 * Check if GraphQL account in the GraphQL context has write access
 * @param {{}} context
 * @throws ForbiddenError Throws an error if there is no write access
 */
export const checkWriteAccess = context => {
  if (
    !context ||
    typeof context !== 'object' ||
    !context.account ||
    typeof context.account !== 'object' ||
    context.account.writeAccess !== true
  ) {
    throw new ForbiddenError('Access denied: write access required');
  }
};

/**
 * Hashes the given string with the sha256 algo
 * @param plainString
 */
export const cryptoHashString = (plainString: string): string => {
  const shasum = crypto.createHash('sha256');
  shasum.update(plainString || '');
  return shasum.digest('hex');
};

export const cryptoHashStringWithSalt = (plainString: string, salt: string): string => {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(plainString || '');
  return hash.digest('hex');
};

export const validatePassword = (password: string): void => {
  if (password.length < 10) {
    throw new Error('Password must be at least 10 characters long');
  }

  if (!/[a-z]/.test(password)) {
    throw new Error('Password must contain at least one lowercase letter');
  }

  if (!/[A-Z]/.test(password)) {
    throw new Error('Password must contain at least one uppercase letter');
  }

  if (!/[0-9]/.test(password)) {
    throw new Error('Password must contain at least one number');
  }

  if (!/[!”#$%&'()*+,-./:;<=>?@\[\\\]^_`{|}~]/.test(password)) {
    throw new Error('Password must contain at least one special character');
  }
};

// This dump the json serialised of anything to stdout
export const dumbLog = (toLog: any) => {
  if (process.env['NODE_ENV'] !== 'test') {
    console.log(JSON.stringify(toLog));
  }
};

export const slugify = (text: string): string => {
  return getSlug(text);
};

/**
 * Generates a unique slug by appending an integer suffix to the slug base.
 *
 * @param slugBase          the slug base, to which a number will be appended
 * @param conflictingSlugs  an array of slugs that can cause conflict
 * @param incrementStart    the start of the increment which ensures uniqueness
 */
export const generateUniqueSlug = (slugBase: string, conflictingSlugs: string[], incrementStart = 2) => {
  let uniqueSlug = slugBase;
  let increment = incrementStart;
  while (conflictingSlugs.includes(uniqueSlug)) {
    uniqueSlug = `${slugBase}-${increment}`;
    increment++;
  }
  return uniqueSlug;
};

export interface File {
  createReadStream: () => fs.ReadStream;
}

export interface ImageMetadata {
  format: string;
  size: number;
  width: number;
  height: number;
}

/**
 * Gets the metadata of an image file using Sharp.
 *
 * @param file  the file of interest
 */
export const getImageMetadata = (file: File): Promise<ImageMetadata> => {
  return new Promise<ImageMetadata>((resolve, reject) => {
    // sharp has to be instantiated, otherwise it won't be able to read the metadata from the stream
    const pipeline = sharp();
    pipeline
      .metadata()
      .then((metadata: ImageMetadata) => resolve(metadata))
      .catch((error: Error) => reject(error));
    file
      .createReadStream()
      .on('error', (error: Error) => reject(error))
      .pipe(pipeline);
  });
};

/**
 * Makes sure that a directory exists.
 * Creates it if it doesn't.
 *
 * @param path the path to the directory
 */
export const ensureDirectoryExistence = (path: string) => {
  try {
    fs.mkdirSync(path, {recursive: true});
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err;
    }
  }
};

/**
 * Saves an avatar image in the uploads folder and an S3 bucket.
 * Non-image formats and huge files are discarded.
 * Crop-based resize is done for big images.
 * The input image is converted to JPEG and saved as such.
 *
 * @param tenantId          the ID of the tenant
 * @param file              the uploaded avatar file
 * @param metadata          the image file metadata
 * @param inputPixelsLimit  the input pixels limit which defaults to 4000x4000
 * @param resizeTrigger     the resize trigger which defaults to 1600 (width and height)
 * @param jpegQuality       the quality of the stored JPEG which defaults to 100%
 */
export const saveAvatarImage = (
  tenantId: string,
  file: File,
  metadata: ImageMetadata,
  inputPixelsLimit = 16000000,
  resizeTrigger = 1600,
  jpegQuality = 100
): Promise<String | Error> => {
  const destination = `public/uploads/${tenantId}/avatar`;
  const avatarId = crypto.randomBytes(8).toString('hex');
  ensureDirectoryExistence(destination);

  return new Promise((resolve, reject) => {
    const transformationPipeline = sharp({
      limitInputPixels: inputPixelsLimit
    }).rotate(); // auto-rotate, taking EXIF into consideration
    const shouldResize = metadata.width > resizeTrigger && metadata.height > resizeTrigger;
    if (shouldResize) {
      transformationPipeline.resize({
        width: resizeTrigger,
        height: resizeTrigger,
        position: sharp.strategy.attention
      });
    }
    transformationPipeline.jpeg({
      quality: jpegQuality
    });

    const errorHandler = (error: Error) => {
      dumbLog(error);
      reject(error);
    };
    file
      .createReadStream()
      .on('error', errorHandler)
      .pipe(transformationPipeline)
      .on('error', errorHandler)
      .pipe(fs.createWriteStream(`${destination}/${avatarId}.jpg`))
      .on('error', errorHandler)
      .on('finish', () => {
        s3.putObject(
          {
            Bucket: config.s3.bucket,
            Key: `${tenantId}/avatar/${avatarId}.jpg`,
            Body: fs.createReadStream(`${destination}/${avatarId}.jpg`)
          },
          error => {
            if (!error) {
              resolve(`/${tenantId}/avatar/${avatarId}.jpg`);
            } else {
              errorHandler(error);
            }
          }
        );
      });
  });
};

/**
 * Deletes a file in case it exists.
 *
 * @param path  the path to the file
 */
export const deleteFile = (path: string) => {
  try {
    fs.unlinkSync(path);
  } catch (err) {
    if (err.code !== 'ENOENT') {
      throw err;
    }
  }
};

/**
 * Deletes an avatar image from the uploads folder and the S3 bucket.
 *
 * @param avatarUrl   the unique URL of the avatar
 */
export const deleteAvatarImage = (avatarUrl: string) => {
  const path = `public/uploads/${avatarUrl}`;
  deleteFile(path);
  s3.deleteObject(
    {
      Bucket: config.s3.bucket,
      Key: avatarUrl
    },
    error => dumbLog(error)
  );
};
