/* This is the public API for peer management.
 *
 * This file is the only one that should be called outside of this domain.
 * Other domains should never call any other file related to Peer since they
 * are considered internal API and may change without any warning.
 */

import {getConnection, Like, Not} from 'typeorm';
import {slugify, generateUniqueSlug} from './helpers';

import {Peer} from '../entity/peer';

/** Export the Peer entity
 *
 * For use in typechecking in other modules.
 * This allow use to mask the entity to the outside world, making sure another
 * domain only loads this file.
 */
export type Type = Peer;

/**
 * Creates a peer in the database.
 *
 * @param newPeer     the new peer object
 * @param accountId   the ID of the account creating the peer
 * @param tenantId    the ID of the tenant to add the peer to
 */
export const create = async (newPeer, accountId: string, tenantId: string) => {
  const repository = await getPeerRepository();
  // We need to create a new instance of the entity to have access to the hooks in the entity listeners subscribers
  // See: https://github.com/typeorm/typeorm/issues/674#issuecomment-346296494
  const peer = Object.assign(new Peer(), {...newPeer, tenant: {id: tenantId}});
  peer.slug = await generateUniqueSlugForPeer(undefined, newPeer.displayName, tenantId);
  return await repository.save(peer, {data: {accountId, tenantId}});
};

/**
 * Updates a peer in the database.
 *
 * @param peerId      the ID of the peer to update
 * @param patch       the update patch
 * @param accountId   the ID of the account updating the peer
 * @param tenantId    the ID of the tenant to update the peer in
 */
export const update = async (peerId: string, patch: any, accountId: string, tenantId: string) => {
  const repository = await getPeerRepository();

  if (patch.displayName) {
    patch.slug = await generateUniqueSlugForPeer(peerId, patch.displayName, tenantId);
  }

  // We need to create a new instance of the entity to have access to the hooks in the entity listeners subscribers
  // See: https://github.com/typeorm/typeorm/issues/674#issuecomment-346296494
  const entity = Object.assign({}, {id: peerId, ...patch});
  // We need to do a save instead of an update because update is only partial and is not listen to by subscribers
  // See: https://github.com/typeorm/typeorm/issues/2809#issuecomment-451914877
  await repository.save(entity, {data: {accountId, tenantId}});

  return await repository.findOne(peerId);
};

/**
 * Removes a peer from the database.
 *
 * @param peerId        the ID of the peer to remove
 * @param cascadeItems  the items to cascade delete
 * @param accountId     the ID of the account removing the peer
 * @param tenantId      the ID of the tenant to remove the peer from
 */
export const remove = async (peerId: string, cascadeItems: any[], accountId: string, tenantId: string) => {
  const repository = await getPeerRepository();
  const peer = await repository.findOne(peerId);
  const removedPeer = Object.assign({}, peer);
  await repository.remove(peer, {
    data: {
      accountId: accountId,
      tenantId: tenantId,
      cascadeItems: cascadeItems
    }
  });
  return removedPeer;
};

/**
 * Retrieves a peer from the database.
 *
 * @param peerId  the ID of the peer to retrieve
 */
export const findById = async (peerId: string) => {
  const repository = await getPeerRepository();
  return await repository.findOne(peerId);
};

/* Get the peer repository
 *
 * A function to prevent mistake when asking for the account permission repository
 */
const getPeerRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(Peer);
};

/**
 * Gets all slugs similar to the one of interest.
 *
 * @param slug            the slug of interest
 * @param tenantId        the ID of the tenant
 * @param peerIdToIgnore  an optional peer ID to ignore for the list
 */
const getSimilarSlugs = async (slug: string, tenantId: string, peerIdToIgnore?: string) => {
  const repository = await getPeerRepository();
  const query: any = {
    select: ['slug'],
    where: {slug: Like(`${slug}%`), tenant: {id: tenantId}}
  };
  if (peerIdToIgnore) {
    query.where.id = Not(peerIdToIgnore);
  }
  const peersWithSimilarSlugs = await repository.find(query);
  return peersWithSimilarSlugs.map(n => n.slug);
};

/**
 * Generates a unique slug for a peer.
 *
 * @param peerId      the ID of the peer
 * @param peerName    the peer's name
 * @param tenantId    the ID of the tenant
 */
const generateUniqueSlugForPeer = async (peerId: string, peerName: string, tenantId: string) => {
  let slug = slugify(peerName);

  // Check for slug conflicts and use auto resolution if necessary
  const similarSlugs = await getSimilarSlugs(slug, tenantId, peerId);
  if (similarSlugs.includes(slug)) {
    slug = generateUniqueSlug(slug, similarSlugs);
  }

  return slug;
};
