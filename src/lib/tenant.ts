/* This is the public API for tenant management.
 *
 * This file is the only one that should be called outside of this domain.
 * Other domains should never call any other file related to Tenants since they
 * are considered internal API and may change without any warning.
 */

import {Tenant} from '../entity/tenant';
import {getConnection} from 'typeorm';
import * as AccountPermission from './account_permission';
// Only for some return type
import {Account} from '../entity/account';
import {Peer} from '../entity/peer';
import { WithMandatory } from './helpers';

const getTenantBySlug = async (slug: string) => {
  return;
};

/** Get a tenant by ID
 *
 * String: a tenant id
 */
export const getById = async (tenantId: string) => {
  const repository = await getTenantRepository();
  return await repository.findOne(tenantId);
};

/** Save a tenant
 *
 * Tenant: a tenant
 */
export const save = async (tenant: WithMandatory<Tenant, 'name' | 'slug'>) => {
  const repository = await getTenantRepository();
  return await repository.save(tenant);
};

/* Return all accounts of a tenant
 *
 * @params String the tenant Id
 * @return Account[]
 */
export const listAllAccounts = async (tenant: string): Promise<Account[]> => {
  return await getConnection()
    .createQueryBuilder()
    // SELECT *
    .select('account')
    // FROM account account
    .from(Account, 'account')
    // JOIN account_permission permission
    // ON permission."accountId" = account.id
    .leftJoinAndSelect('account.permissions', 'permission')
    // WHERE account_permission."tenantId" = 'tenantId'
    .where('permission.tenant = :tenant', {tenant: tenant})
    .getMany();
};

/* Return all peers of a tenant
 *
 * @params String the tenant Id
 * @return Account[]
 */
export const listAllPeers = async (tenant: string): Promise<Peer[]> => {
  return await getConnection().createQueryBuilder().relation(Tenant, 'peers').of(tenant).loadMany();
};

/** Give access to the tenant
 *
 * @params String the tenant id
 * @paramas String the account id to add to the the tenant
 * @return the new permission
 */
export const giveAccess = async (
  tenant: string,
  account: string,
  writeAccess = false
): Promise<AccountPermission.Type> => {
  return await AccountPermission.create(tenant, account, writeAccess);
};

/** Remove access to the tenant
 *
 * @params String the tenant id
 * @paramas String the account id to remove from the tenant
 * @return the removed permission
 */
export const removeAccess = async (tenant: string, account: string): Promise<AccountPermission.Type> => {
  return await AccountPermission.remove(tenant, account);
};

/*
 * User sync section
 */

export interface UserSyncSetting {
  azureAd?: AzureADSyncSetting;
}

export interface AzureADSyncSetting {
  active: boolean;
  clientId: string;
  clientSecret: string;
  refreshToken?: string;
}

/* Get all tenant with AzureAd settings to active
 */
export const getAllActiveAzureAd = async (): Promise<Tenant[]> => {
  const repository = await getTenantRepository();
  return await repository
    .createQueryBuilder()
    .where("(Tenant.userSyncSettings -> 'azureAd' ->> 'active')::boolean is true")
    .getMany();
};

/* Add the Azure AD configuration in a tenant
 *
 * String: the client id given by azure
 * String: the client secret given by azure
 */
export const addAzureADConfiguration = async (clientId: string, clientSecret: string, tenantId: string) => {
  const tenant = await getById(tenantId);

  tenant.userSyncSettings.azureAd = {
    active: false,
    clientId: clientId,
    clientSecret: clientSecret
  };

  return await save(tenant);
};

/* Update the refresh token for the Azure AD user sync
 *
 * String: the new refresh token
 * String: the tenant ID
 */
export const updateAzureADRefreshToken = async (newToken: string, tenantId: string) => {
  const tenant = await getById(tenantId);

  tenant.userSyncSettings.azureAd.refreshToken = newToken;

  // Save do an update if the tenant already exist
  return await save(tenant);
};

/**
 * External authentication settings
 */

export interface ExternalAuthSetting {
  externalAuths?: string[];
  azureTenantId?: string;
}

/* Test if a tenant has any external auth activated
 */
export const hasExternalAuthSetup = async (tenantId: string): Promise<boolean> => {
  const tenant = await getById(tenantId);

  if (
    tenant.externalAuthSettings.externalAuths !== undefined &&
    tenant.externalAuthSettings.externalAuths.length >= 1
  ) {
    return true;
  }

  return false;
};

/* Get the tenant repository
 *
 * A function to prevent mistake when asking for the tenant template repo.
 */
const getTenantRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(Tenant);
};
