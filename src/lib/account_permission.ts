/* This is the public API for account permission management.
 *
 * This file is the only one that should be called outside of this domain.
 * Other domains should never call any other file related to Account Permission
 * since they are considered internal API and may change without any warning.
 */

import {AccountPermission} from '../entity/account_permission';
import {getConnection} from 'typeorm';

/** Export the AccountPermission entity
 *
 * For use in typechecking in other modules.
 * This allow use to mask the entity to the outside world, making sure another
 * domain only loads this file.
 */
export type Type = AccountPermission;

/** Delete a permission
 *
 * Given a permission key (a tenant id and an account id), delete said permission
 *
 * @params string, the tenant id
 * @params string, the account id
 * @return AccountPermission, the deleted permission
 */
export const remove = async (tenant: string, account: string): Promise<AccountPermission> => {
  const repository = await getAccountPermissionRepository();
  const key = {
    account: {id: account},
    tenant: {id: tenant}
  };

  const permission = await repository.findOne(key);

  return await repository.remove(permission);
};

/** Create a new permission
 *
 * @params string, the tenant id
 * @params string, the account id
 * @params bool, if the account has write access to the tenant
 */
export const create = async (tenant: string, account: string, writeAccess = false): Promise<AccountPermission> => {
  const repository = await getAccountPermissionRepository();

  const permission = {
    account: {id: account},
    tenant: {id: tenant},
    writeAccess: writeAccess
  };

  return await repository.save(permission);
};

/**
 * Return a permission
 *
 * @param tenant The tenant ID of the permission
 * @param account The account ID of the permission
 * @return The matching permission
 */
export const get = async (tenant: string, account: string): Promise<AccountPermission> => {
  const repository = await getAccountPermissionRepository();
  return await repository.findOne({tenant: {id: tenant}, account: {id: account}});
};

/**
 * Update a permission
 *
 * @param tenant The tenant ID of the permission to update
 * @param account The account ID of the permission to update
 * @param writeAccess The new value of the write access for the permission
 * @return The updated permission
 */
export const update = async (tenant: string, account: string, writeAccess: boolean): Promise<AccountPermission> => {
  const repository = await getAccountPermissionRepository();

  // We need to create a new instance of the entity to have access to the hooks in the entity listeners subscribers
  // See: https://github.com/typeorm/typeorm/issues/674#issuecomment-346296494
  const entity = Object.assign({}, {
    account: {id: account},
    tenant: {id: tenant},
    writeAccess: writeAccess
  });
  // We need to do a save instead of an update because update is only partial and is not listen to by subscribers
  // See: https://github.com/typeorm/typeorm/issues/2809#issuecomment-451914877
  await repository.save(entity);

  return await repository.findOne({tenant: {id: tenant}, account: {id: account}});

};

/** Return all the permission of an account
 *
 * @params string, the account id
 * @return AccountPermission[], the list of permission
 */
export const listForAccount = async (account: string): Promise<AccountPermission[]> => {
  const repository = await getAccountPermissionRepository();

  return await repository.find({account: {id: account}});
};

/* Get the account permission repository
 *
 * A function to prevent mistake when asking for the account permission repository
 */
const getAccountPermissionRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(AccountPermission);
};
