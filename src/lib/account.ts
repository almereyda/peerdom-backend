/* This is the public API for account management.
 *
 * This file is the only one that should be called outside of this domain.
 * Other domains should never call any other file related to Account since they
 * are considered internal API and may change without any warning.
 */

import {getConnection} from 'typeorm';

import {Account} from '../entity/account';

import {Token, TOKEN_VALIDITY_PERIOD} from '../lib/token';
import * as token from '../lib/token';
import * as AccountPermission from '../lib/account_permission';

/** Given an email, create an account
 *
 * @param string, the email to create an account for
 * @return Account, the created account
 */
export const create = async (email: string) => {
  const repository = await getAccountRepository();
  return await repository.save({email: normalizeEmail(email)});
};

/**
 * Given an account, activate it
 *
 * @param Account, the account to activate
 * @param Account, the updated account
 */
export const activate = async (account): Promise<Account> => {
  const repository = await getAccountRepository();

  account.activated = true;

  return await repository.save(account);
};

/**
 * Given an account, return true if it is activated, false otherwise.
 *
 * @param Account, the account to test
 * @param boolean, true if the account is activated, false otherwise
 */
export const isActivated = (account): boolean => {
  return account.activated;
};

/** Given an email, create the account if it doesn't exist, otherwise
 *  return the existing accounts.
 *
 *  @param string, the email to create an account for
 *  @return Account, the new or existing account
 */
export const upsert = async email => {
  // We create the account if it doesn't already exist
  let account = await findByEmail(email);

  if (!account) {
    account = await create(email);
  }

  return account;
};

/* Return one account based on an email
 *
 * @param String
 * @return Account
 */
export const findByEmail = async (email: string): Promise<Account> => {
  const repository = await getAccountRepository();
  return await repository
    .createQueryBuilder('account')
    .where('LOWER(account.email) = LOWER(:email)', {email: normalizeEmail(email)})
    .getOne();
};

/**
 * Return one account based on its ID
 *
 * @param id The account id
 * @return The account matching the id
 */
export const findById = async (id: string): Promise<Account> => {
  const repository = await getAccountRepository();
  return await repository.findOne(id);
};

/** Check if an account has remaining permission
 *
 * Return a true if an account has at least one permission to any tenant in the server.
 *
 * @param string, the account id
 * @return bool, true if there is one or more permission attached to the account, false otherwise
 */
export const hasPermission = async (account: string): Promise<boolean> => {
  const permissions = await AccountPermission.listForAccount(account);

  if (permissions.length > 0) {
    return true;
  }

  return false;
};

/**
 * Update an account
 *
 * @param id ID of the account to update
 * @param patch Patch of the update
 * @return The updated account
 */
export const update = async (id: string, patch: any): Promise<Account> => {
  const repository = await getAccountRepository();

  // We need to create a new instance of the entity to have access to the hooks in the entity listeners subscribers
  // See: https://github.com/typeorm/typeorm/issues/674#issuecomment-346296494
  const entity = Object.assign({}, {id: id, ...patch});
  // We need to do a save instead of an update because update is only partial and is not listen to by subscribers
  // See: https://github.com/typeorm/typeorm/issues/2809#issuecomment-451914877
  await repository.save(entity);

  return await repository.findOne(id);
};

/** Remove an account from the repository
 *
 * This completly delete an account from the repository
 */
export const remove = async (account: Account): Promise<Account> => {
  const repository = await getAccountRepository();
  return await repository.remove(account);
};

/* Set the reset password token to an account based on the user email and return it
 *
 * @param Account
 * @return Token
 */
export const generatePasswordResetToken = async (email: string): Promise<Token> => {
  const [resetToken, hashedToken] = token.generate();

  const account = await findByEmail(normalizeEmail(email));

  account.resetPasswordToken = hashedToken;
  account.resetPasswordExpires = new Date(Date.now() + TOKEN_VALIDITY_PERIOD);

  await (await getAccountRepository()).save(account);

  return resetToken;
};

/* Normalize an email
 *
 * @param String, the email to normalize
 * @return String, a normalized email
 */
const normalizeEmail = (email: string): string => {
  return email.trim();
};

/* Get the account repository
 *
 * A function to prevent mistake when asking for the node template repo.
 */
const getAccountRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(Account);
};
