/* This is the public API for node management.
 *
 * This file is the only one that should be called outside of this domain.
 * Other domains should never call any other file related to Node since they
 * are considered internal API and may change without any warning.
 */

// TODO Thu Jun 20 12:14:34 2019 Mayeu:
// we should really rename this Map and not node. We are manipulating the map
// here, and calling all that node or tree feels like leaking abstraction from
// the way we are implementing the map.

import {getConnection, getRepository, In, Not, IsNull} from 'typeorm';
import {Node} from '../entity/node';
import {NodeTemplate} from '../entity/node_template';
import {CustomField} from '../entity/custom_field';
import {Holder} from '../entity/holder';
import {slugify, generateUniqueSlug} from '../lib/helpers';

/* Create a node
 *
 * Given all the node data, create a node
 *
 * @param node
 */
export const create = async (newNode, accountId, tenantId) => {
  const repository = await getNodeRepository();
  newNode.slug = await generateUniqueSlugForNode(undefined, newNode.name, newNode.parent);
  if (newNode.parent) {
    newNode.parent = {id: newNode.parent};
  }

  if (newNode.customFields) {
    const newCustomFields = newNode.customFields
      .filter(cf => cf.value)
      .map(cf => ({
        definition: {
          id: cf.definitionId
        },
        value: cf.value
      }));
    const customFieldRepository = await getRepository(CustomField);
    newNode.customFields = await customFieldRepository.save(newCustomFields, {data: {accountId, tenantId}});
  }

  return await repository.save(newNode, {data: {accountId, tenantId}});
};

/* Update a node
 *
 * Given a node id and a patch, update the node
 * If the node is based on a template, update the template and all the other nodes based on it
 *
 * @param nodeId
 * @param patch
 */
export const update = async (context, nodeId, patch) => {
  const nodeRepository = await getNodeRepository();
  const node = await nodeRepository.findOne(nodeId, {
    relations: ['template', 'template.nodes', 'template.nodes.parent', 'parent', 'representatives']
  });
  const isParentUpdate = patch.hasOwnProperty('parent') || patch.hasOwnProperty('external');

  // in case the patch updates the 'external' property of a circle, update its children as well
  if (node.type === 'circle' && isParentUpdate && node.external !== patch.external) {
    const nodes = await getNodeWithChildrenFlatten(context, nodeId);
    // the last item is the node to update, so we can remove it
    const children = nodes.slice(0, -1);
    const patches: Node[] = children.map(c => ({id: c.id, external: patch.external} as Node));
    await nodeRepository.save(patches, {
      data: {accountId: context.account.id, tenantId: context.account.tenant}
    });
  }

  // in case a role node becomes non-electable, set "electedUntil" to NULL for its holders
  if (node.type === 'role' && node.electable && patch.electable === false) {
    const holderRepository = await getHolderRepository();
    const roleHolders = await holderRepository.find({ where: { role: { id: node.id }} });
    const patches = roleHolders.map(holder => ({ id: holder.id, electedUntil: null }));
    await holderRepository.save(patches, {
      data: {accountId: context.account.id, tenantId: context.account.tenant}
    });
  }

  const repPatches: any[] = [];
  // in case any mirrored representatives need to be removed, build those patches first
  if (patch.mirroredRepresentativesToRemove) {
    for (const templateId of patch.mirroredRepresentativesToRemove) {
      const mirroredNodes = await nodeRepository.find({
        where: {template: templateId}
      });
      for (const n of mirroredNodes) {
        repPatches.push({id: n.id, representing: null});
      }
    }
  }

  // check for a change of representatives and update those nodes if necessary
  if (patch.representatives) {
    // check for removed representatives
    for (const rep of node.representatives) {
      if (!patch.representatives.find(r => r.id === rep.id) && !repPatches.find(r => r.id === rep.id)) {
        repPatches.push({id: rep.id, representing: null});
      }
    }

    // check for added representatives
    for (const rep of patch.representatives) {
      // in case the representative needs to be mirrored, update all nodes based on the template
      if (rep.mirrored) {
        const mirroredNodes = await nodeRepository.find({
          where: {template: rep.template},
          relations: ['parent']
        });
        for (const n of mirroredNodes) {
          if (n.parent) {
            repPatches.push({id: n.id, representing: n.parent.id});
          }
        }
      } else if (!node.representatives.find(r => r.id === rep.id)) {
        repPatches.push({id: rep.id, representing: nodeId});
      }
    }
  }

  if (repPatches.length > 0) {
    await nodeRepository.save(repPatches, {
      data: {accountId: context.account.id, tenantId: context.account.tenant}
    });
    delete patch.mirroredRepresentativesToRemove;
    delete patch.representatives;
  }

  if (patch.customFields) {
    const customFieldPatches: any[] = [];
    const customFieldsToRemove: any[] = [];
    for (const cf of patch.customFields) {
      if (cf.value) {
        customFieldPatches.push({
          id: cf.id,
          definition: {
            id: cf.definitionId
          },
          node: {
            id: nodeId
          },
          value: cf.value
        });
      } else if (cf.id) {
        customFieldsToRemove.push({
          id: cf.id
        });
      }
    }

    const customFieldRepository = await getRepository(CustomField);
    if (customFieldPatches.length) {
      patch.customFields = await customFieldRepository.save(customFieldPatches, {
        data: {accountId: context.account.id, tenantId: context.account.tenant}
      });
    } else {
      patch.customFields = [];
    }
    if (customFieldsToRemove.length) {
      await customFieldRepository.remove(customFieldsToRemove, {
        data: {
          accountId: context.account.id,
          tenantId: context.account.tenant
        }
      });
    }
  }

  if (node.template === null || isParentUpdate) {
    const entity = Object.assign(new Node(), {id: nodeId, ...patch});
    if (entity.parent) {
      entity.parent = {id: patch.parent};
    }

    // in case of a name or parent update, the slug needs to be regenerated
    if (patch.name || isParentUpdate) {
      const name = patch.hasOwnProperty('name') ? patch.name : node.name;
      const parent = patch.hasOwnProperty('parent') ? patch.parent : node.parent && node.parent.id;
      entity.slug = await generateUniqueSlugForNode(nodeId, name, parent);
    }

    // in case of a parent update, the representative relationship should be cleared
    if (isParentUpdate) {
      entity.representing = null;
    }

    await nodeRepository.save(entity, {data: {accountId: context.account.id, tenantId: context.account.tenant}});
  } else {
    // node based on a template: update the template and all the nodes based on it
    const nodeTemplateRepository = await getNodeTemplateRepository();
    const nodeTemplateEntity = Object.assign(new NodeTemplate(), {id: node.template.id, ...patch});
    await nodeTemplateRepository.save(nodeTemplateEntity, {
      data: {accountId: context.account.id, tenantId: context.account.tenant}
    });
    for (const nodeFromTemplate of node.template.nodes) {
      const nodeEntity = Object.assign(new Node(), {id: nodeFromTemplate.id, ...patch});
      if (patch.name) {
        nodeEntity.slug = await generateUniqueSlugForNode(
          nodeFromTemplate.id,
          patch.name,
          nodeFromTemplate.parent ? nodeFromTemplate.parent.id : null
        );
      }
      await nodeRepository.save(nodeEntity, {data: {accountId: context.account.id, tenantId: context.account.tenant}});
    }
  }

  return await nodeRepository.findOne(nodeId);
};

export const copy = async (
  context: any,
  nodeId: string,
  parentId: string,
  external: boolean = false,
  withTemplate: boolean = undefined
) => {
  // duplicate the node and all of its children
  const original = await getNodeWithChildren(context, nodeId);
  const duplicate = await duplicateNodeRecursively(original, context.account.id, context.account.tenant, withTemplate);
  duplicate.slug = await generateUniqueSlugForNode(undefined, duplicate.name, parentId);

  // insert all of the duplicates into the database (without a parent ID)
  const nodeRepository = await getNodeRepository();
  const flattenedDuplicate: Node[] = flattenNode(duplicate);
  const nodesToInsert = flattenedDuplicate.map(node => {
    const toInsert = Object.assign(new Node(), node);
    delete toInsert.id;
    delete toInsert.parent;
    delete toInsert.children;
    return toInsert;
  });
  const insertedNodes = await nodeRepository.save(nodesToInsert, {
    data: {accountId: context.account.id, tenantId: context.account.tenant}
  });

  // patch the duplicates to include their parent ID
  const patches = getParentIdPatchesForCopies(duplicate, parentId, external, flattenedDuplicate, insertedNodes);
  const result = await nodeRepository.save(patches, {
    data: {accountId: context.account.id, tenantId: context.account.tenant}
  });

  // return the duplicated node
  return result.find(n => n.parent.id === parentId);
};

/**
 * Duplicates a given node, including all of its children.
 * In case withTemplate is set to true and the given node is not template-based, it saves a new template in the DB.
 *
 * @param originalNode  the node to duplicate
 * @param accountId     the ID of the account that does the duplication
 * @param tenantId      the active tenant
 * @param withTemplate  whether the template should be kept for the duplicate
 */
const duplicateNodeRecursively = async (
  originalNode: Node,
  accountId: string,
  tenantId: string,
  withTemplate: boolean = undefined // a default of undefined will preserve the template only for template-based roles
) => {
  const children = originalNode.children || [];
  const copiedNode = Object.assign(new Node(), {...originalNode, tenant: {id: tenantId}});
  delete copiedNode.external;
  delete copiedNode.children;
  delete copiedNode.holders;
  delete copiedNode.createdAt;
  delete copiedNode.updatedAt;

  // check whether the copy should keep the template
  if (withTemplate === true) {
    // check whether the original node is based on a template
    if (!originalNode.template) {
      // create a new template
      const nodeTemplateRepository = await getNodeTemplateRepository();
      let template = Object.assign(new NodeTemplate(), copiedNode);
      template = await nodeTemplateRepository.save(template, {
        data: {accountId, tenantId}
      });

      // update the original node
      const nodeRepository = await getNodeRepository();
      const originalNodeUpdate = Object.assign(new Node(), {id: originalNode.id, template});
      await nodeRepository.save(originalNodeUpdate, {
        data: {accountId, tenantId}
      });

      // add the newly created template to the copy
      copiedNode.template = template;
    }
  } else if (withTemplate === false) {
    delete copiedNode.template;
  }

  if (withTemplate !== true && originalNode.customFields && originalNode.customFields.length) {
    const copiedCustomFields = originalNode.customFields.map(cf => ({
      definition: cf.definition,
      value: cf.value
    }));
    const customFieldRepository = await getRepository(CustomField);
    copiedNode.customFields = await customFieldRepository.save(copiedCustomFields, {data: {accountId, tenantId}});
  }

  copiedNode.children = await Promise.all(
    children.map(child => duplicateNodeRecursively(child, accountId, tenantId, withTemplate))
  );
  return copiedNode;
};

/**
 * Recursively builds an array of patches used for setting the parent ID of duplicated nodes.
 *
 * @param node        a node containing all its children and grandchildren
 * @param parentId    the parent ID that needs to be set in the patch
 * @param external    whether the node is external or not
 * @param originals   a list of original nodes
 * @param copies      a list of duplicated nodes
 */
const getParentIdPatchesForCopies = (
  node: Node,
  parentId: string,
  external: boolean,
  originals: Node[],
  copies: Node[]
): any[] => {
  // build the patch for the current node
  const index = originals.findIndex(n => n.id === node.id);
  const newNode = copies[index];
  const patch = {
    id: newNode.id,
    parent: {id: parentId},
    external
  };

  // build the patches for the node's children, reducing the result to a single array
  let patches = [];
  if (node.children && node.children.length > 0) {
    const childPatches = node.children.map(child =>
      getParentIdPatchesForCopies(child, newNode.id, external, originals, copies)
    );
    patches = childPatches.reduce((acc: any[], p: any) => [...acc, ...p]);
  }
  return [patch, ...patches];
};

/* Remove a node
 *
 * Given a node ID, delete the node and return the deleted node
 *
 * It also retrieves and prepares all the children nodes and holders that will be removed because of cascading and
 * add them as `data` when asking TypeORM to remove the node.
 * This will be read by the subscriber to the event and it will handle writing a log entry for those removals as well.
 *
 * @param nodeID
 */
export const remove = async (context, nodeId) => {
  const repository = await getNodeRepository();
  const nodeToDelete = await repository.findOne(nodeId, {relations: ['parent', 'customFields']});
  if (nodeToDelete.parent) {
    nodeToDelete.parent = {id: nodeToDelete.parent.id} as any;
  }
  const nodeWithChildrenFlatten = await getNodeWithChildrenFlatten(context, nodeId);
  const customFieldIdsToCheck = nodeToDelete.customFields.map(cf => cf.id);
  let cascadeItems = [];

  if (nodeToDelete.type === 'circle') {
    // The last item is the nodeToDelete so we can remove it
    const children = nodeWithChildrenFlatten.slice(0, -1);

    let cascadeItemsHolder = [];
    const cascadeItemsNode = children.map((node: Node) => {
      for (const customField of node.customFields) {
        if (!customFieldIdsToCheck.find(id => id === customField.id)) {
          customFieldIdsToCheck.push(customField.id);
        }
      }

      if (node.holders) {
        node.holders.forEach(holder => {
          cascadeItemsHolder.push({...holder, role: node});
        });
        delete node.holders; // We don't need to save this in the log
      }
      if (node.parent) {
        node.parent = {id: node.parent.id} as any;
      }
      delete node.children; // We don't need to save this in the log
      return {item: node, entity: 'node'};
    });

    cascadeItemsHolder = cascadeItemsHolder.map(holder => ({
      item: {...holder, peer: {id: holder.peer.id}, role: {id: holder.role.id}},
      entity: 'holder'
    }));

    cascadeItems = cascadeItemsHolder.concat(cascadeItemsNode);
  } else if (nodeToDelete.type === 'role') {
    const holders = await getRepository(Holder).find({
      relations: ['peer', 'role'],
      where: {
        role: {id: nodeToDelete.id}
      }
    });
    cascadeItems = holders.map(holder => ({
      item: {...holder, peer: {id: holder.peer.id}, role: {id: holder.role.id}},
      entity: 'holder'
    }));
  }

  await repository.remove(nodeToDelete, {
    data: {
      accountId: context.account.id,
      tenantId: context.account.tenant,
      cascadeItems: cascadeItems
    }
  });
  await cleanupCustomFields(context, customFieldIdsToCheck);

  // get the templates of removed template-based nodes (if any)
  const templates: NodeTemplate[] = [];
  for (const node of nodeWithChildrenFlatten) {
    if (node.template && !templates.find(t => t.id === node.template.id)) {
      templates.push(node.template);
    }
  }
  // check whether any of the templates needs to be removed as well
  if (templates.length > 0) {
    await cleanupNodeTemplates(context, templates);
  }

  // We add back the nodeId as it is changed to undefined after the removal
  return {...nodeToDelete, id: nodeId};
};

/* Return a full Map for the frontend
 *
 * Given a tenant and some optional option, return the matching nodes
 *
 * @param tenant
 * @param option
 */
interface GetNodesOptions {
  resolveChildren?: boolean;
  resolveDirectPeers?: boolean;
  resolveParent?: boolean;
  resolveTemplate?: boolean;
  resolveRepresentatives?: boolean;
  nodeIds?: number[];
  nodeType?: string;
}
export const getNodes = async (tenant, options: GetNodesOptions) => {
  const repository = await getNodeRepository();

  // Starting to build the query options
  //
  // TODO Wed Jun 26 11:00:51 2019 by Mayeu
  // It could be worth to extract all the filtering/retrieving if that build
  // the query option into a sets of small function with good name to directly
  // show the intend of all this via the code, and make `getNodes` more
  // expressive
  const findOptions: any = {
    where: {
      tenant: tenant
    },
    relations: ['holders', 'holders.peer', 'customFields', 'customFields.definition']
  };

  // Filter by ID
  if (options.nodeIds) {
    findOptions.where.id = In(options.nodeIds);
  }

  // Filter by node type
  if (options.nodeType) {
    findOptions.where.type = options.nodeType;
  }

  // Retrieve children only if the GraphQL query asks for it.
  if (options.resolveChildren || options.resolveDirectPeers) {
    findOptions.relations = findOptions.relations.concat([
      'children',
      'children.holders',
      'children.holders.peer',
      'children.customFields',
      'children.customFields.definition'
    ]);
  }

  if (options.resolveParent) {
    findOptions.relations = findOptions.relations.concat(['parent']);
  }

  // Retrieve the template ID if requested
  if (options.resolveTemplate) {
    findOptions.relations = findOptions.relations.concat(['template', 'children.template']);
  }

  if (options.resolveRepresentatives) {
    findOptions.relations = findOptions.relations.concat([
      'representatives',
      'representatives.holders',
      'representatives.holders.peer'
    ]);
  }

  return await repository.find(findOptions);
};

/* Get the node repository
 *
 * A function to prevent mistake when asking for the node repo.
 */
const getNodeRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(Node);
};

/* Get the holder repository
 *
 * A function to prevent mistake when asking for the holder repo.
 */
const getHolderRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(Holder);
};

/* Get the node template repository
 *
 * A function to prevent mistake when asking for the node template repo.
 */
const getNodeTemplateRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(NodeTemplate);
};

/**
 * Gets the slugs of all children of a particular node.
 *
 * @param parentId         the ID of the parent of interest
 * @param nodeIdToIgnore   an optional node ID to ignore for the list
 */
const getChildrenSlugs = async (parentId: string, nodeIdToIgnore?: string) => {
  const repository = await getNodeRepository();
  const query: any = {
    select: ['slug'],
    where: {parent: parentId ? parentId : IsNull()}
  };
  if (nodeIdToIgnore) {
    query.where.id = Not(nodeIdToIgnore);
  }
  const children = await repository.find(query);
  return children.map(child => child.slug);
};

/**
 * Generates a unique slug for a node.
 *
 * @param nodeId    the ID of the node of interest
 * @param nodeName  the name of the node
 * @param parentId  the ID of the parent
 */
const generateUniqueSlugForNode = async (nodeId: string, nodeName: string, parentId: string) => {
  let slug = slugify(nodeName);
  const conflictingSlugs = await getChildrenSlugs(parentId, nodeId);
  conflictingSlugs.push('external');

  // Check for slug conflicts and use auto resolution if necessary
  if (conflictingSlugs.includes(slug)) {
    slug = generateUniqueSlug(slug, conflictingSlugs);
  }

  return slug;
};

/**
 * Cleans up node templates which are bound to one node only.
 *
 * @param context     the context, containing the account making the operation
 * @param templates   the list of templates to check
 */
const cleanupNodeTemplates = async (context, templates: NodeTemplate[]) => {
  const nodeRepository = await getNodeRepository();
  const templateIds = templates.map(t => t.id);
  const nodes = await nodeRepository.find({
    where: {template: In(templateIds)},
    relations: ['template']
  });
  const nodesToUpdate: Node[] = [];

  for (const template of templates) {
    // check if there is a node based on the template
    const nodeIndex = nodes.findIndex(n => n.template.id === template.id);

    if (nodeIndex !== -1) {
      const node = nodes[nodeIndex];
      nodes.splice(nodeIndex, 1);

      // in case there is only one node based on the template, the template needs to be deleted
      if (!nodes.find(n => n.template.id === template.id)) {
        nodesToUpdate.push(node);
      }
    }
  }

  const templatesToDelete: NodeTemplate[] = [];
  if (nodesToUpdate.length > 0) {
    // remove all obsolete templates from the nodes
    for (const node of nodesToUpdate) {
      templatesToDelete.push(node.template);
      const nodeUpdate = Object.assign(new Node(), {id: node.id, template: null});
      await nodeRepository.save(nodeUpdate, {
        data: {
          accountId: context.account.id,
          tenantId: context.account.tenant
        }
      });
    }

    // delete all obsolete templates
    const nodeTemplateRepository = await getNodeTemplateRepository();
    await nodeTemplateRepository.remove(templatesToDelete, {
      data: {
        accountId: context.account.id,
        tenantId: context.account.tenant
      }
    });
  }

  return templatesToDelete;
};

/**
 * Cleans up custom fields no longer bound to any node.
 *
 * @param context   the context, containing account and tenant info
 * @param ids       the custom field IDs to check for removal
 */
const cleanupCustomFields = async (context, ids) => {
  const customFieldRepository = getRepository(CustomField);
  const customFields = await customFieldRepository.findByIds(ids, {
    loadRelationIds: {
      relations: ['nodes']
    }
  });
  const customFieldsToRemove = customFields.filter(field => !field.nodes || !field.nodes.length);
  if (customFieldsToRemove.length) {
    await customFieldRepository.remove(customFieldsToRemove, {
      data: {
        accountId: context.account.id,
        tenantId: context.account.tenant
      },
      chunk: 50
    });
  }
};

/* Get the node with all its children flatten next to it
 * The node will be the list node of the array
 * All its children includes the children of its children recursively
 */
export const getNodeWithChildrenFlatten = async (context, nodeId): Promise<Node[]> => {
  const nodeWithChildren = await getNodeWithChildren(context, nodeId);
  return flattenNode(nodeWithChildren);
};

/* Get the node with all its children
 * All its children includes the children of its children recursively
 */
export const getNodeWithChildren = async (context, nodeId): Promise<Node> => {
  const nodes = await getNodes(context.account.tenant, {
    resolveChildren: true,
    resolveParent: true,
    resolveTemplate: true,
    nodeIds: [nodeId]
  });
  const parentNode = nodes[0];
  if (parentNode) {
    parentNode.children = await Promise.all(
      parentNode.children.map(async child => {
        if (child.type === 'circle') {
          return await getNodeWithChildren(context, child.id);
        } else {
          return {...child, parent: {id: parentNode.id}} as any;
        }
      })
    );
  }
  return parentNode;
};

/* Helper recursive function for getNodeWithChildrenFlatten
 *
 */
function flattenNode(node): Node[] {
  const children = node.children;
  if (Array.isArray(children)) {
    return children
      .reduce(function(done, curr) {
        return done.concat(flattenNode(curr));
      }, [])
      .concat(node);
  } else {
    return node;
  }
}
