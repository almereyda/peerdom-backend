/* This is the public API for project management.
 *
 * This file is the only one that should be called outside of this domain.
 * Other domains should never call any other file related to Project since they
 * are considered internal API and may change without any warning.
 */

import {getConnection, Like, Not, In} from 'typeorm';
import {slugify, generateUniqueSlug, WithMandatory, Replace} from './helpers';

import {Project} from '../entity/project';

/** Export the Project entity
 *
 * For use in typechecking in other modules.
 * This allow use to mask the entity to the outside world, making sure another
 * domain only loads this file.
 */
export type Type = Project;

interface GetProjectsOptions {
  resolveChildren?: boolean;
  resolveParent?: boolean;
  resolveHolders?: boolean;
  rootOnly?: boolean;
  projectId?: string;
  slug?: string;
  archived?: boolean;
}

/**
 * Retrieve projects from a specific tenant
 *
 * @param tenantId ID of the tenant from which to retrieve the projects
 * @param options Options to specify the IDs of projects and which relations to retrieve
 */
export const get = async (tenantId: string, options: GetProjectsOptions) => {
  const repository = await getProjectRepository();

  const findOptions: any = {
    where: {
      tenant: tenantId
    },
    order: {
      name: 'ASC'
    },
    relations: []
  };

  if (options.rootOnly) {
    // This will retrieve only the root projects.
    // If `options.resolveChildren` is true, children projects will be included only as children.
    findOptions.where.parent = null;
  }

  if (options.projectId) {
    findOptions.where.id = options.projectId;
  }

  if (options.slug) {
    findOptions.where.slug = options.slug;
  }

  if (options.archived !== undefined && options.archived !== null) {
    findOptions.where.archived = options.archived;
  }

  if (options.resolveParent) {
    findOptions.relations.push('parent');
  }
  if (options.resolveChildren) {
    // This enables to retrieve data about the direct children, but not deeper.
    // It means that we currently can provide only two levels of projects.
    findOptions.relations.push('children');
  }
  if (options.resolveHolders) {
    findOptions.relations.push('holders');
    findOptions.relations.push('holders.role');
    findOptions.relations.push('holders.role.parent');
    findOptions.relations.push('holders.role.template');
    findOptions.relations.push('holders.peer');
  }

  return await repository.find(findOptions);
};

/**
 * Creates a project in the database.
 *
 * @param newProject  the project to create
 * @param accountId   the ID of the account creating the project
 * @param tenantId    the ID of the tenant to add the project to
 */
export const create = async (
  newProject: Replace<WithMandatory<Project, 'name'>, 'holders', string[]>,
  accountId: string,
  tenantId: string
) => {
  const repository = await getProjectRepository();
  // We need to create a new instance of the entity to have access to the hooks in the entity listeners subscribers
  // See: https://github.com/typeorm/typeorm/issues/674#issuecomment-346296494
  const project = Object.assign(new Project(), {
    ...newProject,
    slug: await generateUniqueSlugForProject(undefined, newProject.name, tenantId),
    tenant: {id: tenantId},
    holders: newProject.holders.map(holderId => ({id: holderId}))
  });

  return await repository.save(project, {data: {accountId, tenantId}});
};

/**
 * Updates a project in the database.
 *
 * @param projectId      the ID of the project to update
 * @param patch          the update patch
 * @param accountId      the ID of the account updating the project
 * @param tenantId       the ID of the tenant to update the project in
 */
export const update = async (projectId: string, patch: any, accountId: string, tenantId: string) => {
  const repository = await getProjectRepository();

  if (patch.name) {
    patch.slug = await generateUniqueSlugForProject(projectId, patch.name, tenantId);
  }

  if (patch.holders) {
    patch.holders = patch.holders.map(holderId => ({id: holderId}));
  }

  // We need to create a new instance of the entity to have access to the hooks in the entity listeners subscribers
  // See: https://github.com/typeorm/typeorm/issues/674#issuecomment-346296494
  const entity = Object.assign({}, {id: projectId, ...patch});
  // We need to do a save instead of an update because update is only partial and is not listen to by subscribers
  // See: https://github.com/typeorm/typeorm/issues/2809#issuecomment-451914877
  await repository.save(entity, {data: {accountId, tenantId}});

  return await repository.findOne(projectId);
};

/**
 * Removes a project from the database.
 *
 * @param projectId     the ID of the project to remove
 * @param accountId     the ID of the account removing the project
 * @param tenantId      the ID of the tenant to remove the project from
 */
export const remove = async (projectId: string, accountId: string, tenantId: string) => {
  const repository = await getProjectRepository();
  const project = await repository.findOne(projectId);
  const removedProject = Object.assign({}, project);
  await repository.remove(project, {
    data: {
      accountId: accountId,
      tenantId: tenantId
    }
  });
  return removedProject;
};

/* Get the project repository
 *
 * A function to prevent mistake when asking for the account permission repository
 */
const getProjectRepository = async () => {
  const connection = await getConnection();
  return await connection.getRepository(Project);
};

/**
 * Gets all slugs similar to the one of interest.
 *
 * @param slug                the slug of interest
 * @param tenantId            the ID of the tenant
 * @param projectIdToIgnore   an optional project ID to ignore for the list
 */
const getSimilarSlugs = async (slug: string, tenantId: string, projectIdToIgnore?: string) => {
  const repository = await getProjectRepository();
  const query: any = {
    select: ['slug'],
    where: {slug: Like(`${slug}%`), tenant: {id: tenantId}}
  };
  if (projectIdToIgnore) {
    query.where.id = Not(projectIdToIgnore);
  }
  const projectsWithSimilarSlugs = await repository.find(query);
  return projectsWithSimilarSlugs.map(n => n.slug);
};

/**
 * Generates a unique slug for a project.
 *
 * @param projectId     the ID of the project
 * @param projectName   the project's name
 * @param tenantId      the ID of the tenant
 */
const generateUniqueSlugForProject = async (projectId: string, projectName: string, tenantId: string) => {
  let slug = slugify(projectName);

  // Check for slug conflicts and use auto resolution if necessary
  const similarSlugs = await getSimilarSlugs(slug, tenantId, projectId);
  if (similarSlugs.includes(slug)) {
    slug = generateUniqueSlug(slug, similarSlugs);
  }

  return slug;
};
