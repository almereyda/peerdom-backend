import {InsertEvent, UpdateEvent, getRepository, RemoveEvent} from 'typeorm';
import * as uuid from 'uuid/v4';
import * as packageJson from '../../package.json';
import {EventsLog} from '../entity/events_log';
import {Tenant} from '../entity/tenant';
import {Account} from '../entity/account';

interface CommonEntity {
  id: string;
  createdAt: Date;
  updatedAt: Date;
}

export const saveEventLogFromItemInsertEvent = async <Entity extends CommonEntity>(
  itemInsertEvent: InsertEvent<Entity>,
  tenantId: string,
  accountId: string
): Promise<EventsLog> => {
  const itemLog: EventsLog = {
    id: uuid(),
    entity: itemInsertEvent.metadata.tableName,
    action: 'insert',
    author: {id: accountId} as Account,
    change: null,
    tenant: {id: tenantId} as Tenant,
    version: packageJson.version,
    createdAt: itemInsertEvent.entity.createdAt,
    item: itemInsertEvent.entity.id,
    cascadeTriggered: false
  };
  itemLog.change = JSON.parse(JSON.stringify(itemInsertEvent.entity));

  delete itemLog.change.id;
  delete itemLog.change.createdAt;
  delete itemLog.change.updatedAt;
  delete itemLog.change.tenant;

  // Structure of the change property:
  //  {
  //    Fields of the inserted row except from "id", "createdAt", "updatedAt"
  //  }

  return getRepository(EventsLog).save(itemLog);
};

export const saveEventLogFromItemUpdateEvent = async <Entity extends CommonEntity>(
  itemUpdateEvent: UpdateEvent<Entity>,
  tenantId: string,
  accountId: string
): Promise<EventsLog> => {
  // INFO: this method is called  only when information is changed in the model.
  // See: https://github.com/typeorm/typeorm/issues/2246
  const itemLog: EventsLog = {
    id: uuid(),
    entity: itemUpdateEvent.metadata.tableName,
    action: 'update',
    author: {id: accountId} as Account,
    change: null,
    tenant: {id: tenantId} as Tenant,
    version: packageJson.version,
    createdAt: itemUpdateEvent.entity.updatedAt,
    item: itemUpdateEvent.databaseEntity.id,
    cascadeTriggered: false
  };

  // Structure of the change property:
  //  {
  //    "before": {previous value of the field(s) that was(were) changed, key being the name of the field},
  //    "after": {new value of the field(s) that was(were) changed, key being the name of the field},
  //  }

  itemLog.change = Object.keys(itemUpdateEvent.entity).reduce(
    (acc, prop) => {
      if (
        prop !== 'id' &&
        prop !== 'updatedAt' &&
        // The entity prop is only null if it doesn't belong to the patch
        itemUpdateEvent.entity[prop] !== null &&
        itemUpdateEvent.databaseEntity[prop] !== itemUpdateEvent.entity[prop]
      ) {
        acc.before[prop] = itemUpdateEvent.databaseEntity[prop];
        acc.after[prop] = itemUpdateEvent.entity[prop];
      }
      return acc;
    },
    {before: {}, after: {}}
  );
  return getRepository(EventsLog).save(itemLog);
};

export const saveEventLogFromItemRemoveEvent = async <Entity extends CommonEntity>(
  itemRemoveEvent: RemoveEvent<Entity>,
  tenantId: string,
  accountId: string
): Promise<EventsLog> => {
  const itemLog: EventsLog = {
    id: uuid(),
    entity: itemRemoveEvent.metadata.tableName,
    action: 'delete',
    author: {id: accountId} as Account,
    change: null,
    tenant: {id: tenantId} as Tenant,
    version: packageJson.version,
    // We need to set a date ourselves as there is no deletedAt property on Entity in TypeORM
    createdAt: new Date(),
    item: itemRemoveEvent.databaseEntity.id,
    cascadeTriggered: false
  };

  itemLog.change = JSON.parse(JSON.stringify(itemRemoveEvent.entity));

  delete itemLog.change.createdAt;
  delete itemLog.change.updatedAt;

  // Structure of the change property:
  // {
  //   Fields (just before the delete) of the delete row except from "id", "createdAt", "updatedAt"
  // }

  return getRepository(EventsLog).save(itemLog);
};

export const saveEventLogFromCascadeManualRemove = async <Entity extends CommonEntity>(
  cascadeItemsToRemove: {entity: string; item: Entity}[],
  tenantId: string,
  accountId: string
): Promise<void> => {
  for (let index = 0; index < cascadeItemsToRemove.length; index++) {
    const cascadeItemToRemove = cascadeItemsToRemove[index];
    const itemLog: EventsLog = {
      id: uuid(),
      entity: cascadeItemToRemove.entity,
      action: 'delete',
      author: {id: accountId} as Account,
      change: null,
      tenant: {id: tenantId} as Tenant,
      version: packageJson.version,
      // We need to set a date ourselves as there is no deletedAt property on Entity in TypeORM
      createdAt: new Date(),
      item: cascadeItemToRemove.item.id,
      cascadeTriggered: true
    };

    itemLog.change = JSON.parse(JSON.stringify(cascadeItemToRemove.item));

    delete itemLog.change.id;
    delete itemLog.change.createdAt;
    delete itemLog.change.updatedAt;
    await getRepository(EventsLog).save(itemLog);
  }
};
