import * as crypto from 'crypto';
import {getRepository} from 'typeorm';
import {ShareToken} from '../entity/share_token';
import {dumbLog} from '../lib/helpers';

/**
 * Route handler that responds to a share token check GET request.
 *
 * @param req
 * @param res
 * @param next
 */
export const checkShareTokenHandler = async (req, res, next) => {
  dumbLog({handler: 'checkShareToken', params: req.params});

  const tokenParam = req.params.token;
  const shareTokenRepo = getRepository(ShareToken);
  const token = await shareTokenRepo.findOne(tokenParam, {relations: ['tenant']});

  if (token && token.valid) {
    res.json({
      id: token.id,
      app: token.app,
      activatedAt: token.createdAt,
      tenantSlug: token.tenant.slug
    });
  } else {
    next();
    return;
  }
};

/**
 * Route handler that responds to a GET request for a valid share token.
 *
 * @param req
 * @param res
 * @param next
 */
export const getValidShareTokenHandler = async (req, res, next) => {
  dumbLog({handler: 'getValidShareToken', account: req.account, params: req.params});

  if (!req.account || !req.account.tenant) {
    next();
    return;
  }
  const tenantId = req.account.tenant;
  const tenantSlug = req.params.tenant;
  const shareTokenRepo = getRepository(ShareToken);

  const validTokens = await shareTokenRepo.find({
    tenant: tenantId,
    valid: true
  });
  // NOTE: multiple tokens can be valid once sharing of apps other than 'map' is enabled
  if (validTokens && validTokens.length === 1) {
    const token = validTokens[0];
    res.json({
      id: token.id,
      app: token.app,
      activatedAt: token.createdAt,
      tenantSlug
    });
  } else {
    next();
    return;
  }
};

/**
 * Route handler that responds to a share token generation POST request.
 *
 * @param req
 * @param res
 * @param next
 */
export const generateShareTokenHandler = async (req, res, next) => {
  dumbLog({handler: 'generateShareToken', account: req.account, params: req.params});

  if (!req.account || !req.account.tenant) {
    next();
    return;
  }
  const tenantId = req.account.tenant;
  const tenantSlug = req.params.tenant;
  const shareTokenRepo = getRepository(ShareToken);

  // invalidate valid token (if existing)
  const validTokens = await shareTokenRepo.find({
    tenant: tenantId,
    valid: true
  });
  // NOTE: multiple tokens can be valid once sharing of apps other than 'map' is enabled
  if (validTokens && validTokens.length === 1) {
    const validTokenId = validTokens[0].id;
    const patch = {id: validTokenId, valid: false};
    await shareTokenRepo.save(patch);
  }

  // generate a new token, making sure that it's not a duplicate
  let newId: string;
  let existingToken: ShareToken;
  do {
    newId = crypto.randomBytes(8).toString('hex');
    existingToken = await shareTokenRepo.findOne(newId);
  } while (existingToken);
  const entity = {
    id: newId,
    tenant: {id: tenantId},
    valid: true
  };

  await shareTokenRepo.save(entity);
  const token = await shareTokenRepo.findOne(newId);
  res.json({
    id: token.id,
    app: token.app,
    activatedAt: token.createdAt,
    tenantSlug
  });
};

/**
 * Route handler that responds to a PUT request for token invalidation.
 *
 * @param req
 * @param res
 * @param next
 */
export const invalidateShareTokenHandler = async (req, res, next) => {
  dumbLog({handler: 'invalidateShareToken', account: req.account, params: req.params});

  if (!req.account || !req.account.tenant) {
    next();
    return;
  }

  const tokenParam = req.params.token;
  const shareTokenRepo = getRepository(ShareToken);
  const token = await shareTokenRepo.findOne(tokenParam);
  if (!token) {
    next();
    return;
  }

  const patch = {id: tokenParam, valid: false};
  await shareTokenRepo.save(patch);
  res.json({success: true});
};
