import * as crypto from 'crypto';
import * as bcrypt from 'bcrypt';
import {Strategy as LocalStrategy} from 'passport-local';
import {getRepository} from 'typeorm';

import {config} from '../peerdom-config';
import {Token} from '../entity/token';
import {Account} from '../entity/account';
import {Tenant} from '../entity/tenant';
import {AccountPermission} from '../entity/account_permission';
import {cryptoHashString, cryptoHashStringWithSalt} from '../lib/helpers';
import * as accountLib from '../lib/account';
import {dumbLog} from '../lib/helpers';

/**
 * Time in ms after an inactive token will be considered invalid.
 * Corresponds to 7 days.
 * @type {number}
 */
const TOKEN_VALIDITY_PERIOD = 1000 * 60 * 60 * 24 * 7;

/**
 * Minimum time in ms between two updates of a token's updatedAt date.
 * Corresponds to 8 hours.
 * @type {number}
 */
const TOKEN_ACTIVE_PERIOD = 1000 * 60 * 60 * 8;

/**
 * Number of rounds to be used to salt a password hash.
 * @type {number}
 */
const SALT_ROUNDS = 10;

/**
 * Check if there is a account with the given email and password,
 * and if there is, create a new token for that account.
 * @param email
 * @param password
 */
const localStrategyResolver = async (email: string, password: string) => {
  const account = await accountLib.findByEmail(email);

  if (!account) {
    // If no account found, return empty
    return null;
  }

  if (account && !(account.password && (await compareHash(password, account.password)))) {
    // If no password in DB or given password doesn't match password in DB, return empty
    return null;
  }

  // Create a new token
  const tokenPlainId = generateToken(15);
  const tokenRepo = getRepository(Token);
  const token = {
    id: cryptoHashString(tokenPlainId),
    account: {
      id: account.id
    },
    xsrfSalt: generateToken(6)
  };
  await tokenRepo.save(token);

  return {
    token: tokenPlainId
  };
};

export const localStrategy = new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password'
  },
  (email, password, done) => {
    // Map async/await method to express callbacks
    localStrategyResolver(email, password)
      .then(accountInfo => {
        if (accountInfo) {
          done(null, accountInfo);
        } else {
          done(null, false, {message: 'Wrong credentials'});
        }
      })
      .catch(done);
  }
);

/**
 * Respond to the login POST request
 * @param req
 * @param res
 */
export const loginHandler = (req, res) => {
  dumbLog({handler: 'login', authToken: req.user.token});
  // Token is created by Passport
  const cookieOptions = {
    maxAge: TOKEN_VALIDITY_PERIOD,
    httpOnly: true,
    path: '/',
    domain: config.domain ? '.' + config.domain : undefined,
    secure: process.env.NODE_ENV === 'production'
  };
  res.cookie('authToken', req.user.token, cookieOptions);
  res.send();
};

/**
 * Respond to the logout POST request
 * @param req
 * @param res
 * @param next
 */
export const logoutHandler = (req, res, next) => {
  dumbLog({handler: 'logout', authToken: req.cookies['authToken']});
  const hashedReceivedToken = cryptoHashString(req.cookies['authToken']);

  const tokenRepo = getRepository(Token);
  tokenRepo
    .findOne(hashedReceivedToken)
    .then(token => {
      if (token) {
        return tokenRepo.remove(token);
      }
    })
    .then(() => {
      res.clearCookie('authToken', {path: '/'});
      res.json({success: true});
    })
    .catch(next);
};

/**
 * Express middleware that checks the headers for an auth token
 * and adds the account info to the req object if the token is valid.
 * TODO: Use async/await
 * @param req
 * @param res
 * @param next
 */
export const addAccountToRequest = (req, res, next) => {
  const tenantSlug = req.params.tenant;
  let hashedReceivedToken;
  let shareToken;

  if (req.cookies['authToken']) {
    // console.log('🔑 auth.ts: received a request with an authToken');
    hashedReceivedToken = cryptoHashString(req.cookies['authToken']);
  }

  if (req.cookies['shareToken']) {
    shareToken = JSON.parse(req.cookies['shareToken']);
  }

  const accountRepo = getRepository(Account);
  const tokenRepo = getRepository(Token);
  const tenantRepo = getRepository(Tenant);

  tenantRepo
    .findOne({
      where: {
        slug: tenantSlug
      },
      relations: shareToken ? ['shareTokens'] : []
    })

    .then((tenant: Tenant) => {
      if (typeof hashedReceivedToken === 'string' && hashedReceivedToken.length > 0) {
        // console.log('🔑 auth.ts: trying to match the token with a valid tenant and account');
        return new Promise((resolve, reject) => {
          if (tenant) {
            accountRepo
              .createQueryBuilder('account')
              .select(['account.id', 'account.email'])
              .leftJoinAndSelect('account.permissions', 'accountPermission')
              .leftJoinAndSelect('account.tokens', 'token')
              .where('accountPermission.tenant = :tenantId', {tenantId: tenant.id})
              .andWhere('token.id = :tokenId', {tokenId: hashedReceivedToken})
              .getOne()
              .then((account: Account) => {
                resolve({account, tenant});
              })
              .catch(err => {
                reject(err);
              });
          } else {
            // No tenant provided (used notably by the /me endpoint)
            accountRepo
              .createQueryBuilder('account')
              .select(['account.id', 'account.email'])
              .leftJoinAndSelect('account.tokens', 'token')
              .where('token.id = :tokenId', {tokenId: hashedReceivedToken})
              .getOne()
              .then((account: Account) => {
                resolve({account});
              })
              .catch(err => {
                reject(err);
              });
          }
        });
      } else if (tenant && (tenant.public || shareToken)) {
        // by Mayeu: TODO: what is tested? Why is a tenant field directly accessed? What if the tenant entity changes?
        return new Promise(resolve => {
          resolve({tenant});
        });
      } else {
        next();
        return {};
      }
    })
    .then(async ({account, tenant}: {account: Account; tenant: Tenant}) => {
      if (account && account.tokens.length === 1) {
        let permission;

        // by Mayeu: TODO: Same here, what is happening here, why is the number of permission tested?
        if (account.permissions && account.permissions.length === 1) {
          permission = account.permissions[0];
        }

        const token = account.tokens[0];
        const tokenActiveAgo = Date.now() - token.updatedAt.getTime();

        if (
          tokenActiveAgo > TOKEN_VALIDITY_PERIOD ||
          (process.env.NODE_ENV === 'production' &&
            req.method !== 'GET' &&
            req.method !== 'HEAD' &&
            req.header('X-XSRF-TOKEN') !== cryptoHashStringWithSalt(token.id, token.xsrfSalt))
        ) {
          // Token is no longer valid
          // OR
          // The XSRF token doesn't match on mutating request (only in production)

          // console.log('🔑 auth.ts: the request is rejected because the authentication has failed');
          // Delete the token and return
          await tokenRepo.delete(token.id);
        } else {
          // console.log('🔑 auth.ts: the authToken is valid, pimping the request with the needed data');
          // Token is valid, set account info on the request
          req.account = {
            id: account.id,
            email: account.email,
            tenant: permission && permission.tenant,
            writeAccess: permission && permission.writeAccess
          };

          // Check if we need to update the updateAt date.
          // We only do this ever so often to not have a db write on every request
          if (tokenActiveAgo > TOKEN_VALIDITY_PERIOD) {
            // Set new updated at date (will be done by TypeORM automatically
            await tokenRepo.update(token.id, {});
          }
        }
      } else if (shareToken && tenant && tenant.shareTokens.find(t => t.valid && t.id === shareToken.id)) {
        req.account = {
          id: account && account.id,
          email: account && account.email,
          tenant: tenant.id,
          writeAccess: false
        };
      } else if (tenant && tenant.public) {
        // Account has no permission or there is no account
        // And tenant is public
        req.account = {
          id: account && account.id,
          email: account && account.email,
          tenant: tenant.id,
          writeAccess: false
        };
      }
    })
    .then(() => {
      next();
    })
    .catch(error => {
      console.error(error);
      next();
    });
};

const requestXSRFIsInvalid = (req, token) => {
  if (req.header('X-XSRF-TOKEN') !== cryptoHashStringWithSalt(token.id, token.xsrfSalt)) {
    // console.log('🔑 auth.ts: the request do not have a valid XSRF token');
    return true;
  } else {
    // console.log('🔑 auth.ts: the request has a valid XSRF token');
    return false;
  }
};

export const addXsrfCookieToResponse = (req, res, next) => {
  // Add XSRF-TOKEN cookie for XSRF protection.
  // The frontend will read it and add a X-XSRF-TOKEN header to the requests.
  // The backend then checks that those requests have this header and that the token matches for the session.
  // See https://angular.io/guide/http#security-xsrf-protection
  if (req.cookies['authToken']) {
    getRepository(Token)
      .findOne(cryptoHashString(req.cookies['authToken']))
      .then(token => {
        if (!token) {
          console.error('No token');
          next();
          return;
        }

        if (!token.xsrfSalt) {
          console.error('No xsrfSalt in token');
          next();
          return;
        }

        const cookieOptions = {
          maxAge: TOKEN_VALIDITY_PERIOD,
          path: '/',
          domain: config.domain ? '.' + config.domain : undefined,
          secure: process.env.NODE_ENV === 'production'
        };
        const xsrfToken = cryptoHashStringWithSalt(token.id, token.xsrfSalt);
        res.cookie('XSRF-TOKEN', xsrfToken, cookieOptions);
        next();
      });
  } else {
    next();
  }
};

export const ownAccountHandler = (req, res, next) => {
  dumbLog({handler: 'ownAccount', account: req.account || {}});
  if (req.account && req.account.id) {
    getRepository(AccountPermission)
      .createQueryBuilder('accountPermission')
      .select('accountPermission.writeAccess')
      .leftJoinAndSelect('accountPermission.account', 'account')
      .leftJoinAndSelect('accountPermission.tenant', 'tenant')
      .where('account.id = :accountId', {accountId: req.account.id})
      .getMany()
      .then((accountPermissions: AccountPermission[]) => {
        res.json({
          id: req.account.id,
          email: req.account.email,
          permissions: accountPermissions.map(accountPermission => ({
            tenant: {
              slug: accountPermission.tenant.slug,
              name: accountPermission.tenant.name
            },
            writeAccess: accountPermission.writeAccess
          }))
        });
      });
  } else {
    // If no account found => logout
    res.clearCookie('authToken', {path: '/'});
    next();
  }
};

export const generateToken = (size: number): string => {
  return crypto.randomBytes(size).toString('hex');
};

export const hashPassword = async (password: string): Promise<string> => {
  return bcrypt.hash(password, SALT_ROUNDS);
};

export const compareHash = async (password: string, hash: string): Promise<boolean> => {
  return bcrypt.compare(password, hash);
};
