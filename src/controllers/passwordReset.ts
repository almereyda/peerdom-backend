// See http://sahatyalkabov.com/how-to-implement-password-reset-in-nodejs/

import {getRepository, MoreThan} from 'typeorm';

import {Account} from '../entity/account';
import * as account from '../lib/account';
import {cryptoHashString, validatePassword} from '../lib/helpers';
import {hashPassword} from './auth';
import {sendResetPasswordEmail} from '../lib/email';
import {dumbLog} from '../lib/helpers';

/**
 * Express route handler for sending the reset password e-mail
 *
 * TODO: implement a way to prevent spamming someone with reset e-mails
 * @param req
 * @param res
 * @param next
 */
export const requestPasswordReset = async (req, res, next) => {
  dumbLog({handler: 'requestPasswordReset', body: req.body});

  // Read parameters from request
  const emailAddress = req.body.email;

  // Generate a random reset token
  account
    .generatePasswordResetToken(emailAddress)
    .then(resetToken => {
      // Send the e-mail to the owner of the account
      sendResetPasswordEmail(emailAddress, resetToken);
    })
    .then(() => {
      res.json({success: true});
    })
    .catch(err => {
      // console.log(err);
      console.log('controllers:passwordReset.ts: invalid password request for ' + emailAddress);

      next();
    });
};

/**
 * Express route handler for resetting the password with a reset pw token
 * @param req
 * @param res
 * @param next
 */
export const resetPassword = async (req, res, next) => {
  dumbLog({handler: 'resetPassword', body: req.body});

  // Read parameters
  const hashedResetToken = cryptoHashString(req.body.resetToken);
  const hashedPassword = await hashPassword(req.body.password); // TODO: validate password format?

  // Check if we find an account for the given token (and check that the token is still valid)
  const accountRepo = getRepository(Account);
  accountRepo
    .findOne({resetPasswordToken: hashedResetToken, resetPasswordExpires: MoreThan(new Date())})
    .then(acc => {
      if (!acc) {
        res.status(400);
        throw new Error('Invalid token!');
      }

      validatePassword(req.body.password);

      // Update the password and invalidate the reset token
      return accountRepo.update(acc.id, {
        password: hashedPassword,
        resetPasswordToken: null,
        resetPasswordExpires: null,
        activated: true
      });
    })
    .then(() => {
      res.json({success: true});
    })
    .catch(next);
};
