import * as request from 'request-promise-native';

import {config} from '../peerdom-config';
import {dumbLog} from '../lib/helpers';
import * as AzureAD from '../tasks/azure-ad-sync';

export const azureAdSync = async (req, res, next) => {
  const cronjob = config.cronjob;
  // TODO: Test this auth verification thingy
  // Check if a request verification header is required
  if (cronjob.requiredHeader && cronjob.headerValue) {
    // If the don't exist, or doesn't match the expected value, return a 401
    if (
      req.header(cronjob.requiredHeader) === undefined ||
      req.header(cronjob.requiredHeader) !== cronjob.headerValue
    ) {
      const error = {error: 'Unauthorized. Required header is missing or it has the wrong value.'};
      dumbLog(error);
      res.json(error);
      res.status(401).end();
      return;
    }
  }

  try {
    await request.get(process.env['PRD_CRONJOB_HEALTHCHECKSIO_PING_URL'] + '/start');
    await AzureAD.sync();
  } catch (error) {
    console.error(error);
    await request.get(process.env['PRD_CRONJOB_HEALTHCHECKSIO_PING_URL'] + '/fail');
    res.status(500).end();
    return;
  }
  await request.get(process.env['PRD_CRONJOB_HEALTHCHECKSIO_PING_URL']);
  res.status(200).end();
};
