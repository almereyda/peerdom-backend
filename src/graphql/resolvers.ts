import * as GraphQLJSON from 'graphql-type-json';
import {GraphQLUpload} from 'graphql-upload';

import {createPeerMutation, deletePeerMutation, updatePeerMutation} from './mutations/peers';
import {createNodeMutation, deleteNodeMutation, updateNodeMutation, copyNodeMutation} from './mutations/nodes';
import {createHoldersMutation, deleteHoldersMutation, updateHoldersMutation} from './mutations/holders';
import {createProjectMutation, deleteProjectMutation, updateProjectMutation} from './mutations/projects';
import {createAccountMutation, deleteAccountMutation, updateAccountMutation} from './mutations/accounts';
import {
  createAccountPermissionMutation,
  deleteAccountPermissionMutation,
  updateAccountPermissionMutation
} from './mutations/account_permissions';
import {peersResolver} from './resolvers/peers';
import {tenantResolver} from './resolvers/tenant';
import {accountsResolver} from './resolvers/accounts';
import {nodesResolver} from './resolvers/nodes';
import {holdersResolver} from './resolvers/holders';
import {projectsResolver} from './resolvers/projects';
import {eventsLogResolver} from './resolvers/events_log';

export const resolvers = {
  JSON: GraphQLJSON,
  FileUpload: GraphQLUpload,

  Query: {
    ...tenantResolver,
    ...accountsResolver,
    ...peersResolver,
    ...nodesResolver,
    ...holdersResolver,
    ...projectsResolver,
    ...eventsLogResolver
  },

  Mutation: {
    ...createAccountMutation,
    ...deleteAccountMutation,
    ...updateAccountMutation,
    ...createAccountPermissionMutation,
    ...deleteAccountPermissionMutation,
    ...updateAccountPermissionMutation,
    ...createPeerMutation,
    ...deletePeerMutation,
    ...updatePeerMutation,
    ...createNodeMutation,
    ...deleteNodeMutation,
    ...updateNodeMutation,
    ...copyNodeMutation,
    ...createHoldersMutation,
    ...deleteHoldersMutation,
    ...updateHoldersMutation,
    ...createProjectMutation,
    ...deleteProjectMutation,
    ...updateProjectMutation
  }
};
