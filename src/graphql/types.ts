import {
  Account,
  CreateAccountInput,
  CreateAccountPayload,
  DeleteAccountInput,
  DeleteAccountPayload,
  UpdateAccountInput,
  UpdateAccountPayload,
  AccountNew,
  AccountPatch
} from './types/account';
import {
  AccountPermission,
  CreateAccountPermissionInput,
  CreateAccountPermissionPayload,
  DeleteAccountPermissionInput,
  DeleteAccountPermissionPayload,
  UpdateAccountPermissionInput,
  UpdateAccountPermissionPayload,
  AccountPermissionNew,
  AccountPermissionPatch
} from './types/account_permission';
import {
  Peer,
  CreatePeerInput,
  CreatePeerPayload,
  DeletePeerInput,
  DeletePeerPayload,
  UpdatePeerInput,
  UpdatePeerPayload,
  PeerNew,
  PeerPatch,
  MissionNew,
  MissionPatch
} from './types/peer';
import {Mission} from './types/mission';
import {
  Holder,
  CreateHoldersInput,
  CreateHoldersPayload,
  DeleteHoldersInput,
  DeleteHoldersPayload,
  UpdateHoldersInput,
  UpdateHoldersPayload,
  HolderNew,
  HolderPatch
} from './types/holder';
import {
  NodeType,
  Node,
  CreateNodeInput,
  CreateNodePayload,
  DeleteNodeInput,
  DeleteNodePayload,
  UpdateNodeInput,
  UpdateNodePayload,
  CopyNodeInput,
  CopyNodePayload,
  NodeNew,
  NodePatch,
  RepresentativePatch
} from './types/node';
import {NodeTemplate} from './types/node_template';
import {CustomFieldDefinition, CustomField, CustomFieldInput} from './types/custom_field';
import {
  Project,
  CreateProjectInput,
  CreateProjectPayload,
  DeleteProjectInput,
  DeleteProjectPayload,
  UpdateProjectInput,
  UpdateProjectPayload,
  ProjectNew,
  ProjectPatch
} from './types/project';
import {Tenant} from './types/tenant';

import {scalars} from './scalars';
import {EventsLog} from './types/events_log';

export const types = [
  scalars,
  Account,
  CreateAccountInput,
  CreateAccountPayload,
  DeleteAccountInput,
  DeleteAccountPayload,
  UpdateAccountInput,
  UpdateAccountPayload,
  AccountNew,
  AccountPatch,
  AccountPermission,
  CreateAccountPermissionInput,
  CreateAccountPermissionPayload,
  DeleteAccountPermissionInput,
  DeleteAccountPermissionPayload,
  UpdateAccountPermissionInput,
  UpdateAccountPermissionPayload,
  AccountPermissionNew,
  AccountPermissionPatch,
  Holder,
  Mission,
  NodeType,
  Node,
  CreateNodeInput,
  CreateNodePayload,
  DeleteNodeInput,
  DeleteNodePayload,
  UpdateNodeInput,
  UpdateNodePayload,
  CopyNodeInput,
  CopyNodePayload,
  NodeNew,
  NodePatch,
  RepresentativePatch,
  NodeTemplate,
  CustomFieldDefinition,
  CustomField,
  CustomFieldInput,
  Peer,
  Tenant,
  CreatePeerInput,
  CreatePeerPayload,
  DeletePeerInput,
  DeletePeerPayload,
  UpdatePeerInput,
  UpdatePeerPayload,
  PeerNew,
  PeerPatch,
  MissionNew,
  MissionPatch,
  CreateHoldersInput,
  CreateHoldersPayload,
  DeleteHoldersInput,
  DeleteHoldersPayload,
  UpdateHoldersInput,
  UpdateHoldersPayload,
  HolderNew,
  HolderPatch,
  Project,
  CreateProjectInput,
  CreateProjectPayload,
  DeleteProjectInput,
  DeleteProjectPayload,
  UpdateProjectInput,
  UpdateProjectPayload,
  ProjectNew,
  ProjectPatch,
  EventsLog
];
