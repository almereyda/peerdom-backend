import {getRepository} from 'typeorm';
import {Peer} from '../../entity/peer';
import {dumbLog} from '../../lib/helpers';

export const peersResolver = {
  async peers(root, args, context, info) {
    dumbLog({resolver: 'peers', args, accountId: context.account.id, tenantId: context.account.tenant});
    const repository = getRepository(Peer);

    const requestedFields = info.fieldNodes[0].selectionSet.selections.map(sel => sel.name.value);
    const resolveHolders = requestedFields.indexOf('holders') !== -1;
    const resolveAccount = requestedFields.indexOf('account') !== -1;

    const findOptions: any = {
      where: {
        tenant: context.account.tenant
      },
      relations: ['mission'],
      order: {
        displayName: 'ASC'
      }
    };

    if (args.id) {
      findOptions.where.id = args.id;
    }
    if (args.slug) {
      findOptions.where.slug = args.slug;
    }

    if (resolveHolders) {
      findOptions.relations = findOptions.relations.concat([
        'holders',
        'holders.role',
        'holders.role.parent',
        'holders.role.parent.representatives',
        'holders.role.customFields',
        'holders.role.customFields.definition'
      ]);
    }

    if (resolveAccount) {
      findOptions.relations = findOptions.relations.concat(['account']);
    }

    return await repository.find(findOptions);
  }
};
