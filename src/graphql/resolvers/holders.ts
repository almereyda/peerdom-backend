import {FindConditions, getRepository} from 'typeorm';
import {Holder} from '../../entity/holder';
import {dumbLog} from '../../lib/helpers';

export const holdersResolver = {
  async holders(root, args, context) {
    dumbLog({resolver: 'holders', args, accountId: context.account.id, tenantId: context.account.tenant});
    const repository = getRepository(Holder);
    const where: FindConditions<Holder> = {};

    if (!args.roleId) {
      throw new Error('Trying to retrieve holders without specifying the role');
    } else {
      where.role = {
        tenant: context.account.tenant,
        id: args.roleId
      };
    }

    return await repository.find({
      where,
      relations: ['peer']
    });
  }
};
