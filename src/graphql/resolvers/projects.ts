import * as project from '../../lib/project';
import {dumbLog} from '../../lib/helpers';

export const projectsResolver = {
  async projects(root, args, context, info) {
    dumbLog({resolver: 'projects', args, accountId: context.account.id, tenantId: context.account.tenant});

    const requestedFields = info.fieldNodes[0].selectionSet.selections.map(sel => sel.name.value);
    const resolveParent = requestedFields.indexOf('parent') !== -1;
    const resolveChildren =
      requestedFields.indexOf('children') !== -1 || requestedFields.indexOf('ProjectsRecursive') !== -1;
    const resolveHolders = requestedFields.indexOf('holders') !== -1;
    const rootOnly = requestedFields.indexOf('ProjectsRecursive') !== -1;

    return await project.get(context.account.tenant, {
      resolveChildren,
      resolveParent,
      resolveHolders,
      rootOnly,
      projectId: args.id,
      slug: args.slug,
      archived: args.archived
    });
  }
};
