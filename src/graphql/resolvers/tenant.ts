import {getRepository} from 'typeorm';
import {Tenant} from '../../entity/tenant';
import {dumbLog} from '../../lib/helpers';

export const tenantResolver = {
  async tenant(root, args, context) {
    dumbLog({resolver: 'tenant', args, accountId: context.account.id, tenantId: context.account.tenant});
    const repository = getRepository(Tenant);
    const tenant = await repository.findOne({
      where: {
        id: context.account.tenant
      },
      relations: ['nodes', 'nodes.parent', 'customFieldDefinitions']
    });

    // Retrieving root node ID and adding it manually to the `mapSettings`
    const nodesWithoutParent = tenant.nodes.filter(
      node => node.type === 'circle' && node.parent === null && !node.external
    );

    // TODO: handle errors (return 500 and handle in frontend as well)
    if (nodesWithoutParent.length < 1) {
      throw new Error('No root node found');
    } else if (nodesWithoutParent.length > 1) {
      throw new Error('More than 1 root node found');
    }

    tenant.mapSettings.rootCircleId = nodesWithoutParent[0].id;

    // sort custom field definitions by order and updatedAt asc
    tenant.customFieldDefinitions.sort((cfd1, cfd2) => {
      // fields with unspecified order should appear last
      let cfd1Order = cfd1.order;
      let cfd2Order = cfd2.order;
      if (cfd1Order === null || cfd1Order === undefined) {
        cfd1Order = Number.POSITIVE_INFINITY;
      }
      if (cfd2Order === null || cfd2Order === undefined) {
        cfd2Order = Number.POSITIVE_INFINITY;
      }

      if (cfd1Order < cfd2Order) {
        return -1;
      } else if (cfd1Order > cfd2Order) {
        return 1;
      }

      if (cfd1.updatedAt < cfd2.updatedAt) {
        return -1;
      } else if (cfd1.updatedAt > cfd2.updatedAt) {
        return 1;
      } else {
        return 0;
      }
    });

    return tenant;
  }
};
