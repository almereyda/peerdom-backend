import {getRepository} from 'typeorm';
import {EventsLog} from '../../entity/events_log';
import {dumbLog} from '../../lib/helpers';

export const eventsLogResolver = {
  async events(root, args, context) {
    dumbLog({resolver: 'eventsLog', args, accountId: context.account.id, tenantId: context.account.tenant});

    const repository = getRepository(EventsLog);
    const findOptions: any = {
      order: {
        createdAt: 'ASC'
      },
      relations: ['tenant'],
      where: {}
    };

    if (args.tenantId) {
      findOptions.where.tenant = args.tenantId;
    }

    return await repository.find(findOptions);
  }
};
