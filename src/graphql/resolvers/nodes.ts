import * as map from '../../lib/node';
import {dumbLog} from '../../lib/helpers';

export const nodesResolver = {
  async nodes(root, args, context, info) {
    dumbLog({resolver: 'nodes', args, accountId: context.account.id, tenantId: context.account.tenant});

    // Extract the data we need for the request
    const requestedFields = info.fieldNodes[0].selectionSet.selections.map(sel => sel.name.value);
    const tenant = context.account.tenant;
    const resolveChildren = requestedFields.indexOf('children') !== -1;
    const resolveDirectPeers = requestedFields.indexOf('directPeers') !== -1;
    const resolveParent = requestedFields.indexOf('parent') !== -1;
    const resolveTemplate = requestedFields.indexOf('template') !== -1;
    const resolveRepresentatives = requestedFields.indexOf('representatives') !== -1;
    const nodeIds = args.ids;
    const nodeType = args.type;

    // We get all the matching nodes
    const nodes = await map.getNodes(tenant, {
      resolveChildren: resolveChildren,
      resolveDirectPeers: resolveDirectPeers,
      resolveParent: resolveParent,
      resolveTemplate: resolveTemplate,
      resolveRepresentatives: resolveRepresentatives,
      nodeIds: nodeIds,
      nodeType: nodeType
    });

    // Finally we format the nodes
    return nodes.map(node => {
      const formattedNode: any = node; // TODO: replace `any` type with Node as soon as we have TS types
      if (node.type === 'circle') {
        if (resolveDirectPeers) {
          formattedNode.directPeers = formatDirectPeers(node);
        }
      } else {
        // TODO: Is it really needed to do the following?
        formattedNode.holders = node.holders;
      }

      return formattedNode;
    });
  }
};

/* Format Direct Peers
 *
 * format the direct peers of a node
 */
const formatDirectPeers = function (node) {
  if (!node.children) {
    return [];
  }

  const directPeers = node.children.reduce((acc, child) => {
    if (child.type === 'role') {
      return acc.concat(child.holders.map(holder => holder.peer));
    }
    return acc;
  }, []);

  // Remove duplicates
  return directPeers.filter((peer, index, self) => index === self.findIndex(otherPeer => peer.id === otherPeer.id));
};
