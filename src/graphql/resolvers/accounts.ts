import {getRepository, FindManyOptions} from 'typeorm';
import {Account} from '../../entity/account';

export const accountsResolver = {
  async accounts(root, args, context, info) {
    const repository = getRepository(Account);

    const requestedFields = info.fieldNodes[0].selectionSet.selections.map(sel => sel.name.value);
    const resolvePermissions = requestedFields.indexOf('permissions') !== -1;

    const findOptions: FindManyOptions<Account> = {};

    if (args.id) {
      findOptions.where = {
        id: args.id
      };
    }

    if (resolvePermissions) {
      findOptions.relations = ['permissions'];
    }

    const accounts = await repository.find(findOptions);

    return accounts;
  }
};
