import {gql} from 'apollo-server-express';

export const Holder = gql`
  type Holder {
    id: ID!
    focus: String
    electedUntil: GraphQLDate
    peer: Peer
    role: Node
    circle: Node
    createdAt: GraphQLDateTime!
    updatedAt: GraphQLDateTime!
  }
`;

export const HolderNew = gql`
  input HolderNew {
    peer: ID!
    role: ID!
    circle: ID
    focus: String
    electedUntil: GraphQLDate
  }
`;

export const HolderPatch = gql`
  input HolderPatch {
    id: ID!
    peer: ID
    role: ID
    circle: ID
    focus: String
    electedUntil: GraphQLDate
  }
`;

export const UpdateHoldersInput = gql`
  input UpdateHoldersInput {
    holders: [HolderPatch]
  }
`;

export const CreateHoldersInput = gql`
  input CreateHoldersInput {
    newHolders: [HolderNew]
  }
`;

export const DeleteHoldersInput = gql`
  input DeleteHoldersInput {
    ids: [ID]!
  }
`;

export const CreateHoldersPayload = gql`
  type CreateHoldersPayload {
    holders: [Holder]
  }
`;

export const UpdateHoldersPayload = gql`
  type UpdateHoldersPayload {
    holders: [Holder]
  }
`;

export const DeleteHoldersPayload = gql`
  type DeleteHoldersPayload {
    holders: [Holder]
  }
`;
