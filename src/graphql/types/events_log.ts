import {gql} from 'apollo-server-express';

// TODO: add author of type Account
export const EventsLog = gql`
  type EventsLog {
    id: ID!
    entity: String!
    tenant: Tenant
    createdAt: GraphQLDateTime
    item: ID!
    action: String!
    change: JSON
    version: String
  }
`;
