import {gql} from 'apollo-server-express';

export const AccountPermission = gql`
  type AccountPermission {
    tenant: Tenant!
    account: Account!
    writeAccess: Boolean!
  }
`;

export const AccountPermissionNew = gql`
  input AccountPermissionNew {
    tenant: ID!
    account: ID!
    writeAccess: Boolean!
  }
`;

export const AccountPermissionPatch = gql`
  input AccountPermissionPatch {
    writeAccess: Boolean!
  }
`;

export const UpdateAccountPermissionInput = gql`
  input UpdateAccountPermissionInput {
    tenant: ID!
    account: ID!
    patch: AccountPermissionPatch
  }
`;

export const CreateAccountPermissionInput = gql`
  input CreateAccountPermissionInput {
    newAccountPermission: AccountPermissionNew
  }
`;

export const DeleteAccountPermissionInput = gql`
  input DeleteAccountPermissionInput {
    tenant: ID!
    account: ID!
  }
`;

export const CreateAccountPermissionPayload = gql`
  type CreateAccountPermissionPayload {
    accountPermission: AccountPermission
  }
`;

export const UpdateAccountPermissionPayload = gql`
  type UpdateAccountPermissionPayload {
    accountPermission: AccountPermission
  }
`;

export const DeleteAccountPermissionPayload = gql`
  type DeleteAccountPermissionPayload {
    accountPermission: AccountPermission
  }
`;
