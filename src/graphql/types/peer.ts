import {gql} from 'apollo-server-express';

export const Peer = gql`
  type Peer {
    id: ID!
    displayName: String!
    slug: String!
    avatarUrl: String
    email: String
    account: Account
    mission: Mission
    holders: [Holder]
    mapStructure: Node
    createdAt: GraphQLDateTime!
    updatedAt: GraphQLDateTime!
  }
`;

export const MissionNew = gql`
  input MissionNew {
    text: String
  }
`;

export const PeerNew = gql`
  input PeerNew {
    avatar: FileUpload
    displayName: String!
    email: String
    writeAccess: Boolean
    mission: MissionNew
  }
`;

export const MissionPatch = gql`
  input MissionPatch {
    text: String
  }
`;

export const PeerPatch = gql`
  input PeerPatch {
    avatar: FileUpload
    displayName: String
    email: String
    writeAccess: Boolean
    mission: MissionPatch
  }
`;

export const UpdatePeerInput = gql`
  input UpdatePeerInput {
    id: ID!
    patch: PeerPatch
  }
`;

export const CreatePeerInput = gql`
  input CreatePeerInput {
    newPeer: PeerNew
  }
`;

export const DeletePeerInput = gql`
  input DeletePeerInput {
    id: ID!
  }
`;

export const CreatePeerPayload = gql`
  type CreatePeerPayload {
    peer: Peer
  }
`;

export const UpdatePeerPayload = gql`
  type UpdatePeerPayload {
    peer: Peer
  }
`;

export const DeletePeerPayload = gql`
  type DeletePeerPayload {
    peer: Peer
  }
`;
