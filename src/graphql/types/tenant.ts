import {gql} from 'apollo-server-express';

export const Tenant = gql`
  type MapSettings {
    rootCircleId: String
    terms: Terms
  }

  type Terms {
    peer: String
    circle: String
    role: String
    representative: String
  }

  type MissionSettings {
    enabled: Boolean
  }

  type ProjectsSettings {
    enabled: Boolean
  }

  type Tenant {
    id: ID!
    name: String
    slug: String
    mapSettings: MapSettings
    missionSettings: MissionSettings
    projectsSettings: ProjectsSettings
    customFieldDefinitions: [CustomFieldDefinition]
    public: Boolean
    createdAt: GraphQLDateTime
    updatedAt: GraphQLDateTime
  }
`;
