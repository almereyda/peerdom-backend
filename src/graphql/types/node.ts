import {gql} from 'apollo-server-express';

export const NodeType = gql`
  enum NodeType {
    circle
    role
  }
`;

export const Node = gql`
  type Node {
    id: ID!
    parent: Node
    type: NodeType!
    name: String!
    slug: String!
    electable: Boolean
    external: Boolean
    color: String
    representatives: [Node]
    customFields: [CustomField]
    createdAt: GraphQLDateTime
    updatedAt: GraphQLDateTime
    children: [Node]
    peers: [Peer]
    directPeers: [Peer]
    holders: [Holder]
    template: NodeTemplate
  }
`;

export const NodeNew = gql`
  input NodeNew {
    parent: ID
    type: NodeType!
    name: String!
    electable: Boolean
    slug: String
    external: Boolean
    color: String
    customFields: [CustomFieldInput]
  }
`;

export const NodePatch = gql`
  input NodePatch {
    parent: ID
    type: NodeType
    name: String
    slug: String
    electable: Boolean
    external: Boolean
    color: String
    customFields: [CustomFieldInput]
    template: ID
    representatives: [RepresentativePatch]
    mirroredRepresentativesToRemove: [ID]
  }
`;

export const RepresentativePatch = gql`
  input RepresentativePatch {
    id: ID!
    template: ID
    mirrored: Boolean
  }
`;

export const UpdateNodeInput = gql`
  input UpdateNodeInput {
    id: ID!
    patch: NodePatch
  }
`;

export const CreateNodeInput = gql`
  input CreateNodeInput {
    newNode: NodeNew
  }
`;

export const DeleteNodeInput = gql`
  input DeleteNodeInput {
    id: ID!
  }
`;

export const CopyNodeInput = gql`
  input CopyNodeInput {
    id: ID!
    parent: ID
    electable: Boolean
    external: Boolean
    withTemplate: Boolean
  }
`;

export const CreateNodePayload = gql`
  type CreateNodePayload {
    node: Node
  }
`;

export const UpdateNodePayload = gql`
  type UpdateNodePayload {
    node: Node
  }
`;

export const DeleteNodePayload = gql`
  type DeleteNodePayload {
    node: Node
  }
`;

export const CopyNodePayload = gql`
  type CopyNodePayload {
    node: Node
  }
`;
