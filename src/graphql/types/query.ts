import {gql} from 'apollo-server-express';

export const Query = gql`
  type Query {
    tenant: Tenant
    accounts(id: ID): [Account]
    accountPermissions(accountId: ID, tenantId: ID): [AccountPermission]
    peers(id: ID, slug: String): [Peer]
    nodes(ids: [ID], type: String): [Node]
    holders(roleId: ID, circleId: ID): [Holder]
    projects(id: ID, slug: String, archived: Boolean): [Project]
    events(tenantId: ID): [EventsLog]
  }
`;
