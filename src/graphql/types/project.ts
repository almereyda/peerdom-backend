import {gql} from 'apollo-server-express';

export const Project = gql`
  type Project {
    id: ID!
    name: String!
    slug: String!
    description: String
    notes: String
    externalUrl: String
    archived: Boolean
    parent: Project
    children: [Project]
    holders: [Holder]
    createdAt: GraphQLDateTime!
    updatedAt: GraphQLDateTime!
  }
`;


export const ProjectNew = gql`
  input ProjectNew {
    name: String!
    description: String
    notes: String
    externalUrl: String
    archived: Boolean
    parent: ID
    holders: [ID]
  }
`;


export const ProjectPatch = gql`
  input ProjectPatch {
    name: String
    description: String
    notes: String
    externalUrl: String
    archived: Boolean
    parent: ID
    holders: [ID]
  }
`;

export const UpdateProjectInput = gql`
  input UpdateProjectInput {
    id: ID!
    patch: ProjectPatch
  }
`;

export const CreateProjectInput = gql`
  input CreateProjectInput {
    newProject: ProjectNew
  }
`;

export const DeleteProjectInput = gql`
  input DeleteProjectInput {
    id: ID!
  }
`;

export const CreateProjectPayload = gql`
  type CreateProjectPayload {
    project: Project
  }
`;

export const UpdateProjectPayload = gql`
  type UpdateProjectPayload {
    project: Project
  }
`;

export const DeleteProjectPayload = gql`
  type DeleteProjectPayload {
    project: Project
  }
`;
