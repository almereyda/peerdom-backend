import {gql} from 'apollo-server-express';

// For now we just provide the template id if it is requested through GraphQL.
// We do this just to know if a node is based on a template. All the information is copied in the node.
export const NodeTemplate = gql`
  type NodeTemplate {
    id: ID!
  }
`;
