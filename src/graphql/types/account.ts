import {gql} from 'apollo-server-express';

export const Account = gql`
  type Account {
    id: ID!
    email: String!
    permissions: [AccountPermission]
    peers: [Peer]
    createdAt: GraphQLDateTime!
    updatedAt: GraphQLDateTime!
  }
`;

export const AccountNew = gql`
  input AccountNew {
    email: String!
  }
`;

export const AccountPatch = gql`
  input AccountPatch {
    email: String!
  }
`;

export const UpdateAccountInput = gql`
  input UpdateAccountInput {
    id: ID!
    patch: AccountPatch
  }
`;

export const CreateAccountInput = gql`
  input CreateAccountInput {
    newAccount: AccountNew
  }
`;

export const DeleteAccountInput = gql`
  input DeleteAccountInput {
    id: ID!
  }
`;

export const CreateAccountPayload = gql`
  type CreateAccountPayload {
    account: Account
  }
`;

export const UpdateAccountPayload = gql`
  type UpdateAccountPayload {
    account: Account
  }
`;

export const DeleteAccountPayload = gql`
  type DeleteAccountPayload {
    account: Account
  }
`;
