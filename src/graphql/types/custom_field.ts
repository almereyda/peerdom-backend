import {gql} from 'apollo-server-express';

export const CustomFieldDefinition = gql`
  type Translation {
    en: String
    de: String
    fr: String
    nl: String
    sv: String
  }

  enum CustomFieldType {
    text
    textarea
    email
    link
    date
  }

  type CustomFieldDefinition {
    id: ID!
    name: String
    translation: Translation
    type: CustomFieldType
    multiEntry: Boolean
  }
`;

export const CustomField = gql`
  type CustomField {
    id: ID!
    value: JSON
    definition: CustomFieldDefinition
  }
`;

export const CustomFieldInput = gql`
  input CustomFieldInput {
    id: ID
    value: JSON
    definitionId: ID!
  }
`;
