import {gql} from 'apollo-server-express';

export const Mutation = gql`
  type Mutation {
    createAccount(input: CreateAccountInput!): CreateAccountPayload
    deleteAccount(input: DeleteAccountInput!): DeleteAccountPayload
    updateAccount(input: UpdateAccountInput!): UpdateAccountPayload
    createAccountPermission(input: CreateAccountPermissionInput!): CreateAccountPermissionPayload
    deleteAccountPermission(input: DeleteAccountPermissionInput!): DeleteAccountPermissionPayload
    updateAccountPermission(input: UpdateAccountPermissionInput!): UpdateAccountPermissionPayload
    createPeer(input: CreatePeerInput!): CreatePeerPayload
    deletePeer(input: DeletePeerInput!): DeletePeerPayload
    updatePeer(input: UpdatePeerInput!): UpdatePeerPayload
    createNode(input: CreateNodeInput!): CreateNodePayload
    deleteNode(input: DeleteNodeInput!): DeleteNodePayload
    updateNode(input: UpdateNodeInput!): UpdateNodePayload
    copyNode(input: CopyNodeInput!): CopyNodePayload
    createHolders(input: CreateHoldersInput!): CreateHoldersPayload
    deleteHolders(input: DeleteHoldersInput!): DeleteHoldersPayload
    updateHolders(input: UpdateHoldersInput!): UpdateHoldersPayload
    createProject(input: CreateProjectInput!): CreateProjectPayload
    deleteProject(input: DeleteProjectInput!): DeleteProjectPayload
    updateProject(input: UpdateProjectInput!): UpdateProjectPayload
  }
`;
