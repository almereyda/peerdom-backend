import {getRepository} from 'typeorm';
import {Holder} from '../../entity/holder';
import {checkWriteAccess, dumbLog} from '../../lib/helpers';

export const createHoldersMutation = {
  async createHolders(_, {input: attrs}, context) {
    dumbLog({mutation: 'createHolder', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const repository = getRepository(Holder);
    const holders = await repository.save(attrs.newHolders, {
      data: {accountId: context.account.id, tenantId: context.account.tenant}
    });
    return {holders};
  }
};

export const updateHoldersMutation = {
  async updateHolders(_, {input: attrs}, context) {
    dumbLog({mutation: 'updateHolders', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const repository = getRepository(Holder);
    // We need to create a new instance of the entity to have access to the hooks in the entity listeners subscribers
    // See: https://github.com/typeorm/typeorm/issues/674#issuecomment-346296494
    const entities = attrs.holders.map(h => Object.assign(new Holder(), h));
    // We need to do a save instead of an update because update is only partial and is not listen to by subscribers
    // See: https://github.com/typeorm/typeorm/issues/2809#issuecomment-451914877
    const holders = await repository.save(entities, {
      data: {accountId: context.account.id, tenantId: context.account.tenant}
    });
    return {holders};
  }
};

export const deleteHoldersMutation = {
  async deleteHolders(_, {input: attrs}, context) {
    dumbLog({mutation: 'deleteHolders', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const repository = getRepository(Holder);
    const holders = await repository.findByIds(attrs.ids, {relations: ['peer', 'role']});
    const removedHolders = holders.map(h => Object.assign(new Holder(), h));
    await repository.remove(holders, {data: {accountId: context.account.id, tenantId: context.account.tenant}});
    return {holders: removedHolders};
  }
};
