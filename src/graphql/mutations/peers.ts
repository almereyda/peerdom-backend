import {getRepository} from 'typeorm';

import * as peer from '../../lib/peer';
import * as account from '../../lib/account';
import * as accountPermission from '../../lib/account_permission';
import {sendResetPasswordEmail} from '../../lib/email';
import {
  checkWriteAccess,
  dumbLog,
  ImageMetadata,
  getImageMetadata,
  saveAvatarImage,
  deleteAvatarImage
} from '../../lib/helpers';
import {Mission} from '../../entity/mission';
import {Holder} from '../../entity/holder';

export const createPeerMutation = {
  async createPeer(_, {input: attrs}, context) {
    dumbLog({mutation: 'createPeer', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);

    if (attrs.newPeer.email) {
      let acc = await account.findByEmail(attrs.newPeer.email);
      if (!acc) {
        acc = await account.create(attrs.newPeer.email);
        const resetToken = await account.generatePasswordResetToken(acc.email);
        sendResetPasswordEmail(acc.email, resetToken);
      }
      await accountPermission.create(context.account.tenant, acc.id, attrs.newPeer.writeAccess);
      attrs.newPeer.account = acc;
    }

    const avatarFile = await attrs.newPeer.avatar;
    let avatarUrl;
    if (avatarFile) {
      const metadata: ImageMetadata = await getImageMetadata(avatarFile);
      avatarUrl = await saveAvatarImage(context.account.tenant, avatarFile, metadata);
      attrs.newPeer.avatarUrl = avatarUrl;
    }

    const newPeer = await peer.create(attrs.newPeer, context.account.id, context.account.tenant);

    if (attrs.newPeer.mission && attrs.newPeer.mission.text) {
      const missionRepository = getRepository(Mission);
      const mission = {
        text: attrs.newPeer.mission.text,
        peer: newPeer
      };
      await missionRepository.save(mission, {data: {accountId: context.account.id, tenantId: context.account.tenant}});
    }
    return {peer: newPeer};
  }
};

export const updatePeerMutation = {
  async updatePeer(_, {input: attrs}, context) {
    dumbLog({mutation: 'updatePeer', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);

    let missionPatch;
    if (attrs.patch.mission) {
      missionPatch = JSON.parse(JSON.stringify(attrs.patch.mission));
      delete attrs.patch.mission;
    }

    // if the patch does not contain an avatar property, neither storage nor deletion will occur
    if (attrs.patch.hasOwnProperty('avatar')) {
      const avatarFile = await attrs.patch.avatar;
      const oldPeer = await peer.findById(attrs.id);

      if (avatarFile) {
        const metadata: ImageMetadata = await getImageMetadata(avatarFile);
        attrs.patch.avatarUrl = await saveAvatarImage(context.account.tenant, avatarFile, metadata);
        // do not delete the avatar in the unlikely event of a matching URL
        if (oldPeer.avatarUrl !== attrs.patch.avatarUrl) {
          deleteAvatarImage(oldPeer.avatarUrl);
        }
      } else {
        // attrs.patch.avatar is null, remove avatarUrl from DB and delete the current avatar
        attrs.patch.avatarUrl = null;
        deleteAvatarImage(oldPeer.avatarUrl);
      }
    }

    const updatedPeer = await peer.update(attrs.id, attrs.patch, context.account.id, context.account.tenant);

    // Update mission of peer or creates it if it didn't already have one
    if (missionPatch) {
      const missionRepository = getRepository(Mission);
      const mission = await missionRepository.findOne({
        relations: ['peer'],
        where: {
          peer: {id: attrs.id}
        }
      });
      if (mission) {
        if (missionPatch.text) {
          const missionEntity = Object.assign(new Mission(), {id: mission.id, ...missionPatch});
          await missionRepository.save(missionEntity, {
            data: {accountId: context.account.id, tenantId: context.account.tenant}
          });
        } else {
          // Mission has empty text
          await missionRepository.remove(mission, {
            data: {accountId: context.account.id, tenantId: context.account.tenant}
          });
        }
      } else {
        await missionRepository.save(
          {
            text: missionPatch.text,
            peer: updatedPeer
          },
          {data: {accountId: context.account.id, tenantId: context.account.tenant}}
        );
      }
    }

    return {peer: updatedPeer};
  }
};

export const deletePeerMutation = {
  async deletePeer(_, {input: attrs}, context) {
    dumbLog({mutation: 'deletePeer', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);

    let cascadeItems = [];
    const holders = await getRepository(Holder).find({
      relations: ['peer', 'role'],
      where: {
        peer: {id: attrs.id}
      }
    });
    cascadeItems = holders.map(holder => ({
      item: {...holder, peer: {id: holder.peer.id}, role: {id: holder.role.id}},
      entity: 'holder'
    }));
    const mission = await getRepository(Mission).findOne({
      relations: ['peer'],
      where: {
        peer: {id: attrs.id}
      }
    });
    if (mission) {
      cascadeItems.push({
        item: {...mission, peer: {id: mission.peer.id}},
        entity: 'mission'
      });
    }

    const removedPeer = await peer.remove(attrs.id, cascadeItems, context.account.id, context.account.tenant);
    deleteAvatarImage(removedPeer.avatarUrl);
    return {peer: removedPeer};
  }
};
