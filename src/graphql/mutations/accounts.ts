import * as Account from '../../lib/account';
import {checkWriteAccess, dumbLog} from '../../lib/helpers';

export const createAccountMutation = {
  async createAccount(_, {input: attrs}, context) {
    dumbLog({mutation: 'createAccount', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const account = await Account.create(attrs.newAccount.email);
    return {account: account};
  }
};

export const updateAccountMutation = {
  async updateAccount(_, {input: attrs}, context) {
    dumbLog({mutation: 'updateAccount', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const account = await Account.update(attrs.id, attrs.patch);
    return {account: account};
  }
};

export const deleteAccountMutation = {
  async deleteAccount(_, {input: attrs}, context) {
    dumbLog({mutation: 'deleteAccount', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const account = await Account.findById(attrs.id);
    const removedAccount = JSON.parse(JSON.stringify(account));
    await Account.remove(account);
    return {account: removedAccount};
  }
};
