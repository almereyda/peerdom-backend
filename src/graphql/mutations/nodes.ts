import * as node from '../../lib/node';
import {checkWriteAccess, dumbLog} from '../../lib/helpers';

export const createNodeMutation = {
  async createNode(_, {input: attrs}, context) {
    dumbLog({mutation: 'createNode', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);

    // Build the required fields for the Node
    let newNode = {
      tenant: context.account.tenant,
      ...attrs.newNode
    };

    newNode = await node.create(newNode, context.account.id, context.account.tenant);

    return {node: newNode};
  }
};

export const updateNodeMutation = {
  async updateNode(_, {input: attrs}, context) {
    dumbLog({mutation: 'updateNode', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);

    // need to cast to Object because graphql objects returned from apollo server are not recognized
    // await repository.update(attrs.id, {...attrs.patch});
    const updatedNode = await node.update(context, attrs.id, {...attrs.patch});

    return {node: updatedNode};
  }
};

export const copyNodeMutation = {
  async copyNode(_, {input: attrs}, context) {
    dumbLog({mutation: 'copyNode', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);

    const copiedNode = await node.copy(context, attrs.id, attrs.parent, attrs.external, attrs.withTemplate);
    return {node: copiedNode};
  }
};

export const deleteNodeMutation = {
  async deleteNode(_, {input: attrs}, context) {
    dumbLog({mutation: 'deleteNode', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);

    const removedNode = await node.remove(context, attrs.id);

    return {node: removedNode};
  }
};
