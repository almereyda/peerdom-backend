import * as Project from '../../lib/project';
import {checkWriteAccess, dumbLog} from '../../lib/helpers';

export const createProjectMutation = {
  async createProject(_, {input: attrs}, context) {
    dumbLog({mutation: 'createProject', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const project = await Project.create(attrs.newProject, context.account.id, context.account.tenant);
    return {project};
  }
};

export const updateProjectMutation = {
  async updateProject(_, {input: attrs}, context) {
    dumbLog({mutation: 'updateProject', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const project = await Project.update(attrs.id, attrs.patch, context.account.id, context.account.tenant);
    return {project};
  }
};

export const deleteProjectMutation = {
  async deleteProject(_, {input: attrs}, context) {
    dumbLog({mutation: 'deleteProject', input: attrs, accountId: context.account.id, tenantId: context.account.tenant});
    checkWriteAccess(context);
    const removedProject = await Project.remove(attrs.id, context.account.id, context.account.tenant);
    return {project: removedProject};
  }
};
