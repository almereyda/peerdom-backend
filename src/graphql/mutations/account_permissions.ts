import * as AccountPermission from '../../lib/account_permission';
import {checkWriteAccess, dumbLog} from '../../lib/helpers';

export const createAccountPermissionMutation = {
  async createAccountPermission(_, {input: attrs}, context) {
    dumbLog({
      mutation: 'createAccountPermission',
      input: attrs,
      accountId: context.account.id,
      tenantId: context.account.tenant
    });
    checkWriteAccess(context);
    const permission = await AccountPermission.create(
      attrs.newAccountPermission.tenant,
      attrs.newAccountPermission.account,
      attrs.newAccountPermission.writeAccess
    );
    return {permission: permission};
  }
};

export const updateAccountPermissionMutation = {
  async updateAccountPermission(_, {input: attrs}, context) {
    dumbLog({
      mutation: 'updateAccountPermission',
      input: attrs,
      accountId: context.account.id,
      tenantId: context.account.tenant
    });
    checkWriteAccess(context);
    const permission = await AccountPermission.update(
      attrs.tenant,
      attrs.account,
      attrs.patch.writeAccess
    );
    return {permission: permission};
  }
};

export const deleteAccountPermissionMutation = {
  async deleteAccountPermission(_, {input: attrs}, context) {
    dumbLog({
      mutation: 'deleteAccountPermission',
      input: attrs,
      accountId: context.account.id,
      tenantId: context.account.tenant
    });
    checkWriteAccess(context);
    const permission = await AccountPermission.get(attrs.tenant, attrs.account);
    await AccountPermission.remove(attrs.tenant, attrs.account);
    return {permission: permission};
  }
};
