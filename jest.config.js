module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFilesAfterEnv: ['./build/tests/setup.js'],
  testMatch: ['**/build/**/*.spec.js'],
  testPathIgnorePatterns: ['/node_modules/', '.ts$'],
  notify: true,
  notifyMode: 'failure-change'
};
