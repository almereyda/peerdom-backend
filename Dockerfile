FROM node:12

# Basic setup
RUN mkdir /app
RUN chown -R node:node /app
RUN chmod u+rwx /app
USER node:node

# We only copy what npm needs to get our app dependencies
COPY --chown=node:node package*.json /app/
WORKDIR /app
RUN NODE_ENV=dev npm ci

COPY --chown=node:node src /app/src
COPY --chown=node:node scripts/start.sh /app/scripts/
COPY --chown=node:node scripts/setup-db-config.sh /app/scripts/
COPY --chown=node:node config /app/config
COPY --chown=node:node Makefile /app/
COPY --chown=node:node ts* /app/

ENV node_pre_command=""

RUN make prod-build

EXPOSE 3000

CMD ["make", "prod-serve"]
