# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 3.0.0-beta.14 - UNRELEASED

### Added

- **node**: allow setting a role as electable ([peerdom/peerdom-frontend#186](https://gitlab.com/peerdom/peerdom-frontend/issues/186))

## 3.0.0-beta.13 - 2020-05-13

### Added

- **peer**: allow account creation when adding peers ([peerdom/peerdom-frontend#126](https://gitlab.com/peerdom/peerdom-frontend/issues/126))

- **tenant**: add a `externaAuthSettings` fields in the entity to store any
  external authentication configuration. Currently only add the needed value for Azure. ([peerdom/peerdom-backend#60][backend-60])

- **account**: add two fields in the models to check if an account is activated (`activated` field) and is using external auth (`useExternalAuth`) ([peerdom/peerdom-backend#60][backend-60])

[backend-60]: https://gitlab.com/peerdom/peerdom-backend/-/issues/60

### Changed

- **email**: change password reset email body ([peerdom/peerdom-frontend#304](https://gitlab.com/peerdom/peerdom-frontend/issues/304))

### Fixed

- **peer**: resolve slug per tenant ([peerdom/peerdom-frontend#273](https://gitlab.com/peerdom/peerdom-frontend/issues/273)
- **project**: resolve slug per tenant ([peerdom/peerdom-frontend#273](https://gitlab.com/peerdom/peerdom-frontend/issues/273)

## 3.0.0-beta.12 - 2020-04-29

### Added

- **projects**: add projects app ([peerdom/peerdom-frontend#187](https://gitlab.com/peerdom/peerdom-frontend/issues/187))
- **log**: add logs to GraphQL resolvers and REST routes ([#61](https://gitlab.com/peerdom/peerdom-backend/issues/61))

### Changed

- **Azure AD sync**: the sync is now triggered via http request to conform with Google App Engine requirement ([#52](https://gitlab.com/peerdom/peerdom-backend/issues/52))

## 3.0.0-beta.11 - 2020-04-14

### Added

- **tenant**: extend custom field definitions with order ([#57](https://gitlab.com/peerdom/peerdom-backend/issues/57))

### Changed

- **deps**: upgrade to Node 12 and update dependencies ([#42](https://gitlab.com/peerdom/peerdom-backend/issues/42))

### Fixed

- **peer**: generate new image address for each avatar upload ([#51](https://gitlab.com/peerdom/peerdom-backend/issues/51))
- **peer**: make sure custom fields appear on profile page ([peerdom/peerdom-frontend#220](https://gitlab.com/peerdom/peerdom-frontend/issues/220))

## 3.0.0-beta.10 - 2020-03-26

### Added

- **node**: add support for custom fields ([#36](https://gitlab.com/peerdom/peerdom-backend/issues/36))

### Changed

- **holders**: allow update of multiple holders at once ([peerdom/peerdom-frontend#182](https://gitlab.com/peerdom/peerdom-frontend/issues/182))

## 3.0.0-beta.9 - 2020-02-28

### Added

- **peer**: save avatars in S3-compatible storage ([peerdom/infrastructure-deployment#99](https://gitlab.com/peerdom/infrastructure-deployment/issues/4))

## 3.0.0-beta.8 - 2020-02-13

### Fixed

- **peer**: correct rotation of iPhone-captured avatars ([peerdom/peerdom-frontend#99](https://gitlab.com/peerdom/peerdom-frontend/issues/99))

## 3.0.0-beta.7 - 2020-01-30

### Added

- **node**: add support for representatives ([peerdom/peerdom-frontend#14](https://gitlab.com/peerdom/peerdom-frontend/issues/14))

## 3.0.0-beta.6 - 2020-01-06

### Added

- **node**: add support for external circles ([peerdom/peerdom-frontend#93](https://gitlab.com/peerdom/peerdom-frontend/issues/93)

### Fixed

- **node**: improve performance of copy operation ([#45](https://gitlab.com/peerdom/peerdom-backend/issues/45))

## 3.0.0-beta.5 - 2019-12-23

### Added

- **share**: expose REST endpoints for share token management ([peerdom/peerdom-frontend#38](https://gitlab.com/peerdom/peerdom-frontend/issues/38)
- **share**: allow data access to users with a valid share token ([peerdom/peerdom-frontend#38](https://gitlab.com/peerdom/peerdom-frontend/issues/38)

## 3.0.0-beta.4 - 2019-12-11

### Added

- **node**: add support for external nodes ([peerdom/peerdom-frontend#48](https://gitlab.com/peerdom/peerdom-frontend/issues/48))

## 3.0.0-beta.3 - 2019-12-04

### Added

- **peer**: add support for avatar upload and removal ([#17](https://gitlab.com/peerdom/peerdom-backend/issues/17))
- **node**: add support for copying roles ([peerdom/peerdom-frontend#3](https://gitlab.com/peerdom/peerdom-frontend/issues/3))
- **node**: add support for copying circles ([peerdom/peerdom-frontend#44](https://gitlab.com/peerdom/peerdom-frontend/issues/44))
- **peer**: add automatic resolution of global slug conflicts ([#30](https://gitlab.com/peerdom/peerdom-backend/issues/30))
- **node**: add automatic resolution of local slug conflicts ([#30](https://gitlab.com/peerdom/peerdom-backend/issues/30))

### Changed

- **node**: regenerate slug on node name update ([#29](https://gitlab.com/peerdom/peerdom-backend/issues/29))
- **node**: delete template when only one role is bound to it ([peerdom/peerdom-frontend#3](https://gitlab.com/peerdom/peerdom-frontend/issues/3))

### Fixed

- **peer**: remove avatar file on peer deletion ([#17](https://gitlab.com/peerdom/peerdom-backend/issues/17))
- **node**: fix reference to parent in event log items ([#24](https://gitlab.com/peerdom/peerdom-backend/issues/24))

## 3.0.0-beta.2 - 2019-10-17

### Changed

- **makefile**: NODE_ENV is now set to 'test' when running the tests
- **log**: prevent the "dumbLog" to print during tests
- **add-account script**: use the new Account lib instead of calling TypeORM directly
- **node**: enable parent-only update for template-based nodes
- **ci**: enforce the commit signature in the CI

### Fixed

- **account**: email are now trimmed before account creation and search
- **core**: discard special characters when generating slugs
- **auth**: make email match case insensitive

## 3.0.2-beta.1 - 2019-09-24

### Fixed

- **deployment**: fix the variable setup in deployment

## 3.0.1-beta.1 - 2019-09-24

### Changed

- **email**: switched from Mailgun SMTP to their HTTP API to fix our email sending issue

## 3.0.0-beta.1 - 2019-09-20

### Added

- **add-account script**: create a Peer for each account created
- **events log**: save changes made to app data in a log table
- **test-watch make command**: Added a command to launch the tests in watch
  mode. It will also send you a notification if the tests changes.
- **sync users from Azure AD**: add support for syncing users directly from
  Microsoft Active Directory SaaS.
- **mutation logging**: all graphql mutation are now logged to stdout

### Fixed

- **tenant**: remove matomo settings
- **peer**: create lower case slug on creation

## 1.0.0-alpha.2 - 2019-07-16

### Added

- **auth**: add password validation

### Changed

- **peer**: return peer list in alphabetical order

### Fixed

- **node**: include id in returned node after delete

## 1.0.0-alpha.1 - 2019-07-02

### Added

- First release of Peerdom backend
