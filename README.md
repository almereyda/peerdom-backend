# Peerdom backend

Ecosystem of digital tools that helps organisations evolve as a decentralized,
values-based collective of peers (backend)

⚠️ **Cannonical repository**: the Peerdom project is developed on
[GitLab.com](https://gitlab.com/peerdom/peerdom-backend). Issues or code
submission should only be submitted there.

## Project setup for development

### Installing dependencies

To develop the Peerdom backend you need to have the following packages installed:

- [nvm](https://github.com/nvm-sh/nvm), which will install the required NodeJS
  version
- [Docker](https://docs.docker.com/install/), to run all the external services
  needed by Peerdom ([docker-compose](https://docs.docker.com/compose/)
  is required and needs to be installed manually on GNU/Linux systems)
- the `make` command, which should come by default on your OS

### Running the project

If you plan to make contribution to Peerdom, please be sure to check read our
[contributing](./CONTRIBUTING.md) guidelines before submitting your work.

#### TL;DR

```bash
nvm install && make serve
```

#### Long version

The first time you want to run the project you need to install the required
NodeJS version using `nvm` (you only need to run this the very first time you
setup the project):

```bash
nvm install
```

Then you can simply run:

```bash
make serve
```

This command will:

- Ensure all the needed Node & Container dependency are installed
- Build the project
- Copy the default dev configuration
- Migrate the database
- and finally start the Peerdom backend locally

If you hit an error will runing `make serve`, try to run `make clean-all`
first, then try `make serve` again.

To run the tests you can use:

```bash
make test
```

You can find all the `make` command by running:

```bash
make help
# or simply
make
```

### Accessing the app and services

- Peerdom backend will be accessible at `http://localhost:3000`
- Adminer, a webui for databases, will be accessible at `http://localhost:8080`
- Minio UI & API, a S3 compatible service, will be accessible at `http://localhost:9000`
- Mailhog, an email catching tool, will be accessible at
  `http://localhost:8025`, and will receive any email sent by the backend.

#### Test accounts

After loading the fixtures, there are 4 test accounts available:

- `test+read-tenant-1@peerdom.org`
- `test+write-tenant-1@peerdom.org`
- `test+no-tenant@peerdom.org`
- `test+write-tenant-2@peerdom.org`

By using `password` as password for all of them, it's possible to test different
states of the application.

#### Connecting to the S3 service

Here is an example configuration for `s3cmd` to connect to the local S3 service
running on compose:

```
# Setup endpoint
host_base = localhost:9000
host_bucket = localhost:9000
bucket_location = us-east-1
use_https = false

# Setup access keys
access_key = minio
secret_key = minio123

# Enable S3 v4 signature APIs
signature_v2 = False
```

The app requires a bucket named "uploads" present in the local S3 service.
You will need to install the [MinIO client](https://docs.min.io/docs/minio-client-quickstart-guide.html)
in order to prepare the bucket. Then, execute the following commands:

```
# Configure host and credentials
mc config host add minio http://127.0.0.1:9000 minio minio123

# Create a new bucket
mc mb minio/uploads

# Set the bucket policy to "public"
mc policy set public minio/uploads
```

### Other usefull things to know

#### Clear database, run migrations and load fixtures

To migrate the database from the current state to the last one:

```bash
make migrate
```

To migrate the database from scratch (ie: delete everything)

```bash
make migrate-from-scratch
```

To load all fixtures data:

```bash
make load-data
```

Note that you only need to run the desired make command, every dependency and
needed command will be ran as part of the make dependency

#### Set config

Edit ormconfig.json and add the DB settings (user & password)

#### Generating `schema.json`

```bash
apollo schema:download --endpoint=127.0.0.1:3000
```

#### Generating TypeScript types from GraphQL schema

```bash
apollo codegen:generate --target typescript \
       --addTypename --queries "./src/graphql/types/*.ts" \
       --schema ./src/graphql/schema.ts
```

#### Synchronize database schema

This is only useful during development, then you should generate the adequate migration file instead.

```bash
ts-node typeorm schema:sync
```

If not installed globally

```bash
./node_modules/.bin/ts-node ./node_modules/.bin/typeorm schema:sync
```

## Testing on the graphql playground

Set the URL to `http://127.0.0.1:3000/firstOrg/graphql` to access
the data of the test tenant.

Here are proposed queries and mutations to test the data on the graphql playground

### Set a cookie in the browser devtools

The name of the cookie must be `authToken`.

Set its value to `63d919543f568fcb89b280eb2eb2c5` for write access.

Set its value to `369751f5e85ea85ba4c7c2fae41195` for read access.

Note: Cookies need a `Max-Age` value, don't forget it when adding it in your browser.

### Read the `displayName` of all `peers`

```
{
  peers {displayName}
}
```

### Peer CRUD operations

```
query peerQuery {
  peers(id:"10000000-0000-0000-0000-000000000002") {
    displayName
  }
}

mutation peerMutation {
    createPeer(
    input: {newPeer: {displayName: "x"}}
    ) {
      peer {
        displayName
      }
    }

    deletePeer(
      input: {id: "10000000-0000-0000-0000-000000000001"}
    ) {
      peer {
        displayName
      }
    }

    updatePeer(
        input: {id: "10000000-0000-0000-0000-000000000002", patch: {displayName: "y"}}
    ) {
        peer {
          displayName
        }
    }
}
```

## Deployment

The app is deployed via a `git push` to our deployment server (you need to be
on our :rocket: IP range to access the server, so hop in a VPN near by if
necessary).

When deploying the server will execute:

- `make start`

Anything that should be done during the deployment should be part of the
`start` target dependency chain in the makefile. Feel free to use the `scripts`
folder to store complex script there.

## Creating accounts and permissions

To create an account and let them set their own password:

- Create the account: `INSERT INTO "account" (email) VALUES ('user@example.com');`
- Send the password reset e-mail: `curl 'https://peerdom.org/requestPasswordReset' --data '{"email":"user@example.com"}'`

To create an account with a predefined password:

- `INSERT INTO "account" (email, password) VALUES ('user@example.com', crypt('password', gen_salt('bf', 8)));`

Give an account "user@example.com" access to a tenant named "exampleTenant" with write access:

```
INSERT INTO "account_permission" ("tenantId", "accountId", "writeAccess")
VALUES (
	(SELECT id FROM "tenant" WHERE "name" = 'exampleTenant'),
	(SELECT id FROM "account" WHERE "email" = 'user@example.com'),
	true
);
```

## Configuring custom terms

Custom terms are activated via the `mapSettings` JSON within the tenant table.
For example, if you want to activate the "team" term, the JSON needs to look like this:

```json
{
  "terms": {
    "circle": "team"
  }
}
```

This will put into effect the translations placed under `terms.circle.team`.

If, for example, you want to introduce a new custom term for roles, besides modifying `mapSettings`:

```json
{
  "terms": {
    "circle": "team",
    "role": "newTerm"
  }
}
```

you also need to add the relevant translations to `terms.role.newTerm`.

## Configuring custom fields

The custom fields available for each tenant are defined within the `custom_field_definition` table.
Use the following SQL to enable a new custom field within for a tenant:

```sql
INSERT INTO "custom_field_definition" ("name", "translation", "type", "multiEntry", "tenantId")
VALUES (
  'New field',
  '{"en":"New field", "de":"Neues Feld", "fr":"Nouveau champ", "nl":"Nieuw veld", "sv":"Nytt fält"}',
  'textarea',
  false,
  (SELECT id FROM "tenant" WHERE "name" = 'exampleTenant')
);
```

The type enumeration currently supports the following values,
with only the first two being supported on the front-end:

- text
- textarea
- email
- link
- date

## License

This work is currently all right reserved by Nothing AG. When we will be close
to a release we will license this work under the Business Source License 1.1.
You can [subscribe to this pull
request](https://github.com/peerdom/peerdom-backend/pull/2) if you want to
follow the situation.

The gist of the BSL 1.1 is:

- The source code will always remain available
- Each version of the software will be proprietary and under an usage limit
  **for 3 years**
- At the 3 years anniversary of the version, the version will **automatically
  be relicensed under the AGPLv3**
