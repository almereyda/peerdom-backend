import 'ts-jest';
import {createConnection, getConnection} from 'typeorm';

jest.setTimeout(10000);

beforeAll(async () => {
  await createConnection();
});

afterAll(async () => {
  await getConnection().close();
});
