import 'ts-jest';
import * as request from 'supertest';
import * as cookie from 'cookie';

import * as account from '../../src/lib/account';
import {app} from '../../src/app';

// Mock the mailer
jest.mock('nodemailer');
const nodemailer = require('nodemailer'); // Doesn't work with import, no clue why
const sendMailMock = jest.fn();
nodemailer.createTransport.mockReturnValue({
  sendMail: sendMailMock
});

import * as passwordReset from '../../src/controllers/passwordReset';
import {config} from '../../src/peerdom-config';

let resetToken;
const newPassword = 'newlySetPassword1!';

// Connect to the DB to be able to use the controllers directly
beforeAll(async () => {
  resetToken = await account.generatePasswordResetToken('test+write-tenant-1@peerdom.org');
});

describe('rest and controller: passwordReset', () => {
  test('rest: resetPassword', done => {
    request(app)
      .post('/resetPassword')
      .send({
        resetToken: resetToken,
        password: newPassword
      })
      .end((err, res) => {
        expect(res.body).toEqual({success: true});
        expect(res.status).toBe(200);

        done();
      });
  });

  test('login with new password', done => {
    request(app)
      .post('/login')
      .send({
        email: 'test+write-tenant-1@peerdom.org',
        password: newPassword
      })
      .end((err, res) => {
        expect(res.status).toBe(200);
        const firstCookie = cookie.parse(res.header['set-cookie'][0]);
        const firstCookieExpiration = new Date(firstCookie['Expires']);
        expect(firstCookie.authToken).toBeDefined();
        expect(typeof firstCookie.authToken).toBe('string');
        expect(firstCookieExpiration.getTime()).toBeGreaterThan(new Date().getTime());

        done();
      });
  });

  test('rest: Request a password for a valid account', done => {
    request(app)
      .post('/requestPasswordReset')
      .send({
        email: 'test+write-tenant-1@peerdom.org'
      })
      .end((err, res) => {
        expect(res.body).toEqual({success: true});
        expect(res.status).toBe(200);

        /*
         * TODO: To be able to test the received reset link, we'd have to mock nodemailer.
         * This is only possible if the tests start the server themselves. So this first
         * requires a change in how the tests are run. This should probably include re-loading
         * the fixtures before every test run.
         */

        done();
      });
  });

  test('rest: Request a password for an invalid account', done => {
    request(app)
      .post('/requestPasswordReset')
      .send({
        email: 'thisdoesnot@existat.all'
      })
      .end((err, res) => {
        expect(res.status).toBe(404);

        done();
      });
  });
});
