import 'ts-jest';
import * as request from 'supertest';
import * as cookie from 'cookie';

import {app} from '../../src/app';

const GRAPHQL_URL = '/firstOrg/graphql';

describe('rest: auth with write account', () => {
  let token;

  test('login', done => {
    request(app)
      .post('/login')
      .send({
        email: 'tEst+writE-TEnAnt-1@PeErdOm.org', // Also testing case insensitivity
        password: 'password'
      })
      .end((err, res) => {
        expect(res.status).toBe(200);
        const firstCookie = cookie.parse(res.header['set-cookie'][0]);
        const firstCookieExpiration = new Date(firstCookie['Expires']);
        expect(firstCookie.authToken).toBeDefined();
        expect(typeof firstCookie.authToken).toBe('string');
        expect(firstCookieExpiration.getTime()).toBeGreaterThan(new Date().getTime());
        token = firstCookie.authToken;

        done();
      });
  });

  test('use received token to read data', done => {
    // Use a simple GraphQL query to verity that the token works
    request(app)
      .post(GRAPHQL_URL)
      .send({
        operationName: null,
        variables: {},
        query: '{ tenant { id } }'
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(200);
        expect(res.body.data.tenant.id).toBe('20000000-0000-0000-0000-000000000001');
        done();
      });
  });

  test('fails to access data from other tenant', done => {
    request(app)
      .post('/secondOrg/graphql')
      .send({
        operationName: null,
        variables: {},
        query: '{ tenant { id } }'
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(400);
        expect(res.body.errors).toBeDefined();
        expect(res.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
        done();
      });
  });

  test('logout', done => {
    request(app)
      .post('/logout')
      .send({})
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(200);
        expect(res.body.success).toBe(true);
        done();
      });
  });

  // Fri Jun 28 12:27:22 2019 @Mayeu:
  // I see the point of having this here, but I don't like the fact that we are
  // in the file testing REST call and this one is actually using GraphQL.
  test('can no longer access data after logging out', done => {
    request(app)
      .post(GRAPHQL_URL)
      .send({
        operationName: null,
        variables: {},
        query: '{ tenant { id } }'
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(400);
        expect(res.body.errors).toBeDefined();
        expect(res.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
        done();
      });
  });
});

describe('rest: auth with read account', () => {
  let token;
  test('login', done => {
    request(app)
      .post('/login')
      .send({
        email: 'test+read-tenant-1@peerdom.org',
        password: 'password'
      })
      .end((err, res) => {
        expect(res.status).toBe(200);
        const firstCookie = cookie.parse(res.header['set-cookie'][0]);
        const firstCookieExpiration = new Date(firstCookie['Expires']);
        expect(firstCookie.authToken).toBeDefined();
        expect(typeof firstCookie.authToken).toBe('string');
        expect(firstCookieExpiration.getTime()).toBeGreaterThan(new Date().getTime());
        token = firstCookie.authToken;

        done();
      });
  });

  test('use received token to read data', done => {
    // Use a simple GraphQL query to verity that the token works
    request(app)
      .post(GRAPHQL_URL)
      .send({
        operationName: null,
        variables: {},
        query: '{ tenant { id } }'
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(200);
        expect(res.body.data.tenant.id).toBe('20000000-0000-0000-0000-000000000001');
        done();
      });
  });

  test('fails to write data', done => {
    request(app)
      .post(GRAPHQL_URL)
      .send({
        operationName: null,
        variables: {},
        query: `
          mutation {
            createPeer(
              input: {
                newPeer: { displayName: "Tester${Math.random()}" }
              }
            ) {
              peer {
                id
              }
            }
          }
        `
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        // expect(res.status).toBe(400); // TODO: this returns status 200 even though it has a forbidden error...
        expect(res.body.errors).toBeDefined();
        expect(res.body.errors[0].extensions.code).toBe('FORBIDDEN');
        done();
      });
  });

  test('fails to delete data', done => {
    request(app)
      .post(GRAPHQL_URL)
      .send({
        operationName: null,
        variables: {},
        query: `
          mutation {
            deletePeer(
              input: {
                id: "10000000-0000-0000-0000-000000000001"
              }
            ) {
              peer {
                displayName
              }
            }
          }
        `
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        // expect(res.status).toBe(400); // TODO: this returns status 200 even though it has an access denied error...
        expect(res.body.errors).toBeDefined();
        expect(res.body.errors[0].extensions.code).toBe('FORBIDDEN');
        done();
      });
  });

  test('fails to access data from other tenant', done => {
    request(app)
      .post('/secondOrg/graphql')
      .send({
        operationName: null,
        variables: {},
        query: '{ tenant { id } }'
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(400);
        expect(res.body.errors).toBeDefined();
        expect(res.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
        done();
      });
  });

  test('logout', done => {
    request(app)
      .post('/logout')
      .send({})
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(200);
        expect(res.body.success).toBe(true);
        done();
      });
  });

  test('can no longer access data after logging out', done => {
    request(app)
      .post(GRAPHQL_URL)
      .send({
        operationName: null,
        variables: {},
        query: '{ tenant { id } }'
      })
      .set('Cookie', 'authToken=' + token)
      .end((err, res) => {
        expect(res.status).toBe(400);
        expect(res.body.errors).toBeDefined();
        expect(res.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
        done();
      });
  });
});

describe('rest: auth access denied', () => {
  test('login with wrong password', done => {
    request(app)
      .post('/login')
      .send({
        email: 'test+read-tenant-1@peerdom.org',
        password: 'wrong pw'
      })
      .end((err, res) => {
        expect(res.status).toBe(401);
        expect(res.body).toEqual({});
        done();
      });
  });

  test('login with wrong email', done => {
    request(app)
      .post('/login')
      .send({
        email: 'wrong@peerdom.org',
        password: 'password'
      })
      .end((err, res) => {
        expect(res.status).toBe(401);
        expect(res.body).toEqual({});
        done();
      });
  });
});
