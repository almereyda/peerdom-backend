import {Factory} from 'typeorm-factory';
import * as faker from 'faker';
import * as bcrypt from 'bcrypt';

import * as TenantFactory from './tenant';
import {Account} from '../../src/entity/account';
import {AccountPermission} from '../../src/entity/account_permission';

export const minimal = new Factory(Account)
  .sequence('email', _i => faker.internet.email())
  .sequence('password', i => bcrypt.hashSync(`password-${i}`, 1));

export const permission = new Factory(AccountPermission)
  .attr('writeAccess', false)
  .assocOne('tenant', TenantFactory.minimal)
  .assocOne('account', minimal);
