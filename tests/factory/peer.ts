import {Factory} from 'typeorm-factory';
import * as faker from 'faker';

import {Peer} from '../../src/entity/peer';
import * as TenantFactory from './tenant';
import {slugify} from '../../src/lib/helpers';

export const minimal = new Factory(Peer)
  .sequence('displayName', i => `Peer Name ${i}`)
  .sequence('slug', i => slugify(`Peer Name ${i}`))
  .assocOne('tenant', TenantFactory.minimal);
