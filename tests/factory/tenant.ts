import {Factory} from 'typeorm-factory';

import * as AccountFactory from './account';
import * as PeerFactory from './peer';
import {Tenant} from '../../src/entity/tenant';
import {slugify} from '../../src/lib/helpers';

export const minimal = new Factory(Tenant)
  .sequence('name', i => `Tenant ${i}`)
  .sequence('slug', i => slugify(`Tenant ${i}`));

export const withTenPeers = minimal.assocMany('peers', PeerFactory.minimal, 10);

export const withTenAccounts = async () => {
  const tenant = await minimal.create();
  const accounts = await AccountFactory.minimal.createList(10);

  console.log(tenant);
  for (const account of accounts) {
    console.log(account);
    const permission = await AccountFactory.permission.create({tenant: tenant, account: account});
  }

  return tenant;
};

export const withAzureSettings = minimal.attr('userSyncSettings', {
  azureAd: {
    active: true,
    clientId: 'A Client ID',
    clientSecret: 'SoupeurSikret'
  }
});

export const withAzureRefreshToken = minimal.attr('userSyncSettings', {
  azureAd: {
    active: true,
    clientId: 'A Client ID',
    clientSecret: 'SoupeurSikret',
    refreshToken: 'SuperSecureTokenOMG'
  }
});
