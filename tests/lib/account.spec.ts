import 'ts-jest';
import * as request from 'supertest';
import * as cookie from 'cookie';
import {createConnection, getConnection} from 'typeorm';
import * as faker from 'faker';

import * as Account from '../../src/lib/account';

describe('Test the account module', () => {
  test('account: creating a reset password token for an unknown account should fail', async () => {
    await expect(Account.generatePasswordResetToken('donotexist@inthedb.lol')).rejects.toThrow(Error);
  });

  test('account: creating a reset password token for a known account should succeed', async () => {
    const token = await Account.generatePasswordResetToken('test+write-tenant-2@peerdom.org');
    expect(token).toBeDefined();
  });

  test('account: email should be normalized before account creation', async () => {
    const account = await Account.create('an@emai.l');

    await expect(Account.create(' an@emai.l ')).rejects.toThrow();
  });

  test('account: email should be normalized when finding accounts', async () => {
    const email = faker.internet.email();
    const account = await Account.create(email);

    expect(account).toEqual(await Account.findByEmail(` ${email} `));
  });

  test('account: upserting should work if the email is already used', async () => {
    const email = faker.internet.email();
    const account = await Account.create(email);

    expect(account).toEqual(await Account.upsert(email));
  });

  test('account: upserting should work if there is no account using an email yet', async () => {
    const email = faker.internet.email();
    const foundAccount = await Account.findByEmail(email);

    expect(foundAccount).toBeFalsy();

    expect(await Account.create(email)).toBeTruthy();
  });

  test('account: a new account should not be activated by default', async () => {
    const email = faker.internet.email();
    const account = await Account.create(email);

    expect(Account.isActivated(account)).toBeFalsy();
  });

  test('account: activating an account should be saved in the database', async () => {
    const email = faker.internet.email();
    const account = await Account.create(email);
    await Account.activate(account);

    const accountFromDB = await Account.findByEmail(email);

    expect(Account.isActivated(accountFromDB)).toBeTruthy();
  });
});
