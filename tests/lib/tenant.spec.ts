import 'ts-jest';
import * as tenant from '../../src/lib/tenant';
import * as tenantFactory from '../factory/tenant';

describe('Test the tenant module', () => {
  test('if a tenant exist, getById should return it', async () => {
    // Here we create an arbitrary tenant in the database
    const newTenant = await tenantFactory.minimal.create();

    const sameTenant = await tenant.getById(newTenant.id);

    expect(sameTenant).toStrictEqual(newTenant);
  });

  test('it should be possible to update the refresh token of the AzureAD user sync', async () => {
    const newTenant = await tenantFactory.withAzureSettings.create();
    const token = 'newToken';

    const updatedTenant = await tenant.updateAzureADRefreshToken(token, newTenant.id);
    const fromDb = await tenant.getById(newTenant.id);

    expect(updatedTenant.id).toBe(newTenant.id);
    expect(updatedTenant.userSyncSettings.azureAd.refreshToken).toBe(token);
    expect(fromDb.userSyncSettings.azureAd.refreshToken).toBe(token);
  });

  test('it should be possible to add Azure AD configuration to a tenant', async () => {
    const newTenant = await tenantFactory.minimal.create();
    const clientId = 'client_id';
    const clientSecret = 'client_secret';
    const azureSettings = {
      active: false,
      clientId: clientId,
      clientSecret: clientSecret
    };

    const updatedTenant = await tenant.addAzureADConfiguration(clientId, clientSecret, newTenant.id);

    expect(updatedTenant.userSyncSettings.azureAd).toStrictEqual(azureSettings);

    const fromDb = await tenant.getById(newTenant.id);

    expect(fromDb.userSyncSettings.azureAd).toStrictEqual(azureSettings);
  });

  // TODO: add a test for tenant.getAllActiveAzureConnection thingy
  // TODO: add a test for tenant.listAllAccounts thingy
  test('it should be possible to list all accounts linked to a tenant', async () => {
    // TODO: Factory not working
    // const newTenant = await tenantFactory.withTenAccounts();

    const accounts = await tenant.listAllAccounts('20000000-0000-0000-0000-000000000001');

    expect(accounts.length).toBe(2);
  });

  test('it should be possible to list all peers linked to a tenant', async () => {
    // TODO: Factory not working
    // const newTenant = await tenantFactory.withTenPeers.create();

    const peers = await tenant.listAllPeers('20000000-0000-0000-0000-000000000001');

    expect(peers.length).toBe(10);
  });
});
