import 'ts-jest';
import * as faker from 'faker';

import * as Project from '../../src/lib/project';
import * as Tenant from '../../src/lib/tenant';
import {slugify} from '../../src/lib/helpers';

let createdProjectId: string;
let tenantId: string;

describe('Test the project module', () => {
  beforeAll(async () => {
    const tenantName = faker.company.companyName();
    const tenant = await Tenant.save({
      name: tenantName,
      slug: slugify(tenantName)
    });

    tenantId = tenant.id;
  });

  test('it should be possible to create a project', async () => {
    const project = {
      name: faker.lorem.word(),
      holders: []
    };

    const createdProject: Project.Type = await Project.create(project, undefined, tenantId);

    createdProjectId = createdProject.id;

    expect(createdProject.id).toBeDefined();
  });

  test('it should be possible to update a project', async () => {
    const projectPatch = {
      name: faker.lorem.word()
    };

    const updatedProject: Project.Type = await Project.update(createdProjectId, projectPatch, undefined, tenantId);

    expect(updatedProject.name).toBe(projectPatch.name);
    expect(updatedProject.slug).toBe(slugify(projectPatch.name));
  });

  test('it should be possible to delete a project', async () => {
    await Project.remove(createdProjectId, undefined, tenantId);

    const nonExistantProject: Project.Type = await Project.get(tenantId, {projectId: createdProjectId})[0];
    expect(nonExistantProject).toBeUndefined();
  });
});
