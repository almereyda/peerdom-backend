import 'ts-jest';
import * as faker from 'faker';
import {factory} from 'typeorm-factories';

import * as Peer from '../../src/lib/peer';
import * as PeerFactory from '../factory/peer';
import * as TenantFactory from '../factory/tenant';

describe('Test the peer module', () => {
  test('it should be possible to create a peer', async () => {
    const tenant = await TenantFactory.minimal.create();
    const name = await faker.name.findName();

    const peer: Peer.Type = await Peer.create({name}, undefined, tenant.id);

    expect(peer.id).toBeDefined();
  });

  test('Given a peer, it should be possible to delete it', async () => {
    const tenant = await TenantFactory.minimal.create();
    const name = faker.name.findName();
    const peer = await Peer.create({name}, undefined, tenant.id);

    const deleted = await Peer.remove(peer.id, [], undefined, tenant.id);

    expect(deleted.id).not.toBeDefined();
  });
});
