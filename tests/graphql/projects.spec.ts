import 'ts-jest';
import * as request from 'supertest';
import * as cookie from 'cookie';
import {slugify} from '../../src/lib/helpers';

import {app} from '../../src/app';

let token, createdProjectId;

const GRAPHQL_URL = '/firstOrg/graphql';

beforeAll(done => {
  request(app)
    .post('/login')
    .send({
      email: 'test+write-tenant-1@peerdom.org',
      password: 'password'
    })
    .end((err, res) => {
      const firstCookie = cookie.parse(res.header['set-cookie'][0]);
      token = firstCookie.authToken;

      done();
    });
});

afterAll(done => {
  request(app)
    .post('/logout')
    .send({})
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      done();
    });
});

test('graphql: retrieve projects', done => {
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
        {
          projects {
            id
            name
          }
        }
      `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      const data = res.body.data;
      expect(data.projects).toBeDefined();
      expect(data.projects.length).toBe(10);

      for (const project of data.projects) {
        expect(typeof project.id).toEqual('string');
        expect(typeof project.name).toEqual('string');
        expect(Object.keys(project).length).toEqual(2);
      }

      done();
    });
});

test('graphql: write a project', done => {
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            createProject(
              input: {
                newProject: { name: "Project ${Math.random()}" }
              }
            ) {
              project {
                id
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      expect(res.body.data.createProject.project.id).toBeDefined();
      createdProjectId = res.body.data.createProject.project.id;
      done();
    });
});

test('graphql: update the name of a project', done => {
  const newProjectName = 'Updated Tester' + Math.random();
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            updateProject(
              input: {
                id: "${createdProjectId}",
                patch: {
                  name: "${newProjectName}"
                }
              }
            ) {
              project {
                id,
                slug
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      expect(res.body.data.updateProject.project.id).toBeDefined();
      expect(res.body.data.updateProject.project.slug).toBe(slugify(newProjectName));
      done();
    });
});

test('graphql: delete a project', done => {
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            deleteProject(
              input: {
                id: "${createdProjectId}"
              }
            ) {
              project {
                name
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      done();
    });
});
