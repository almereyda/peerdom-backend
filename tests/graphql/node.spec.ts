import 'ts-jest';
import * as request from 'supertest';
import * as cookie from 'cookie';
import {slugify} from '../../src/lib/helpers';

import {app} from '../../src/app';

let token, createdNodeId;

const GRAPHQL_URL = '/firstOrg/graphql';

beforeAll(done => {
  request(app)
    .post('/login')
    .send({
      email: 'test+write-tenant-1@peerdom.org',
      password: 'password'
    })
    .end((err, res) => {
      const firstCookie = cookie.parse(res.header['set-cookie'][0]);
      token = firstCookie.authToken;

      done();
    });
});

afterAll(done => {
  request(app)
    .post('/logout')
    .send({})
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      done();
    });
});

test('graphql: write a node', done => {
  const nodeName = 'Node Name' + Math.random();

  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            createNode(
              input: {
                newNode: {
                  name: "${nodeName}",
                  type: "circle"
                  slug: "${slugify(nodeName)}"
                  parent: "40000000-1000-0000-0000-000000000000"
                }
              }
            ) {
              node {
                id
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      if (err) {
        console.log(err);
      }
      expect(res.status).toBe(200);
      expect(res.body.data.createNode.node.id).toBeDefined();
      createdNodeId = res.body.data.createNode.node.id;
      done();
    });
});

test('graphql: update a node', done => {
  const nodeName = 'Updated Node Name' + Math.random();

  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            updateNode(
              input: {
                id: "${createdNodeId}",
                patch: {
                  name: "${nodeName}",
                }
              }
            ) {
              node {
                id
                name
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      if (err) {
        console.log(err);
      }
      expect(res.status).toBe(200);
      expect(res.body.data.updateNode.node.id).toBeDefined();
      expect(res.body.data.updateNode.node.name).toBe(nodeName);
      done();
    });
});

test('graphql: delete a node', done => {
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            deleteNode(
              input: {
                id: "${createdNodeId}"
              }
            ) {
              node {
                name
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      done();
    });
});
