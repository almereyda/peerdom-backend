import 'ts-jest';
import * as request from 'supertest';
import * as cookie from 'cookie';
import {slugify} from '../../src/lib/helpers';

import {app} from '../../src/app';

let token, createdPeerId;

const GRAPHQL_URL = '/firstOrg/graphql';

beforeAll(done => {
  request(app)
    .post('/login')
    .send({
      email: 'test+write-tenant-1@peerdom.org',
      password: 'password'
    })
    .end((err, res) => {
      const firstCookie = cookie.parse(res.header['set-cookie'][0]);
      token = firstCookie.authToken;

      done();
    });
});

afterAll(done => {
  request(app)
    .post('/logout')
    .send({})
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      done();
    });
});

test('graphql: retrieve peers', done => {
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
        {
          peers {
            id
            displayName
          }
        }
      `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      const data = res.body.data;
      expect(data.peers).toBeDefined();
      expect(data.peers.length).toBe(10);

      for (const peer of data.peers) {
        expect(typeof peer.id).toEqual('string');
        expect(typeof peer.displayName).toEqual('string');
        expect(Object.keys(peer).length).toEqual(2);
      }

      done();
    });
});

test('graphql: write a peer', done => {
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            createPeer(
              input: {
                newPeer: { displayName: "Tester${Math.random()}" }
              }
            ) {
              peer {
                id
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      expect(res.body.data.createPeer.peer.id).toBeDefined();
      createdPeerId = res.body.data.createPeer.peer.id;
      done();
    });
});

test('graphql: update the name of a peer', done => {
  const newPeerName = 'Updated Tester' + Math.random();
  const newPeerNameSlug = slugify(newPeerName);
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            updatePeer(
              input: {
                id: "${createdPeerId}",
                patch: {
                  displayName: "${newPeerName}"
                }
              }
            ) {
              peer {
                id,
                slug
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      expect(res.body.data.updatePeer.peer.id).toBeDefined();
      expect(res.body.data.updatePeer.peer.slug).toBe(slugify(newPeerName));
      done();
    });
});

test('graphql: delete a peer', done => {
  request(app)
    .post(GRAPHQL_URL)
    .send({
      operationName: null,
      variables: {},
      query: `
          mutation {
            deletePeer(
              input: {
                id: "${createdPeerId}"
              }
            ) {
              peer {
                displayName
              }
            }
          }
        `
    })
    .set('Cookie', 'authToken=' + token)
    .end((err, res) => {
      expect(res.status).toBe(200);
      done();
    });
});
